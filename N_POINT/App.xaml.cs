﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using N_POINT.MapTools;
using N_POINT.Misc;
using N_POINT.HostCommunication;
using System.Net;

namespace N_POINT
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        public delegate void SwitchControlsEvent(string a_strName);
        public static event SwitchControlsEvent EventSwitchControls;

        public delegate void ConnectToServiceEvent(bool a_boolSuccess);
        public static event ConnectToServiceEvent EventConnectToHostService;

        public delegate void ApplicationExitEvent(bool a_exit);
        public static event ApplicationExitEvent EventApplicationExit;
        static GeoQueueManager m_GEOQueueMgr;

        private static Data.DataManager m_DataMgr;
        static NeptuneWebService.fixed_networkSoapPortClient m_client;
        static ConfigSettingsManager m_configMgr;
        static HostInterface m_hostInterface;
        static EsriGeocoder m_Geocoder;
        static Authentication m_Authentication;
        static Geocoder m_AddressGeocoder;
        static Logger m_appLogger;
        static bool m_boolWebServiceConnectInProgress = false;
        static bool m_boolDebugMode = false;

        public static Data.DataManager DataManager
        {
            get
            {
                if (m_DataMgr == null)
                {
                    m_DataMgr = new Data.DataManager();
                }
                return m_DataMgr;
            }
        }


        public static bool IsDebugMode
        {
            get { return m_boolDebugMode; }
            set { m_boolDebugMode = value; }
        }

        public static GeoQueueManager GEOQueueMgr
        {
            get
            {
                if (m_GEOQueueMgr == null)
                {
                    m_GEOQueueMgr = new GeoQueueManager();

                }

                return m_GEOQueueMgr;
            }
        }

        public static bool WebServiceConnectInProgress
        {
            get { return m_boolWebServiceConnectInProgress; }
        }

        static bool mRegisteredForServerCertificateValidationCallback = false;

        public static async Task<bool> ConnectToWebService()
        {
            //this is needed to get around accepting the certificate for https
            if (!mRegisteredForServerCertificateValidationCallback)
            {
                System.Net.ServicePointManager.ServerCertificateValidationCallback +=
                    (se, cert, chain, sslerror) =>
                    {
                        return true;
                    };
                mRegisteredForServerCertificateValidationCallback = true;
            }

            bool l_boolResult = true;
            string strTestURI = "";

            m_boolWebServiceConnectInProgress = true;
            try
            {
                if (m_client != null)
                {
                    m_client.Close();
                }
                //App.AppLogger.LogMsg(Logger.enumProcessType.MISC, Logger.enumLogType.GENERAL, "Connecting to web service...");
                //strTestURI = "http://10.6.1.54:8080/fixed_network";
                //strTestURI = "http://moonriver:8080/fixed_network";

                Properties.Settings settings = new Properties.Settings();

                //strTestURI = Properties.Settings.Default.HostWebService;
                strTestURI = settings.HostWebService;

                if (strTestURI.Trim().Length > 0)
                {
                    System.ServiceModel.HttpBindingBase binding;
                    if (strTestURI.ToLower().IndexOf("https") == 0)
                    {
                        binding = new System.ServiceModel.BasicHttpsBinding();
                    }
                    else
                    {
                        binding = new System.ServiceModel.BasicHttpBinding();
                    }

                    binding.Name = "BasicHttpBinding_fixed_networkSoapPort";
                    // Set this so any large responses from the host won't get rejected. Default is 65536.
                    binding.MaxReceivedMessageSize = 131072;

                    System.ServiceModel.EndpointAddress ea = new System.ServiceModel.EndpointAddress(strTestURI);
                    
                    m_client = new NeptuneWebService.fixed_networkSoapPortClient(binding, ea);
                }
                else
                {
                    m_client = new NeptuneWebService.fixed_networkSoapPortClient();
                    l_boolResult = false;
                }

                var resp = await App.AuthenticationMgr.GetAuthenticationTokenAsync(App.ConfigurationSettingsMgr.SiteID);

                l_boolResult = resp;


                if (l_boolResult)
                {
                    //App.AppLogger.LogMsg(Logger.enumProcessType.MISC, Logger.enumLogType.GENERAL, "Connected to host web service");
                }
                else
                {
                    // If the host web service is set along with the site ID and customer number, show error message
                    // to user.
                    if ((App.ConfigurationSettingsMgr.HostWebServiceURL.Trim().Length > 0) && 
                        (App.ConfigurationSettingsMgr.SiteID.Trim().Length > 0) && 
                        (App.ConfigurationSettingsMgr.CustomerNumber.Trim().Length > 0))
                    {
                        App.AppLogger.LogMsg(Logger.enumProcessType.MISC, Logger.enumLogType.ERROR, "Unable to connect to host web service");
                        //MessageBox.Show("Unable to connect to host web service");
                    }
                }
                
            }
            catch (Exception ex)
            {
                l_boolResult = false;
                App.AppLogger.LogMsg(Logger.enumProcessType.MISC, Logger.enumLogType.ERROR, string.Format("Error during attempt to connect to host web service: Message: {0}", ex.Message));
            }
            m_boolWebServiceConnectInProgress = false;
            if (EventConnectToHostService != null)
            {
                EventConnectToHostService(l_boolResult);
            }

            return l_boolResult;
        }

        public static NeptuneWebService.fixed_networkSoapPortClient NeptuneClient
        {
            get
            {
                if (m_client == null)
                {

                    ConnectToWebService();
                    /*
                    string strTestURI = "";

                    //strTestURI = "http://10.6.1.54:8080/fixed_network";
                    //strTestURI = "http://moonriver:8080/fixed_network";
                    Properties.Settings settings = new Properties.Settings();
                    strTestURI = settings.HostWebService;
                    if (strTestURI.Trim().Length > 0)
                    {
                        System.ServiceModel.BasicHttpBinding binding = new System.ServiceModel.BasicHttpBinding();
                        binding.Name = "BasicHttpBinding_fixed_networkSoapPort";

                        System.ServiceModel.EndpointAddress ea = new System.ServiceModel.EndpointAddress(strTestURI);
                        m_client = new NeptuneWebService.fixed_networkSoapPortClient(binding, ea);
                    }
                    else
                    {
                        m_client = new NeptuneWebService.fixed_networkSoapPortClient();
                    }
                    */
                }

                return m_client;
            }
        }

        public static Authentication AuthenticationMgr
        {
            get
            {
                if (m_Authentication == null)
                {
                    m_Authentication = new Authentication();
                }

                return m_Authentication;
            }
        }

        public static HostInterface HostInterfaceMgr
        {
            get
            {
                if (m_hostInterface == null)
                {
                    m_hostInterface = new HostInterface();
                }

                return m_hostInterface;
            }
        }

        public static EsriGeocoder Esri_Geocoder
        {
            get
            {
                if (m_Geocoder == null)
                {
                    m_Geocoder = new EsriGeocoder();
                }

                return m_Geocoder;
            }
        }

        public static Geocoder AddressGeocoder
        {
            get
            {
                if (m_AddressGeocoder == null)
                {
                    m_AddressGeocoder = new Geocoder();
                }

                return m_AddressGeocoder;
            }
        }


        public static void SwitchControls(string a_strName)
        {
            if (EventSwitchControls != null)
            {
                EventSwitchControls(a_strName);
            }
        }

        public static ConfigSettingsManager ConfigurationSettingsMgr
        {
            get
            {
                if (m_configMgr == null)
                {
                    m_configMgr = new ConfigSettingsManager();
                }

                return m_configMgr;
            }
        }

        public static Logger AppLogger
        {
            get
            {
                if (m_appLogger == null)
                {
                    m_appLogger = new Logger();
                }

                return m_appLogger;
            }
        }

        public static void ApplicationIsExiting()
        {
            if (EventApplicationExit != null)
            {
                EventApplicationExit(true);
            }
        }

        protected override void OnStartup(StartupEventArgs e)
        {
            foreach (string argument in e.Args)
            {
                if (argument.Equals("/ResetProperties"))
                {
                    N_POINT.Properties.Settings.Default.Reset();
                    N_POINT.Properties.Settings.Default.Save();
                }
            }

			ServicePointManager.Expect100Continue = true;
			ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
		}

        private void OnAppExit(object sender, ExitEventArgs e)
        {
            // cancel any in progress tile generation
            if (N_POINT.Controls.UserControlMainTabControl.m_TileGenerator != null)
            {
                N_POINT.Controls.UserControlMainTabControl.m_TileGenerator.CancelTasks();
            }
            //AppLogger.LogMsg(Logger.enumProcessType.MISC, Logger.enumLogType.GENERAL, "Application exit event triggered.");
        }
    }
}
