﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Xml;
using N_POINT.Data;
using System.Diagnostics;
using System.Threading;

namespace N_POINT
{
    /// <summary>
    /// Interaction logic for BatchGeocodeMode.xaml
    /// </summary>
    public partial class BatchGeocodeMode : Window
    {
        bool m_boolIsGeocoding = false;

        private HostCommunication.HostInterface mHostInterface = null;
        private Data.DataSetInfo mDataSetInfo = null;

        public bool AnySavedToHost { get; set; }

        private bool mRetreiveComplete = true;
        private bool mGeocodeComplete = true;
        private bool mAbortGeocode = false;
        private bool mFatalError = false;
        private string mFatalErrorMessage = null;

        private Thread mControllerThread = null;
        private Thread mRetrieveThread = null;
        private Thread mGeocodeThread = null;
        private Thread mSaveThread = null;

        private class BatchGeocodeStats : N_POINT.MapTools.Geocoder.GeocodingStatus
        {
            /// <summary>
            /// Total Accounts to process
            /// </summary>
            public int TotalAccounts { get; set; }

            /// <summary>
            /// Total accounts saved
            /// </summary>
            public int AccountsSaved { get; set; }

            /// <summary>
            /// Total accounts completely processed
            /// </summary>
            public int AccountsProcessed { get; set; }

            public BatchGeocodeStats()
            {
                TotalAccounts = 0;
                AccountsSaved = 0;
                AccountsProcessed = 0;
            }
        }

        private Stopwatch mWatchControllerThread = null;
        private volatile BatchGeocodeStats mGeocoderStatus = null;

        object mAccountsToSaveBufferLock = new object();
        List<Accounts> mAccountsToSaveBuffer = new List<Accounts>();

        object mAccountsToGeocodeBufferLock = new object();
        List<Accounts> mAccountsToGeocodeBuffer = new List<Accounts>();

        private const bool EMULATE_GEOCODING = false;

        private const int MAX_BATCH_SIZE = 200;
        private const int MIN_BATCH_SIZE = 1;
        private const int DEFAULT_BATCH_SIZE = 100;
        private int BatchSize
        {
            get
            {
                if ((N_POINT.Properties.Settings.Default.BatchGeocodeBatchSize < MIN_BATCH_SIZE) ||
                    (N_POINT.Properties.Settings.Default.BatchGeocodeBatchSize > MAX_BATCH_SIZE))
                {
                    N_POINT.Properties.Settings.Default.BatchGeocodeBatchSize = DEFAULT_BATCH_SIZE;
                    N_POINT.Properties.Settings.Default.Save();
                }

                return N_POINT.Properties.Settings.Default.BatchGeocodeBatchSize;
            }
            set
            {
                if (N_POINT.Properties.Settings.Default.BatchGeocodeBatchSize != value)
                {
                    if ((value < MIN_BATCH_SIZE) ||
                        (value > MAX_BATCH_SIZE))
                    {
                        value = DEFAULT_BATCH_SIZE;
                    }
                    N_POINT.Properties.Settings.Default.BatchGeocodeBatchSize = value;
                    N_POINT.Properties.Settings.Default.Save();
                }
            }
        }

        MapTools.Geocoder m_geocoder;
        private string mSearch;
        private bool mNonGeocodedOnly;
        public BatchGeocodeMode(string aSearch, bool aNonGeocodedOnly)
        {            
            AnySavedToHost = false;
            mSearch = aSearch;
            mNonGeocodedOnly = aNonGeocodedOnly;
            InitializeComponent();
            Owner = App.Current.MainWindow;

            TextBoxBatchSize.Text = BatchSize.ToString();

            m_geocoder = new MapTools.Geocoder();

            if (App.ConfigurationSettingsMgr.AllowBatchGeocodeOfPrevItems)
            {
                lblInfo.Content = "Geocoding all accounts";
            }
            else
            {
                lblInfo.Content = "Geocoding non-geocoded accounts only";
            }
        }

        /// <summary>
        /// Starts the batch geocoding process or stops the process if it is already in progress
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks>
        /// Revision History
        /// Date     Who Defect# Description
        /// -------- --- ------- ----------------------------------------    
        /// 10/07/14 BJC         Initial Version
        /// </remarks>
        private void OnStartGeocoding(object sender, RoutedEventArgs e)
        {
            if (!m_boolIsGeocoding)
            {
                int lBatchSize = 0;
                if( (! int.TryParse(TextBoxBatchSize.Text.ToString(), out lBatchSize)) ||
                    ((lBatchSize < MIN_BATCH_SIZE) || (lBatchSize > MAX_BATCH_SIZE)))
                {
                    MessageBox.Show(this, String.Format("Batch size must be between {0} and {1}", MIN_BATCH_SIZE, MAX_BATCH_SIZE));
                    return;
                }

                BatchSize = lBatchSize;

                Mouse.OverrideCursor = Cursors.Wait;                
                mHostInterface = new HostCommunication.HostInterface();

                bool lGetNonGeocoded = false;
                if ((App.ConfigurationSettingsMgr.AllowBatchGeocodeOfPrevItems) && (!mNonGeocodedOnly))
                {
                    lGetNonGeocoded = true;
                }
                bool lGotData = mHostInterface.RequestDataSetFromHost(mSearch, lGetNonGeocoded, out mDataSetInfo);
                Mouse.OverrideCursor = null;

                if(! lGotData)
                {
                    System.Windows.MessageBox.Show(this, "Error requesting data set from host.");
                    mHostInterface = null;
                    return;
                }

                if (mDataSetInfo.NumberOfResults <= 0)
                {
                    System.Windows.MessageBox.Show(this, "No data available to geocode.");
                    mHostInterface = null;
                    return;
                }

                // start the controller thread
                mControllerThread = new Thread(new ThreadStart(ControllerThread));
                mControllerThread.Start();
                
                btnClose.Background = Brushes.LightGray;
                btnExecute.Content = "Stop";
                btnClose.IsEnabled = false;
                TextBoxBatchSize.IsEnabled = false;
            }

            // user is cancelling
            else 
            {
                Mouse.OverrideCursor = Cursors.Wait;   

                mAbortGeocode = true;

                // wait on the controller thread for up to 10 seconds
                if(! mControllerThread.Join(10000))
                {
                    // still alive, kill all of the child threads
                    App.AppLogger.LogMsg(
                        Misc.Logger.enumProcessType.GEOCODER,
                        Misc.Logger.enumLogType.WARNING,
                        "Forcing down batch geocode child threads");                    
                    mRetrieveThread.Abort();
                    mGeocodeThread.Abort();
                    mSaveThread.Abort();

                    // wait on the controller thread for up to 2 more seconds
                    if(! mControllerThread.Join(10000))
                    {
                        // still alive kill the controller thread       
                        App.AppLogger.LogMsg(
                        Misc.Logger.enumProcessType.GEOCODER,
                        Misc.Logger.enumLogType.WARNING,
                        "Forcing down batch geocode controller thread");             
                        mControllerThread.Abort();
                    }
                }

                Mouse.OverrideCursor = null;

                GeocodingDone();
            }
        }

        private void ControllerThread()
        {
            mGeocoderStatus = new BatchGeocodeStats();
            mGeocoderStatus.TotalAccounts = mDataSetInfo.NumberOfResults;

            mWatchControllerThread = new Stopwatch();
            mWatchControllerThread.Start();
            App.AppLogger.LogMsg(
                Misc.Logger.enumProcessType.GEOCODER, 
                Misc.Logger.enumLogType.GENERAL,
                String.Format(
                    "Batch Geocode Started - {0}",
                    App.ConfigurationSettingsMgr.AllowBatchGeocodeOfPrevItems ? "all accounts" : "non-geocoded accounts only"));

            m_boolIsGeocoding = true;
            mRetreiveComplete = false;
            mGeocodeComplete = false;
            mAbortGeocode = false;
            mFatalError = false;
            mFatalErrorMessage = null;

            // kick off the retrieve orders thread
            mRetrieveThread = new Thread(new ThreadStart(RetrieveThread));
            mRetrieveThread.Start();

            // kick off the geocoder thread
            mGeocodeThread = new Thread(new ThreadStart(GeocodeThread));
            mGeocodeThread.Start();

            // kick off the save orders thread
            mSaveThread = new Thread(new ThreadStart(SaveThread));
            mSaveThread.Start();

            // wait for threads to end
            mRetrieveThread.Join();
            mSaveThread.Join();
            mGeocodeThread.Join();

            // update the stats one more time
            Dispatcher.BeginInvoke(new Action(() => UpdateStatus(mGeocoderStatus)));

            App.AppLogger.LogMsg(
                Misc.Logger.enumProcessType.GEOCODER,
                Misc.Logger.enumLogType.GENERAL,
                "Batch Geocode Ended" + 
                ((mAbortGeocode == true) ? " - Aborted" : "") +
                ((mFatalError == true) ? " - Fatal Error" : "") +
                ((String.IsNullOrEmpty(mFatalErrorMessage)) ? "" : " - Message: " + mFatalErrorMessage));
            LogResults();

            m_boolIsGeocoding = false;
            Dispatcher.BeginInvoke(new Action(() => GeocodingDone()));
        }

        private void LogResults()
        {
            if( (mGeocoderStatus != null) && (mWatchControllerThread != null) )
            {
                double lTimeS = mWatchControllerThread.ElapsedMilliseconds / 1000.0;
                string lMsg =
                    "Geocode results: " + GetStatisticsString(mGeocoderStatus, "; ") + "; " +
                    String.Format("Time: {0}s; Accounts/s: {1}", lTimeS, mGeocoderStatus.NumberGeocoded / lTimeS);
                App.AppLogger.LogMsg(
                    Misc.Logger.enumProcessType.GEOCODER,
                    Misc.Logger.enumLogType.GENERAL,
                    lMsg);
            }
        }

        private void SetRetrieveProgress(int aMin, int aMax, int aValue)
        {
            RetrieveProgress.Minimum = aMin;
            RetrieveProgress.Maximum = aMax;
            RetrieveProgress.Value = aValue;            
        }

        private void SetSaveProgress(int aMin, int aMax, int aValue)
        {
            SaveProgress.Minimum = aMin;
            SaveProgress.Maximum = aMax;
            SaveProgress.Value = aValue;
        }

        private void SetGeocodeProgress(int aMin, int aMax, int aValue)
        {
            GeocodeProgress.Minimum = aMin;
            GeocodeProgress.Maximum = aMax;
            GeocodeProgress.Value = aValue;
        }

        private void SaveThread()
        {
            List<Accounts> lAccounts = new List<Accounts>();

            // use invoke instead of Begin Invoke to be sure the progress bar is cleared out
            Dispatcher.Invoke(new Action(() => { SetSaveProgress(0, mDataSetInfo.NumberOfResults, mGeocoderStatus.AccountsProcessed); }));

            while ( (!mAbortGeocode) && 
                    ((!mGeocodeComplete) || (mAccountsToSaveBuffer.Count > 0)) )
            {
                // just sleep if no data to process
                if (mAccountsToSaveBuffer.Count <= 0)
                {
                    Thread.Sleep(500);
                    continue;
                }

                // grab orders out of the save buffer to our local buffer
                lock (mAccountsToSaveBufferLock)
                {
                    while ((mAccountsToSaveBuffer.Count > 0) && (lAccounts.Count < BatchSize))
                    {
                        // remove from the buffer
                        Accounts lAccount = mAccountsToSaveBuffer[0];
                        mAccountsToSaveBuffer.RemoveAt(0);

                        // add to our local buffer if this one was geocoded and the geocode is valid
                        if ((lAccount.GeocodeResult.SubmittedToGeocoder) &&
                            (lAccount.GeocodeResult.ResultValid))
                        {
                            lAccounts.Add(lAccount);
                        }
                        
                        // incrementing here since we want to count accounts which we don't save to the host
                        mGeocoderStatus.AccountsProcessed++;
                    }
                }

                if (mAbortGeocode) { break; }

                // save to the host
                if(lAccounts.Count > 0)
                {
                    bool l_boolResult = App.HostInterfaceMgr.SaveUpdatedRecordsToHost(lAccounts);
                    if (!l_boolResult)
                    {
                        mGeocoderStatus.NumberSaveToHostErrors += lAccounts.Count;
                        //mAbortGeocode = true;
                        //mFatalError = true;
                        //mFatalErrorMessage = "Error while saving changes back to the host";
                        //break;
                    }
                    else
                    {
                        mGeocoderStatus.AccountsSaved += lAccounts.Count;
                        AnySavedToHost = true;
                    }

                    // sleep a bit between saving pages
                    Thread.Sleep(100);
                }

                // update the progress                
                Dispatcher.BeginInvoke(new Action(() => { SetSaveProgress(0, mDataSetInfo.NumberOfResults, mGeocoderStatus.AccountsProcessed); }));

                // update the stats
                Dispatcher.BeginInvoke(new Action(() => UpdateStatus(mGeocoderStatus)));

                // clear the local buffer
                lAccounts.Clear();
            }
        }

        private async void GeocodeThread()
        {
            mGeocodeComplete = false;

            List<Accounts> lAccounts = new List<Accounts>();
            int lAccountsProcessed = 0;

            // use invoke instead of Begin Invoke to be sure the progress bar is cleared out
            Dispatcher.Invoke(new Action(() => { SetGeocodeProgress(0, mDataSetInfo.NumberOfResults, lAccountsProcessed); }));

            while( (!mAbortGeocode) &&
                   ( (!mRetreiveComplete) || (mAccountsToGeocodeBuffer.Count > 0) ) )
            {
                // just sleep if no data to process or if too far ahead of the save thread
                // we don't want to incur geocode cost in case user cancels or there is some error
                if( (mAccountsToGeocodeBuffer.Count <= 0) ||
                    (mAccountsToSaveBuffer.Count > (BatchSize * 2)))
                {
                    Thread.Sleep(500);
                    continue;
                }

                // grab orders out of the geocode buffer to our local buffer
                lock (mAccountsToGeocodeBufferLock)
                {
                    while( (mAccountsToGeocodeBuffer.Count > 0) && (lAccounts.Count < BatchSize) )
                    {
                        lAccounts.Add(mAccountsToGeocodeBuffer[0]);
                        mAccountsToGeocodeBuffer.RemoveAt(0);
                    }
                }

                if (mAbortGeocode) { break; }

                // geocode the orders
                try
                {
                    await m_geocoder.BatchGeocode_N(lAccounts, mGeocoderStatus, EMULATE_GEOCODING);
                    Dispatcher.Invoke(new Action(() => UpdateStatus(mGeocoderStatus)));
                }
                catch (Exception ex)
                {
                    App.AppLogger.LogMsg(Misc.Logger.enumProcessType.GEOCODER, Misc.Logger.enumLogType.ERROR, string.Format("Batch Geocoding Error. Message: {0}", ex.Message));
                    if (!mAbortGeocode) // if we didn't abort geocoding, show the error message
                    {
                        string strDispMsg;
                        string strCompare = ex.Message.ToLower();

                        // Look for Esri specific codes related to exhausted credits
                        if (strCompare.Contains("error code '500'") || strCompare.Contains("error code '403'"))
                        {
                            strDispMsg = "Batch Geocoding System Error. Please Contact Neptune Support for assistance.\r\n";
                            strDispMsg += "Email: hhsupp@neptunetg.com\r\n";
                            strDispMsg += "Phone: (800) 647-4832";
                        }
                        else
                        {
                            strDispMsg = string.Format("Batch geocoding exception: {0}", ex.Message);
                        }

                        mAbortGeocode = true;
                        mFatalError = true;
                        mFatalErrorMessage = strDispMsg;
                    }
                }

                if (mAbortGeocode) { break; }

                // move order to the save buffer
                lock (mAccountsToSaveBufferLock)
                {
                    mAccountsToSaveBuffer.AddRange(lAccounts);
                }

                // update the progress
                lAccountsProcessed += lAccounts.Count;
                await Dispatcher.BeginInvoke(new Action(() => { SetGeocodeProgress(0, mDataSetInfo.NumberOfResults, lAccountsProcessed); }));

                // clear out local buffer
                lAccounts.Clear();
            }

            mGeocodeComplete = true;
        }

        private void RetrieveThread()
        {
            mRetreiveComplete = false;

            int lNumPages = mDataSetInfo.NumberOfResults / BatchSize;
            if(mDataSetInfo.NumberOfResults % BatchSize > 0) { lNumPages++; }

            List<Accounts> lAccounts = new List<Accounts>();

            int lAccountsProcessed = 0;
            // use invoke instead of Begin Invoke to be sure the progress bar is cleared out
            Dispatcher.Invoke(new Action(() => { SetRetrieveProgress(0, mDataSetInfo.NumberOfResults, lAccountsProcessed); }));
            for(int i=0; i<lNumPages; i++)
            {
                if (mAbortGeocode) { break; }

                // sleep a bit between retrieving pages
                if (i > 0) { Thread.Sleep(100); }

                if (mAbortGeocode) { break; }

                System.Xml.XmlDocument lDocument = null;
                if (mHostInterface.RequestGeocodeList(mDataSetInfo.QueryNumber, i + 1, BatchSize, out lDocument))
                {
                    XmlNodeList xl;

                    //
                    //The document was valid so now we can try and extract the number of results and query number from the document
                    //
                    xl = lDocument.GetElementsByTagName("Point");
                    if ((xl != null) && (xl.Count > 0))
                    {
                        foreach (XmlNode node in xl)
                        {
                            Accounts lAccount = AddAccountRecord(node, lAccounts.Count);
                            if (lAccount != null) { lAccounts.Add(lAccount); }
                        }
                    }
                }
                else
                {
                    mAbortGeocode = true;
                    mFatalError = true;
                    mFatalErrorMessage = "Unable to retrieve data from the server";
                    break;
                }

                if (mAbortGeocode) { break; }

                // move over to the geocode array
                if (lAccounts.Count > 0)
                {
                    lock (mAccountsToGeocodeBufferLock)
                    {
                        mAccountsToGeocodeBuffer.AddRange(lAccounts);
                    }
                }

                // update progress
                lAccountsProcessed += lAccounts.Count;
                Dispatcher.BeginInvoke(new Action(() => { SetRetrieveProgress(0, mDataSetInfo.NumberOfResults, lAccountsProcessed); }));

                // clear the accounts array
                lAccounts.Clear();
            }

            mRetreiveComplete = true;
        }

        private Accounts AddAccountRecord(XmlNode l_xnode, int aUniqueIndex)
        {
            XmlNode item;
            string l_strName;
            string l_strData;
            double dValue;
            Accounts acct = new Accounts();
            try
            {
                item = l_xnode.FirstChild;
                do
                {
                    l_strName = item.Name;
                    l_strData = item.InnerText;
                    switch (l_strName)
                    {
                        case "MeterNumber":
                            acct.MeterNumber = l_strData;
                            break;
                        case "PremiseKey":
                            acct.PremiseKey = l_strData;
                            break;
                        case "Account":
                            acct.Account = l_strData;
                            break;
                        case "Name":
                            acct.Name = l_strData;
                            break;
                        case "Address":
                            acct.Address = l_strData;
                            break;
                        case "City":
                            acct.City = l_strData;
                            break;
                        case "State":
                            acct.State = l_strData;
                            break;
                        case "Zip":
                            acct.ZipCode = l_strData;
                            break;
                        case "Latitude":
                            double.TryParse(l_strData, out dValue);
                            acct.Latitude = dValue;
                            break;
                        case "Longitude":
                            double.TryParse(l_strData, out dValue);
                            acct.Longitude = dValue;
                            break;
                        case "GeoResult":
                            break;
                    }
                    item = item.NextSibling;
                } while (item != null);

                if ((acct.Latitude != 0) && (acct.Longitude != 0))
                {
                    acct.IsGeocoded = true;
                }

                acct.ChangesMade = false;                
                acct.UniqueIndex = aUniqueIndex;
            }
            catch (Exception)
            {
                Debug.Assert(false);
                acct = null;
            }

            return acct;
        }

        /// <summary>
        /// Updates the status display indicating how many records have been geocoded, how many are left
        /// and how many were skipped or not found.
        /// </summary>
        /// <param name="a_geostatus"></param>
        /// <remarks>
        /// Revision History
        /// Date     Who Defect# Description
        /// -------- --- ------- ----------------------------------------    
        /// 10/07/14 BJC         Initial Version
        /// </remarks>
        private void UpdateStatus(BatchGeocodeStats a_geostatus)
        {
            lblGeocodingStatus.Content =
                string.Format("Processing {0} of {1}", a_geostatus.AccountsProcessed, a_geostatus.TotalAccounts) + "\n" +
                GetStatisticsString(a_geostatus, "\n");
        }

        private string GetStatisticsString(BatchGeocodeStats a_geostatus, string aSeparator)
        {
            string lMsg =
                string.Format("Number Geocoded: {0}", a_geostatus.NumberGeocoded) + aSeparator +
                string.Format("Number Not Found: {0}", a_geostatus.NumberNotFound) + aSeparator +
                string.Format("Number Skipped: {0}", a_geostatus.NumberSkipped) + aSeparator +
                string.Format("Number Saved: {0}", a_geostatus.AccountsSaved);
            if(a_geostatus.NumberSaveToHostErrors > 0)
            {
                lMsg += aSeparator + "Save To Host Errors: " + a_geostatus.NumberSaveToHostErrors;
            }
            return lMsg;
        }

        /// <summary>
        /// Closes the batch geocoder window when the "Close" button is clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks>
        /// Revision History
        /// Date     Who Defect# Description
        /// -------- --- ------- ----------------------------------------    
        /// 10/07/14 BJC         Initial Version
        /// </remarks>
        private void OnCloseGeocoder(object sender, RoutedEventArgs e)
        {
            // If batch geocoding of previously geocoded items is set, turn it off after
            // exiting the batch geocoding process
            if (App.ConfigurationSettingsMgr.AllowBatchGeocodeOfPrevItems)
            {
                App.ConfigurationSettingsMgr.AllowBatchGeocodeOfPrevItems = false;
            }

            // if anything saved to the host notify of delay
            if (AnySavedToHost)
            {
                MessageBox.Show(this, "It may take several minutes for geocode changes saved to the host to be reflected back in the N_POINT application.");
            }

            this.Close();
        }

        private void GeocodingDone()
        {
            if (mFatalError)
            {
                string lMsg = "Geocoding aborted!\n";
                if (String.IsNullOrWhiteSpace(mFatalErrorMessage))
                {
                    lMsg += "Unknown error while geocoding.";                    
                }
                else
                {
                    lMsg += mFatalErrorMessage;
                }

                MessageBox.Show(this, lMsg);

                // clear the error flag and message so we don't show it again for this run
                mFatalError = false;
                mFatalErrorMessage = null;
            }

            else if(mGeocoderStatus.NumberSaveToHostErrors > 0)
            {
                MessageBox.Show(
                    this,
                    string.Format("Unable to save {0} accounts to the host system.  The call to the host web service failed.  See the log file for more information.", mGeocoderStatus.NumberSaveToHostErrors));
            }

            btnClose.Background = (Brush)Application.Current.Resources["Button_Background"];
            btnClose.IsEnabled = true;
            TextBoxBatchSize.IsEnabled = true;
            btnExecute.Content = "Start";

            // if allowing geocode of previous items then don't let the user geocode again
            // don't want them to abuse geocoding and run up credits
            if (App.ConfigurationSettingsMgr.AllowBatchGeocodeOfPrevItems)
            {
                btnExecute.IsEnabled = false;
                btnExecute.Visibility = System.Windows.Visibility.Collapsed;
            }
        }
    }
}
