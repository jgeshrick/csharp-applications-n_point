﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace N_POINT.Controls
{
    /// <summary>
    /// Interaction logic for UserControlAppLogin.xaml
    /// </summary>
    public partial class UserControlAppLogin : UserControl
    {
        UserControlConfigSettings m_cfgSettings;
        public UserControlAppLogin()
        {
            InitializeComponent();
            m_cfgSettings = new UserControlConfigSettings();
            mainContent.Content = m_cfgSettings;
            tbMessage.Text = "Please provide a valid Customer Number and Option Key in order to gain access to the application.";
        }

    }
}
