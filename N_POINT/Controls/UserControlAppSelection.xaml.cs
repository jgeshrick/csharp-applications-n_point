﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace N_POINT.Controls
{
    /// <summary>
    /// Interaction logic for UserControlAppSelection.xaml
    /// </summary>
    public partial class UserControlAppSelection : UserControl
    {
        public enum APP_SELECTION
        {
            APP_TILE_PACKAGER,
            APP_GEOCODER,
            APP_CONFIGURATION
        }

        public delegate void AppSelectionEvent(APP_SELECTION a_selection);
        public event AppSelectionEvent EventAppSelection;

        List<SelectionItem> items = new List<SelectionItem>();

        public const string TILE_PACKAGER = "Tile Package Generation";
        public const string GEOCODER = "Geocoder";
        public const string CONFIGURATION = "Configuration Settings";
        public const string SELECTION_MENU = "Selection";

        public UserControlAppSelection()
        {
            InitializeComponent();
            items.Add(new SelectionItem() { Selection = TILE_PACKAGER });
            items.Add(new SelectionItem() { Selection = GEOCODER });
            items.Add(new SelectionItem() { Selection = CONFIGURATION });
            listboxSelection.ItemsSource = items;
        }

        private void OnListBoxDoubleClick(object sender, MouseButtonEventArgs e)
        {
            SelectionItem l_item = (SelectionItem)listboxSelection.SelectedItem;
            string strSelection = TILE_PACKAGER;

            if (l_item.Selection == GEOCODER)
            {
                strSelection = GEOCODER;
            }
            else if (l_item.Selection == CONFIGURATION)
            {
                strSelection = CONFIGURATION;
            }

            if (EventAppSelection != null)
            {
                //EventAppSelection(l_selection);

            }

            App.SwitchControls(strSelection);

        }
    }

    public class SelectionItem
    {
        public string Selection { get; set; }
    }
}
