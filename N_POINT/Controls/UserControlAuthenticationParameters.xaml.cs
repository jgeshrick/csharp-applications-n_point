﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace N_POINT.Controls
{
    /// <summary>
    /// Interaction logic for UserControlAuthenticationParameters.xaml
    /// </summary>
    public partial class UserControlAuthenticationParameters : UserControl
    {
        public UserControlAuthenticationParameters()
        {
            InitializeComponent();
            this.DataContext = App.ConfigurationSettingsMgr;
            tbMessage.Text = "Please provide a valid Customer Number and Option Key in order to gain access to the application.";
        }

        /// <summary>
        /// Validates the customer information entered by the user.  If the information is valid, the 
        /// user is granted access to the rest of the application.  Otherwise, the user is blocked from 
        /// using any features of the application.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnValidateCustomerOptions(object sender, RoutedEventArgs e)
        {
            bool l_boolValid = false;
            NSP_HostHelpers.NSC_HostHelpers.HostOptions l_hostOptions;
            l_hostOptions = NSP_HostHelpers.NSC_HostHelpers.ParseOptionKey(App.ConfigurationSettingsMgr.CustomerNumber, 
                                                                           App.ConfigurationSettingsMgr.OptionKey);
            if (l_hostOptions != null)
            {
                l_boolValid = l_hostOptions.m_mobileMapping;
            }

            if (l_boolValid)
            {
                MainWindow.AllowAccessToApplication(true);

                // Save settings if the user enters correct option key values
                App.ConfigurationSettingsMgr.SaveSettings();
            }
            else
            {
                MainWindow.AllowAccessToApplication(false);
                AppAccessDeniedMessage();
            }
        }

        void AppAccessDeniedMessage()
        {
            MessageBox.Show("Access Denied.\r\n\r\nThe Customer Number/Option Key combination is invalid");
        }
    }
}

