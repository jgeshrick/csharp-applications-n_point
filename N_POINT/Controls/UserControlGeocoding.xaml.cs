﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Collections.ObjectModel;
using System.Diagnostics;

namespace N_POINT.Controls
{
    /// <summary>
    /// Interaction logic for UserControlGeocoding.xaml
    /// </summary>
    public partial class UserControlGeocoding : UserControl    
    {
        MapTools.EsriArcgisRuntimeInterface m_EsriMapInterface;

        CollectionViewSource m_view;

        int m_nLastSelected = 0;
        System.Timers.Timer m_highlighttimer;
        System.Timers.Timer testtmr;
        System.Timers.Timer m_JumpToPage;
        bool m_boolHostAvailable = false;
        public UserControlGeocoding()
        {
            //App.AppLogger.LogMsg(Misc.Logger.enumProcessType.MISC, Misc.Logger.enumLogType.GENERAL, "Geocoder initializing");
            InitializeComponent();

            App.EventApplicationExit += App_EventApplicationExit;

            m_view = new System.Windows.Data.CollectionViewSource();

            m_EsriMapInterface = new MapTools.EsriArcgisRuntimeInterface(MapView1, Dispatcher);
            m_highlighttimer = new System.Timers.Timer();
            m_highlighttimer.Elapsed += HighlightTextTimerElapsed;
            m_highlighttimer.Interval = 1000;

            m_JumpToPage = new System.Timers.Timer();
            m_JumpToPage.Elapsed += JumpToPageTimer;
            m_JumpToPage.Interval = 1000;

            m_EsriMapInterface.EventNewLocation += UpdateMapPointLocation;
            m_EsriMapInterface.EventSelectedLocation += CaptureLocationPoints;
            Misc.Authentication.EventAuthorizationValid += Authentication_EventAuthorizationValid;
            App.EventConnectToHostService += App_EventConnectToHostService;

            tbSearchTerm.DataContext = this;
            tbLocalitySearch.DataContext = this;

            labelTotalPages.DataContext = App.DataManager;
            listviewData.DataContext = App.DataManager.AccountData;
            listviewData.SelectionMode = SelectionMode.Single;

            testtmr = new System.Timers.Timer();
            testtmr.Elapsed += testtmr_Elapsed;
            testtmr.Interval = 5000;
            //App.AppLogger.LogMsg(Misc.Logger.enumProcessType.MISC, Misc.Logger.enumLogType.GENERAL, "Geocoder initialization done");

            rbMapView.IsChecked = true;            
       }

        private void App_EventApplicationExit(bool a_exit)
        {
            if (m_EsriMapInterface != null)
            {
                m_EsriMapInterface.ShutdownMap();
            }
        }

        /// <summary>
        /// Triggered when the software needs to reconnect to the host. 
        /// Clears the data storage from the previous connection.
        /// </summary>
        /// <param name="a_boolSuccess"></param>
        /// <remarks>
        /// Revision History
        /// Date     Who Defect# Description
        /// -------- --- ------- ----------------------------------------    
        /// 10/07/14 BJC         Initial Version
        /// </remarks>
        private void App_EventConnectToHostService(bool a_boolSuccess)
        {
            // don't care about success or failure - if a reconnect attempt occurred, clear data always
            App.DataManager.ClearData();

            UpdatePageStatus();
        }

        /// <summary>
        /// Enables or disables the buttons in the UI based on the availability of the service.
        /// </summary>
        /// <param name="a_boolValue">disable buttons if false, otherwise enable buttons</param>
        void Authentication_EventAuthorizationValid(bool a_boolValue)
        {
            m_boolHostAvailable = a_boolValue;
            if (a_boolValue)
            {
                Dispatcher.Invoke(new Action(() => EnableButtons()));
                //EnableButtons();                
            }
            else
            {
                Dispatcher.Invoke(new Action(() => DisableButtons()));
                //DisableButtons();
            }

        }

        /// <summary>
        /// Changes all button backgrounds to gray and sets the tooltip to show that the host 
        /// service is not available.
        /// </summary>
        void DisableButtons()
        {
            btnHostSearch.Background = Brushes.LightGray;
            btnBatchGeocode.Background = Brushes.LightGray;
            btnFirstPage.Background = Brushes.LightGray;
            btnLastPage.Background = Brushes.LightGray;
            btnNextPage.Background = Brushes.LightGray;
            btnPrevPage.Background = Brushes.LightGray;
            btnSaveToHost.Background = Brushes.LightGray;

            btnBatchGeocode.ToolTip = "Host Service Unavailable";
            btnHostSearch.ToolTip = "Host Service Unavailable";
            btnFirstPage.ToolTip = "Host Service Unavailable";
            btnLastPage.ToolTip = "Host Service Unavailable";
            btnNextPage.ToolTip = "Host Service Unavailable";
            btnPrevPage.ToolTip = "Host Service Unavailable";
        }

        /// <summary>
        /// Changes button backgrounds to the normal button background color.
        /// </summary>
        void EnableButtons()
        {
            btnHostSearch.Background = (Brush)Application.Current.Resources["Button_Background"];
            btnBatchGeocode.Background = (Brush)Application.Current.Resources["Button_Background"];
            btnFirstPage.Background = (Brush)Application.Current.Resources["Button_Background"];
            btnLastPage.Background = (Brush)Application.Current.Resources["Button_Background"];
            btnNextPage.Background = (Brush)Application.Current.Resources["Button_Background"];
            btnPrevPage.Background = (Brush)Application.Current.Resources["Button_Background"];
            btnSaveToHost.Background = (Brush)Application.Current.Resources["Button_Background"];

            btnBatchGeocode.ToolTip = null;
            btnHostSearch.ToolTip = null;
            btnFirstPage.ToolTip = null;
            btnLastPage.ToolTip = null;
            btnNextPage.ToolTip = null;
            btnPrevPage.ToolTip = null;

        }
        public string NumPagesDisplay
        {
            get
            {
                string l_str;
                l_str = string.Format(" of {0}", App.DataManager.NumberOfPages);
                return l_str;
            }
        }

        void View_CurrentChanged(object sender, EventArgs e)
        {
            object obj;
            obj = listviewData.SelectedItem;

            int nIndex;
            nIndex = listviewData.SelectedIndex;
        }

        void testtmr_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            testtmr.Enabled = false;
            Dispatcher.Invoke(new Action(() => ShowSelection()));
        }

        void ShowSelection()
        {
            object obj;
            obj = listviewData.SelectedItem;

            int nIndex;
            nIndex = listviewData.SelectedIndex;

            obj = m_view.View.CurrentItem;
            if (listviewData.SelectedItem != m_view.View.CurrentItem)
            {
                m_view.View.MoveCurrentToPosition(nIndex);
            }
        }

        private void ListViewItemSelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }


        public string SearchTerm
        {
            get;
            set;
        }

        public string LocalitySearch
        {
            get;
            set; 
        }

        void SelfTest()
        {
            NeptuneWebService.authenticateRequest request;
            request = new NeptuneWebService.authenticateRequest();
            NeptuneWebService.fixed_networkSoapPortClient client = new NeptuneWebService.fixed_networkSoapPortClient();
            string strRet;
            string strXML;
            int nCode;
            strXML = "<authenticate><PartnerString>15DCF6852AFC90875135DDFE273F4B59</PartnerString><SiteId>36084</SiteId></authenticate>";
            strRet = client.authenticate(strXML, out nCode);
        }


        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            listviewData.DataContext = App.DataManager;
        }

        private void SetupViewSource()
        {
            m_view.Source = App.DataManager.AccountData;
            m_view.Filter += new FilterEventHandler(ViewFilter);
            listviewData.DataContext = m_view;
            m_view.View.CurrentChanged += View_CurrentChanged;
        }

        void HighlightTextBoxSelection()
        {
            tbCurrentPage.SelectAll();
        }

        private void HighlightTextTimerElapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            m_highlighttimer.Enabled = false;
            Dispatcher.Invoke(new Action(() => HighlightTextBoxSelection()));
        }

        private void ViewFilter(object sender, FilterEventArgs e)
        {
            int l_index = App.DataManager.AccountData.IndexOf((Data.Accounts)e.Item);
        }

        private void UpdatePageStatus()
        {
            if (m_highlighttimer != null)
            {
                m_highlighttimer.Enabled = true;
            }

            tbCurrentPage.Text = string.Format("{0}", App.DataManager.CurrentPage);
        }

        private void OnTextBoxTextChanged(object sender, TextChangedEventArgs e)
        {
            if (m_JumpToPage != null)
            {
                //m_JumpToPage.Enabled = true;
            }
        }

        /// <summary>
        /// Triggers the timer that will activate the page number entered by the user.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnJumpToPageKeyDown(object sender, KeyEventArgs e)
        {
            if (m_JumpToPage != null)
            {
                m_JumpToPage.Enabled = true;
            }
        }

        private void JumpToPageTimer(object sender, System.Timers.ElapsedEventArgs e)
        {
            m_JumpToPage.Enabled = false;
            Dispatcher.Invoke(new Action(() => GotoPageUserSelectedPage()));
        }

        /// <summary>
        /// Sets the current page to the one that the user selected
        /// </summary>
        void GotoPageUserSelectedPage()
        {
            string l_str = tbCurrentPage.Text;
            int l_intValue;
            if (int.TryParse(l_str, out l_intValue))
            {
                Mouse.OverrideCursor = Cursors.Wait;

                App.DataManager.GotoPage(l_intValue);
                listviewData.SelectedItem = System.Windows.DependencyProperty.UnsetValue;
                m_EsriMapInterface.ClearGraphicsFromMap();
                UpdatePageStatus();

                Mouse.OverrideCursor = null;
            }
            else
            {
                tbCurrentPage.Text = App.DataManager.CurrentPage.ToString();
            }
        }



        /// <summary>
        /// Go to the first page of a data set
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks>
        /// Revision History
        /// Date     Who Defect# Description
        /// -------- --- ------- ----------------------------------------    
        /// 10/07/14 BJC         Initial Version
        /// </remarks>
        private void OnFirstPage(object sender, RoutedEventArgs e)
        {
            if (App.AuthenticationMgr.ServiceAvailable)
            {
                Mouse.OverrideCursor = Cursors.Wait;
                if (App.DataManager.FirstPage())
                {
                    listviewData.SelectedItem = System.Windows.DependencyProperty.UnsetValue;
                    m_EsriMapInterface.ClearGraphicsFromMap();
                    UpdatePageStatus();
                }
                Mouse.OverrideCursor = null;
            }
        }

        /// <summary>
        /// Scroll backward to the previous page of a data set
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks>
        /// Revision History
        /// Date     Who Defect# Description
        /// -------- --- ------- ----------------------------------------    
        /// 10/07/14 BJC         Initial Version
        /// </remarks>
        private void OnPrevPage(object sender, RoutedEventArgs e)
        {
            if (App.AuthenticationMgr.ServiceAvailable)
            {
                Mouse.OverrideCursor = Cursors.Wait;
                if (App.DataManager.PreviousPage())
                {
                    listviewData.SelectedItem = System.Windows.DependencyProperty.UnsetValue;
                    m_EsriMapInterface.ClearGraphicsFromMap();
                    UpdatePageStatus();
                }
                Mouse.OverrideCursor = null;
            }
        }

        /// <summary>
        /// Scroll to the next page of a data set
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks>
        /// Revision History
        /// Date     Who Defect# Description
        /// -------- --- ------- ----------------------------------------    
        /// 10/07/14 BJC         Initial Version
        /// </remarks>
        private void OnNextPage(object sender, RoutedEventArgs e)
        {
            if (App.AuthenticationMgr.ServiceAvailable)
            {
                Mouse.OverrideCursor = Cursors.Wait;
                if (App.DataManager.NextPage())
                {
                    listviewData.SelectedItem = System.Windows.DependencyProperty.UnsetValue;
                    m_EsriMapInterface.ClearGraphicsFromMap();
                    UpdatePageStatus();
                }
                Mouse.OverrideCursor = null;
            }
        }

        /// <summary>
        /// Go to the last page of a data set
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks>
        /// Revision History
        /// Date     Who Defect# Description
        /// -------- --- ------- ----------------------------------------    
        /// 10/07/14 BJC         Initial Version
        /// </remarks>
        private void OnLastPage(object sender, RoutedEventArgs e)
        {
            if (App.AuthenticationMgr.ServiceAvailable)
            {
                Mouse.OverrideCursor = Cursors.Wait;
                if (App.DataManager.LastPage())
                {
                    listviewData.SelectedItem = System.Windows.DependencyProperty.UnsetValue;
                    m_EsriMapInterface.ClearGraphicsFromMap();
                    UpdatePageStatus();
                }
                Mouse.OverrideCursor = null;
            }
        }

        private void OnGeocodeBatchMode(object sender, RoutedEventArgs e)
        {
            if (App.AuthenticationMgr.ServiceAvailable)
            {                
                if ((App.ConfigurationSettingsMgr.DefaultState.Trim() == "") || (App.ConfigurationSettingsMgr.DefaultCity.Trim() == ""))
                {
                    MessageBox.Show("Default City and State in configuration settings must be set for batch mode geocoding.");
                }
                else
                {
                    //MapTools.Geocoder.BatchGeocodeStatus.TotalAccounts = App.DataManager.TotalAccountsInRequest;

                    BatchGeocodeMode bgm = new BatchGeocodeMode(SearchTerm, CheckNonGeocodedOnly.IsChecked.Value);
                    bgm.Owner = Window.GetWindow(Application.Current.MainWindow);
                    bgm.ShowDialog();

                    // Refresh the dataset on the host after batch geocoding in order to see the updates, if any.
                    if ((App.DataManager.TotalAccountsInRequest > 0) && (bgm.AnySavedToHost))
                    {
                        Mouse.OverrideCursor = Cursors.Wait;
                        RequestNewDataSet();
                        Mouse.OverrideCursor = null;
                    }
                }
            }
        }

        private void UpdateMapPointLocation(object sender, int a_nIndex, double dLatitude, double dLongitude)
        {
            if ((a_nIndex >= 0) && (a_nIndex < App.DataManager.AccountData.Count))
            {
                Data.Accounts item = App.DataManager.GetAccountItem(a_nIndex);
                if (item != null)
                {
                    item.GeocodeResult.HowGeocodeWasCaptured = "S";
                    item.GeocodeResult.Latitude = dLatitude;
                    item.GeocodeResult.Longitude = dLongitude;
                    item.GeocodeResult.ResultValid = true;
                    item.GeocodeResult.GeocodeScore = 100;
                    item.CaptureAccuracy = 100;
                    item.ChangesMade = true;
                    App.DataManager.ChangesSaved = false;
                    App.DataManager.SetAccountItem(a_nIndex, item);
                }
            }
        }

        private void CaptureLocationPoints(object sender, double dLatitude, double dLongitude)
        {
            Data.Accounts l_account = (Data.Accounts)listviewData.SelectedItem;
            if (l_account != null) 
            {
                m_EsriMapInterface.ClearGraphicsFromMap();
                l_account.GeocodeResult.Latitude = dLatitude;
                l_account.GeocodeResult.Longitude = dLongitude;
                l_account.GeocodeResult.ResultValid = true;
                l_account.GeocodeResult.HowGeocodeWasCaptured = "S";
                l_account.GeocodeResult.GeocodeScore = 100;
                l_account.IsGeocoded = true;
                l_account.ChangesMade = true;
                l_account.CaptureAccuracy = 100;
                l_account.SubmittedToGeocoder = true;
                App.DataManager.ChangesSaved = false;
                PlacePointOnMap(l_account);
            }
        }

        private void OnListViewPreviewMouseLeftBtnDown(object sender, MouseButtonEventArgs e)
        {
            object objsrc = e.Source;
            if (listviewData.SelectedItem != null)
            {
                if (listviewData.SelectedIndex != -1)
                {
                    m_nLastSelected = listviewData.SelectedIndex;
                }
                else
                {
                    listviewData.SelectedIndex = m_nLastSelected;
                }
                m_EsriMapInterface.ClearGraphicsFromMap();
                object obj = listviewData.SelectedItem;

                Data.Accounts l_account = (Data.Accounts)listviewData.SelectedItem;

                if (((l_account.Latitude != 0) && (l_account.Longitude != 0)) ||
                    ((l_account.GeocodeResult.Latitude != 0) && (l_account.GeocodeResult.Longitude != 0)))
                {
                    PlacePointOnMap(l_account);
                }
                else
                {
                    // submit item for geocoding - no await here
                    App.AddressGeocoder.GeocodeSingleAddressAsync(l_account);
                }
            }
        }

        
        private void PlacePointOnMap(Data.Accounts a_Account)
        {
            bool l_boolResultAvailable = false;
            MapTools.MapPointData mpd = new MapTools.MapPointData();
            if ((a_Account.GeocodeResult != null) && ((a_Account.GeocodeResult.Latitude != 0) && (a_Account.GeocodeResult.Longitude != 0)) )
            {
                mpd.Latitude = a_Account.GeocodeResult.Latitude;
                mpd.Longitude = a_Account.GeocodeResult.Longitude;
                l_boolResultAvailable = true;
            }
            else
            {
                if ((a_Account.Latitude != 0) && (a_Account.Longitude != 0))
                {
                    mpd.Latitude = a_Account.Latitude;
                    mpd.Longitude = a_Account.Longitude;
                    l_boolResultAvailable = true;
                }
            }

            if (l_boolResultAvailable)
            {
                mpd.Attributes.Add("Address", a_Account.Address);
                mpd.Attributes.Add("ItemIndex", a_Account.UniqueIndex);

                try
                {
                    m_EsriMapInterface.AddPointToMap(mpd);
                    m_EsriMapInterface.ZoomToLevel(16);
                }
                catch(Exception ex)
                {
                    App.AppLogger.LogMsg(Misc.Logger.enumProcessType.GEOCODER, Misc.Logger.enumLogType.ERROR, string.Format("Error placing point on map. Exception: {0}", ex.Message));
                }
            }
        }

        private async void OnListViewSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (listviewData.SelectedItem != null)
            {
                if (listviewData.SelectedIndex != -1)
                {
                    m_nLastSelected = listviewData.SelectedIndex;
                }
                else
                {
                    listviewData.SelectedIndex = m_nLastSelected;
                }
                m_EsriMapInterface.ClearGraphicsFromMap();
                object obj = listviewData.SelectedItem;

                Data.Accounts l_account = (Data.Accounts)listviewData.SelectedItem;

                if (((l_account.Latitude != 0) && (l_account.Longitude != 0)) || 
                    ((l_account.GeocodeResult.Latitude != 0) && (l_account.GeocodeResult.Longitude != 0)) )
                {
                    PlacePointOnMap(l_account);
                }
                else
                {
                    // submit item for geocoding
                    try
                    {
                        var task = await App.AddressGeocoder.GeocodeSingleAddressAsync(l_account);
                        if (task.GeocodeException == null)
                        {
                            PlacePointOnMap(l_account);
                        }
                        else
                        {
                            string strMsg = Misc.Utility.GetAllExceptionMessages(task.GeocodeException);
                            MessageBox.Show(strMsg);
                        }
                    }
                    catch (Exception)
                    {
                    }
                }
            }
        }

        private void OnTextBoxGotFocus(object sender, RoutedEventArgs e)
        {
            Dispatcher.Invoke(new Action(() => HighlightTextBoxSelection()) );
        }

        private void OnCheckNonGeocodedOnly(object sender, RoutedEventArgs e)
        {
        }

        /// <summary>
        /// Request a new data set from the host
        /// </summary>
        private void RequestNewDataSet()
        {
            if (App.AuthenticationMgr.ServiceAvailable)
            {
                string strSearch;
                bool l_boolValue = !CheckNonGeocodedOnly.IsChecked.Value;

                Mouse.OverrideCursor = Cursors.Wait;
                strSearch = SearchTerm;
                bool l_boolResult = App.DataManager.RequestDataSet(strSearch, l_boolValue);

                UpdatePageStatus();
                Mouse.OverrideCursor = null;
                // If request was successful and there are no pages in the data set
                if ((App.DataManager.NumberOfPages == 0) && l_boolResult)
                {
                    MessageBox.Show("Search did not find any records matching the specified criteria.");
                }
            }
        }

        /// <summary>
        /// Initiates a new search to build a dataset of records to manage
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnGenerateSearchQuery(object sender, RoutedEventArgs e)
        {
            // before generating a new query, make sure to clear the map first
            m_EsriMapInterface.ClearGraphicsFromMap();
            RequestNewDataSet();
        }

        /// <summary>
        /// Searches an address entered into the "Location" edit box
        /// </summary>
        async void AddressSearch()
        {
            LocalitySearch = tbLocalitySearch.Text;
            string strAddress = LocalitySearch;
            if (strAddress.Trim().Length > 0)
            {
                try
                {
                    var task = await App.AddressGeocoder.GeocodeSingleAddressAsync(strAddress);
                    if (task.ResultValid)
                    {
                        // Only go to a location if the result is valid
                        m_EsriMapInterface.CenterMapAtLocation(task.Latitude, task.Longitude);
                        m_EsriMapInterface.ZoomToLevel(15);
                    }
                }
                catch (Exception)
                {

                }

            }
        }

        /// <summary>
        /// Moves the map to the address specified in the "Location" text box
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnGotoGeneralLocality(object sender, RoutedEventArgs e)
        {
            if (m_EsriMapInterface.MapInitialized)
            {
                AddressSearch();
            }
            else if (m_EsriMapInterface.MapPermissionDenied)
            {
                MainWindow.ShowMapSystemError();
            }

        }

        /// <summary>
        /// Zooms the map in
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnMouseDownZoomIn(object sender, MouseButtonEventArgs e)
        {
            m_EsriMapInterface.ZoomMap(2);
        }

        /// <summary>
        /// Zooms the map out one
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnMouseDownZoomOut(object sender, MouseButtonEventArgs e)
        {
            m_EsriMapInterface.ZoomMap(0.5);
        }

        private void OnSaveGeocodedAccounts(object sender, RoutedEventArgs e)
        {
            if (App.DataManager.NumberOfChangedAccounts() > 0)
            {
                Mouse.OverrideCursor = Cursors.Wait;
                if (listviewData.SelectedItem != null)
                {
                    listviewData.SelectedItem = System.Windows.DependencyProperty.UnsetValue;
                    m_EsriMapInterface.ClearGraphicsFromMap();
                }
                if (App.DataManager.SaveChangesBackToHost())
                {
                    System.Windows.MessageBox.Show("Changes successfully saved to host.");
                }
                else
                {
                    System.Windows.MessageBox.Show("Error saving changes to host.", "Save Error");
                }
                Mouse.OverrideCursor = null;
            }
        }

        private void OnShowMapTileLayer(object sender, RoutedEventArgs e)
        {
            m_EsriMapInterface.ShowTileLayer();
        }

        private void OnShowSatelliteTileLayer(object sender, RoutedEventArgs e)
        {
            m_EsriMapInterface.ShowSatelliteLayer();
        }

        private void OnTBHostSearch(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Return)
            {
                SearchTerm = tbSearchTerm.Text;
                RequestNewDataSet();
            }
        }

        private void OnTBLocalitySearchKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Return)
            {
                if (m_EsriMapInterface.MapInitialized)
                {
                    AddressSearch();
                }
                else if (m_EsriMapInterface.MapPermissionDenied)
                {
                    MainWindow.ShowMapSystemError();
                }

            }
        }

        private void OnLVPreviewMouseBtnDown(object sender, MouseButtonEventArgs e)
        {
            e.Handled = true;
        }


    }

    public class NullImageConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value == null)
                return DependencyProperty.UnsetValue;
            return value;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
