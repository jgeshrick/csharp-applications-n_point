﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace N_POINT.Controls
{
    /// <summary>
    /// Interaction logic for UserControlMainSelectionMenu.xaml
    /// </summary>
    public partial class UserControlMainSelectionMenu : UserControl
    {
        UserControlAppSelection m_Selection;
        UserControlMapTileGenerator m_TileGenerator;
        UserControlConfigSettings m_ConfigSettings;
        UserControlGeocoding m_Geocoder;

        public UserControlMainSelectionMenu()
        {
            InitializeComponent();

            m_Selection = new UserControlAppSelection();
            mainContent.Content = m_Selection;
            m_Selection.EventAppSelection += SelectApp;
            App.EventSwitchControls += App_EventSwitchControls;

        }

        private void App_EventSwitchControls(string a_strName)
        {
            //throw new NotImplementedException();
            DisplayControl(a_strName);
        }

        private void DisplayControl(string a_strName)
        {
            if (a_strName == UserControlAppSelection.GEOCODER)
            {
                //switch to geocoder
                ShowGeocoder();
            }
            else if (a_strName == UserControlAppSelection.CONFIGURATION)
            {
                ShowConfigurationScreen();
            }
            else if (a_strName == UserControlAppSelection.SELECTION_MENU)
            {
                ShowSelectionMenu();
            }
            else
            {
                ShowTileGenerator();
            }
        }

        private void SelectApp(UserControlAppSelection.APP_SELECTION a_selection)
        {
            //throw new NotImplementedException();
            switch (a_selection)
            {
                case UserControlAppSelection.APP_SELECTION.APP_GEOCODER:
                    ShowGeocoder();
                    break;
                case UserControlAppSelection.APP_SELECTION.APP_TILE_PACKAGER:
                    ShowTileGenerator();
                    break;
                case UserControlAppSelection.APP_SELECTION.APP_CONFIGURATION:
                    ShowConfigurationScreen();
                    break;
            }
        }

        private void ShowGeocoder()
        {
            if (m_Geocoder == null)
            {
                m_Geocoder = new UserControlGeocoding();
            }

            mainContent.Content = m_Geocoder;
        }

        private void ShowTileGenerator()
        {
            if (m_TileGenerator == null)
            {
                m_TileGenerator = new UserControlMapTileGenerator();
            }

            mainContent.Content = m_TileGenerator;
        }

        private void ShowConfigurationScreen()
        {
            if (m_ConfigSettings == null)
            {
                m_ConfigSettings = new UserControlConfigSettings();
            }

            mainContent.Content = m_ConfigSettings;
        }

        private void ShowSelectionMenu()
        {
            if (m_Selection == null)
            {
                m_Selection = new UserControlAppSelection();
            }

            mainContent.Content = m_Selection;
        }


    }
}
