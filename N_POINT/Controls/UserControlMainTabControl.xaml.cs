﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace N_POINT.Controls
{
    /// <summary>
    /// Interaction logic for UserControlMainTabControl.xaml
    /// </summary>
    public partial class UserControlMainTabControl : UserControl
    {
        public static string MAP_TAB = "MAP";
        public static string GEOCODER_TAB = "GEOCODER";

        public static UserControlMapTileGenerator m_TileGenerator;
        UserControlGeocoding m_Geocoder;
        UserControlConfigSettings m_Configuration;

        TabItem m_tiTileGenerator;
        TabItem m_tiGeocoder;
        TabItem m_tiConfiguration;
        public UserControlMainTabControl()
        {
            InitializeComponent();
            MainWindow.EventEnableDisableTab += EnableDisableTab;
            SetupUI();
        }

        private void EnableDisableTab(string a_strName, bool a_boolEnable)
        {
            if (a_strName == UserControlMainTabControl.MAP_TAB)
            {
                if (a_boolEnable)
                {
                    if (!mainTabControl.Items.Contains(m_tiTileGenerator))
                    {
                        mainTabControl.Items.Insert(0, m_tiTileGenerator);
                    }
                }
                else
                {
                    if (mainTabControl.Items.Contains(m_tiTileGenerator))
                    {
                        mainTabControl.Items.Remove(m_tiTileGenerator);
                    }
                }
            }
            else if (a_strName == UserControlMainTabControl.GEOCODER_TAB)
            {
                if (a_boolEnable)
                {
                    int nInsertIndex = 0;
                    if (!mainTabControl.Items.Contains(m_tiGeocoder))
                    {
                        // Insert the Geocoder tab in the current position of the configuration tab
                        nInsertIndex = mainTabControl.Items.IndexOf(m_tiConfiguration);
                        if (nInsertIndex == -1)
                        {
                            nInsertIndex = 0;
                        }
                        mainTabControl.Items.Insert(nInsertIndex, m_tiGeocoder);
                    }
                }
                else
                {
                    if (mainTabControl.Items.Contains(m_tiGeocoder))
                    {
                        mainTabControl.Items.Remove(m_tiGeocoder);
                    }
                }
            }
        }

        void SetupUI()
        {
            m_TileGenerator = new UserControlMapTileGenerator();
            m_Geocoder = new UserControlGeocoding();
            m_Configuration = new UserControlConfigSettings();

            m_tiConfiguration = new TabItem();
            m_tiConfiguration.Content = m_Configuration;
            m_tiTileGenerator = new TabItem();
            m_tiTileGenerator.Content = m_TileGenerator;
            m_tiGeocoder = new TabItem();
            m_tiGeocoder.Content = m_Geocoder;

            m_tiTileGenerator.Header = "Map Tile Generator";
            m_tiTileGenerator.Style = (Style)this.Resources["StyleMainTabSelected"];
            m_tiTileGenerator.Content = m_TileGenerator;
            mainTabControl.Items.Add(m_tiTileGenerator);

            m_tiGeocoder.Header = "Geocoder";
            m_tiGeocoder.Style = (Style)this.Resources["StyleMainTabSelected"];
            m_tiGeocoder.Content = m_Geocoder;
            mainTabControl.Items.Add(m_tiGeocoder);

            m_tiConfiguration.Header = "Configuration";
            m_tiConfiguration.Style = (Style)this.Resources["StyleMainTabSelected"];
            m_tiConfiguration.Content = m_Configuration;
            mainTabControl.Items.Add(m_tiConfiguration);
        }


        private void OnTabControlSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (e.Source is TabControl)
            {
                TabItem ti = e.AddedItems[0] as TabItem;
                if (ti != null)
                {
                    // set all tabs to unselected tab
                    Style l_unselectedStyle = (Style)mainTabControl.FindResource("StyleMainTabUnSelected");
                    foreach (TabItem l_tabItem in mainTabControl.Items)
                    {
                        if ((l_tabItem.Style != l_unselectedStyle) &&
                                (l_tabItem != ti))
                        {
                            l_tabItem.Style = l_unselectedStyle;
                        }
                    }

                    ti.Style = (Style)mainTabControl.FindResource("StyleMainTabSelected");
                }
            }
        }
    }
}


namespace N_POINT.Converters
{
    public class TabSizeConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter,
            System.Globalization.CultureInfo culture)
        {
            TabControl tabControl = values[0] as TabControl;
            double width = tabControl.ActualWidth / tabControl.Items.Count;
            width = (int)width;
            //Subtract 1, otherwise we could overflow to two rows.
            return (width <= 1) ? 0 : (width - 3);
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter,
            System.Globalization.CultureInfo culture)
        {
            throw new NotSupportedException();
        }
    }
}