﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Esri.ArcGISRuntime.Geometry;
using Esri.ArcGISRuntime.Layers;
using Esri.ArcGISRuntime.Security;
using Esri.ArcGISRuntime.Tasks.Geoprocessing;
using Esri.ArcGISRuntime.Tasks.Offline;
using System.Diagnostics;

//using System.Windows.Forms;


namespace N_POINT.Controls
{
    /// <summary>
    /// Interaction logic for UserControlMapTileGenerator.xaml
    /// </summary>
    public partial class UserControlMapTileGenerator : UserControl
    {
        MapTools.EsriArcgisRuntimeInterface m_EsriMapInterface;
        DlgMapTPKGenerator mDlgMapTPKGenerator;
        public const int MAX_NON_AUTHORIZED_LEVEL = 17;
        private const int MIN_NON_AUTHORIZED_LEVEL = 1;
        private const int DEFAULT_MAP_ZOOM = 12;

        int m_nSelectedMaxLevel;
        int m_nSelectedMinLevel;


        UserControlShowMapAreaInfo m_MapArea;

        public void CancelTasks()
        {
            if(mDlgMapTPKGenerator != null)
            {
                mDlgMapTPKGenerator.Cancel();
            }
        }

        public UserControlMapTileGenerator()
        {
            InitializeComponent();
            
            m_EsriMapInterface = new MapTools.EsriArcgisRuntimeInterface(MapView1, Dispatcher);           

            m_MapArea = new UserControlShowMapAreaInfo();

            MapInfoContent.Content = m_MapArea;
            m_EsriMapInterface.EventMapExtentChanged += UpdateMapAreaDisplay;
            tbSearch.DataContext = this;

            SetupUI();

            rbShowMapTileLayer.IsChecked = true;

            App.EventApplicationExit += App_EventApplicationExit;
        }

        private void App_EventApplicationExit(bool a_exit)
        {
            if (m_EsriMapInterface != null)
            {
                m_EsriMapInterface.ShutdownMap();
            }
        }

        public string CityStateOrZip
        {
            get;
            set;
        }

        private void UpdateMapAreaDisplay(object sender, MapTools.MapAreaInfo l_mapArea)
        {
            m_MapArea.UpdateMapAreaInfo(l_mapArea);
        }

        /// <summary>
        /// Submits a request to the tile caching web services or if a request is active, 
        /// it cancels the current request.
        /// </summary>
        /// <remarks>
        /// Revision History
        /// Date     Who Defect# Description
        /// -------- --- ------- ----------------------------------------    
        /// 12/04/14 BJC         Initial Version
        /// </remarks>
        private void SubmitJob()
        {
            if (m_EsriMapInterface.MapInitialized)
            {
                bool l_boolOperationAllowed = true;
                int l_nMinLevel, l_nMaxLevel;
                string str;

                str = (string)comboMaxLevel.SelectedValue;
                int.TryParse(str, out l_nMaxLevel);

                str = (string)comboMinLevel.SelectedValue;
                int.TryParse(str, out l_nMinLevel);

                if (l_nMaxLevel > MAX_NON_AUTHORIZED_LEVEL)
                {
                    // check to see if the key is:
                    // 1) Valid for this case
                    // 2) Previously used
                    // 3) Expired
                    if (!App.ConfigurationSettingsMgr.IsUseDetailedMapOptionAllowed())
                    {
                        MessageBox.Show(
                            Window.GetWindow(Application.Current.MainWindow),
                            string.Format("Not Authorized to generate map tiles to this level.\n{0}", App.ConfigurationSettingsMgr.StandAloneDetailedMapDenialReason),
                            "Not Authorized");
                        l_boolOperationAllowed = false;
                    }
                }

                if (l_boolOperationAllowed)
                {
                    bool lIncludeSatelliteImagery = false;
                    if (chkIncludeSatelliteImagery.IsChecked.HasValue)
                    {
                        lIncludeSatelliteImagery = chkIncludeSatelliteImagery.IsChecked.Value;
                    }

                    // generate default map name from the search term entered
                    App.ConfigurationSettingsMgr.MapRootName = "Map";
                    if (!String.IsNullOrWhiteSpace(tbSearch.Text.ToString()))
                    {
                        String lTrimmed = tbSearch.Text.ToString().Trim().Replace(" ", "");
                        lTrimmed = lTrimmed.Replace(".", "");
                        lTrimmed = lTrimmed.Replace(",", "");
                        String lBase = "";
                        for (int i = 0; i < lTrimmed.Length; i++)
                        {
                            if ((lTrimmed[i] >= 'A' && lTrimmed[i] <= 'Z') ||
                                (lTrimmed[i] >= 'a' && lTrimmed[i] <= 'z') ||
                                (lTrimmed[i] >= '0' && lTrimmed[i] <= '9'))
                            {
                                lBase += lTrimmed[i];
                            }
                            else
                            {
                                break;
                            }
                        }
                        // use new name of bigger than 5
                        if (lBase.Length >= 5)
                        {
                            App.ConfigurationSettingsMgr.MapRootName = lBase;
                        }
                    }

                    mDlgMapTPKGenerator = 
                        new DlgMapTPKGenerator(
                            () =>
                            {
                                if (l_nMaxLevel > Controls.UserControlMapTileGenerator.MAX_NON_AUTHORIZED_LEVEL)
                                {
                                    str = MAX_NON_AUTHORIZED_LEVEL.ToString();
                                    comboMaxLevel.SelectedItem = str;
                                }
                            },
                            l_nMinLevel, 
                            l_nMaxLevel, 
                            lIncludeSatelliteImagery, 
                            m_EsriMapInterface.CurrentExtent, 
                            m_EsriMapInterface.LevelOfDetails);
                    
                    //lDlg.ShowDialog();

                    // Once we're done, reset the max level back down to the max non-authorized level if
                    // the user had permission to go above the non-authorized level.
                    
                }
            }
            else
            {
                if (m_EsriMapInterface.MapPermissionDenied)
                {
                    MainWindow.ShowMapSystemError();
                }
                else
                {
                    MessageBox.Show(
                        Window.GetWindow(Application.Current.MainWindow),
                        "The map initialization process has not yet completed.");
                }
            }              
        }

        /// <summary>
        /// Initiates the submit job function or if there is already a jub running, cancels the current job.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks>
        /// Revision History
        /// Date     Who Defect# Description
        /// -------- --- ------- ----------------------------------------    
        /// 12/04/14 BJC         Initial Version
        /// </remarks>
        private void OnSubmitJob(object sender, RoutedEventArgs e)
        {
            SubmitJob();
        }

        private void OnSubmitJobTouchDown(object sender, TouchEventArgs e)
        {
            SubmitJob();
        }

        /// <summary>
        /// Initializes the drop down list controls with the minimum and maximum values
        /// used for map tile generation
        /// </summary>
        /// <remarks>
        /// Revision History
        /// Date     Who Defect# Description
        /// -------- --- ------- ----------------------------------------    
        /// 12/07/14 BJC         Initial Version
        /// </remarks>
        void SetupUI()
        {
            for (int count = 1; count < 20; count++)
            {
                string str;
                str = string.Format("{0}", count);
                comboMaxLevel.Items.Add(str);
                comboMinLevel.Items.Add(str);
            }

            m_nSelectedMaxLevel = MAX_NON_AUTHORIZED_LEVEL;
            m_nSelectedMinLevel = MIN_NON_AUTHORIZED_LEVEL;

            comboMinLevel.SelectedItem = MIN_NON_AUTHORIZED_LEVEL.ToString();
            comboMaxLevel.SelectedItem = MAX_NON_AUTHORIZED_LEVEL.ToString();
        }

        private void OnSearchTextGotFocus(object sender, RoutedEventArgs e)
        {
            tbSearch.SelectAll();
        }

        /// <summary>
        /// Request from the Esri interface the address entered in the address search text box.
        /// </summary>
        /// <remarks>
        /// Revision History
        /// Date     Who Defect# Description
        /// -------- --- ------- ----------------------------------------    
        /// 10/17/14 BJC         Initial Version
        /// </remarks>
        private async void AddressSearch()
        {
            string l_strAddress;

            l_strAddress = tbSearch.Text.Trim();

            if (l_strAddress.Length > 0)
            {
                try
                {
                    var task = await App.AddressGeocoder.GeocodeSingleAddressAsync(l_strAddress);

                    if (task.ResultValid)
                    {
                        // Center the map at the location
                        m_EsriMapInterface.CenterMapAtLocation(task.Latitude, task.Longitude);

                        // Set the map to a default zoom level after finding a location
                        m_EsriMapInterface.ZoomToLevel(DEFAULT_MAP_ZOOM);
                    }
                    else
                    {
                        MessageBox.Show(
                            Window.GetWindow(Application.Current.MainWindow),
                            "Location not found using the given search criteria", 
                            "Location Not Found");
                    }
                }
                catch (Exception ex)
                {
                    App.AppLogger.LogMsg(Misc.Logger.enumProcessType.TILECACHER, Misc.Logger.enumLogType.ERROR, string.Format("Error trying to find location: {0}", ex.Message));
                    MessageBox.Show(
                        Window.GetWindow(Application.Current.MainWindow),
                        "Error trying to find location.");
                }
            }
        }

        /// <summary>
        /// Search for the address entered in the address search text box
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks>
        /// Revision History
        /// Date     Who Defect# Description
        /// -------- --- ------- ----------------------------------------    
        /// 12/07/14 BJC         Initial Version
        /// </remarks>
        private async void OnBtnAddressSearch(object sender, RoutedEventArgs e)
        {
            if (m_EsriMapInterface.MapInitialized)
            {
                AddressSearch();
            }
            else if (m_EsriMapInterface.MapPermissionDenied)
            {
                MainWindow.ShowMapSystemError();
            }
        }

        /// <summary>
        /// Request the Esri interface object to show the map tile layer
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks>
        /// Revision History
        /// Date     Who Defect# Description
        /// -------- --- ------- ----------------------------------------    
        /// 12/07/14 BJC         Initial Version
        /// </remarks>
        private void OnBtnShowMapTileLayer(object sender, RoutedEventArgs e)
        {

            m_EsriMapInterface.ShowTileLayer();
        }

        /// <summary>
        /// Request the Esri interface object show the map satellite layer
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks>
        /// Revision History
        /// Date     Who Defect# Description
        /// -------- --- ------- ----------------------------------------    
        /// 12/07/14 BJC         Initial Version
        /// </remarks>
        private void OnBtnShowMapSatelliteLayer(object sender, RoutedEventArgs e)
        {
            m_EsriMapInterface.ShowSatelliteLayer();
        }

        /// <summary>
        /// Request the Esri interface object to show the map tile layer.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks>
        /// Revision History
        /// Date     Who Defect# Description
        /// -------- --- ------- ----------------------------------------    
        /// 12/07/14 BJC         Initial Version
        /// </remarks>
        private void OnSelectShowMapTileLayer(object sender, RoutedEventArgs e)
        {
            m_EsriMapInterface.ShowTileLayer();
        }

        private void OnMouseDownZoomIn(object sender, MouseButtonEventArgs e)
        {
            m_EsriMapInterface.ZoomMap(2);
        }


        private void OnMouseDownZoomOut(object sender, MouseButtonEventArgs e)
        {
            m_EsriMapInterface.ZoomMap(0.5);
        }

        /// <summary>
        /// Processes key strokes in the search edit box. The only key of interest for now is the "Return" key.
        /// When the user presses the return key, initiate the search.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks>
        /// Revision History
        /// Date     Who Defect# Description
        /// -------- --- ------- ----------------------------------------    
        /// 12/07/14 BJC         Initial Version
        /// </remarks>
        private void OnSearchTBKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Return)
            {
                if (m_EsriMapInterface.MapInitialized)
                {
                    AddressSearch();
                }
                else if (m_EsriMapInterface.MapPermissionDenied)
                {
                    MainWindow.ShowMapSystemError();
                }
            }
        }

        /// <summary>
        /// Called when the user changes the combo maximum level.  This function verifies that the user
        /// selected a valid maximum level.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks>
        /// Revision History
        /// Date     Who Defect# Description
        /// -------- --- ------- ----------------------------------------    
        /// 12/07/14 BJC         Initial Version
        /// </remarks>
        private void OnComboMaxLevelChanged(object sender, SelectionChangedEventArgs e)
        {
            int l_nSelectedMaxLevel;
            string l_str = comboMaxLevel.SelectedItem.ToString();
            int.TryParse(l_str, out l_nSelectedMaxLevel);

            if (l_nSelectedMaxLevel > MAX_NON_AUTHORIZED_LEVEL)
            {
                if (App.ConfigurationSettingsMgr.IsUseDetailedMapOptionAllowed())
                {
                    // Allow access to restricted levels
                    comboMaxLevel.SelectedItem = l_nSelectedMaxLevel;
                }
                else 
                {
                    MessageBox.Show(
                        Window.GetWindow(Application.Current.MainWindow),
                        string.Format("Not Authorized to generate map tiles to the selected level.\n{0}", App.ConfigurationSettingsMgr.StandAloneDetailedMapDenialReason), 
                        "Not Authorized");
                    l_str = MAX_NON_AUTHORIZED_LEVEL.ToString();
                    comboMaxLevel.SelectedItem = l_str;
                    m_nSelectedMaxLevel = MAX_NON_AUTHORIZED_LEVEL;
                }                
            }
            else if (l_nSelectedMaxLevel < m_nSelectedMinLevel)
            {
                MessageBox.Show(
                    Window.GetWindow(Application.Current.MainWindow),
                    string.Format("The maximum level must be greater than or equal to the minimum level."));
                comboMaxLevel.SelectedItem = m_nSelectedMaxLevel.ToString();
            }
            else
            {
                // selection was valid, keep track of the maximum level selected.
                m_nSelectedMaxLevel = l_nSelectedMaxLevel;
            }
        }

        /// <summary>
        /// Called when the user changes the minimum level drop down list selection
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks>
        /// Revision History
        /// Date     Who Defect# Description
        /// -------- --- ------- ----------------------------------------    
        /// 12/07/14 BJC         Initial Version
        /// </remarks>
        private void OnComboMinLevelChanged(object sender, SelectionChangedEventArgs e)
        {
            int l_nSelectedMaxLevel;
            int l_nSelectedMinLevel;
            if ((comboMaxLevel.SelectedItem != null) && (comboMinLevel.SelectedItem != null))
            {
                string l_str = comboMaxLevel.SelectedItem.ToString();
                int.TryParse(l_str, out l_nSelectedMaxLevel);

                l_str = comboMinLevel.SelectedItem.ToString();
                int.TryParse(l_str, out l_nSelectedMinLevel);

                if (l_nSelectedMinLevel > l_nSelectedMaxLevel)
                {
                    MessageBox.Show(
                        Window.GetWindow(Application.Current.MainWindow),
                        "The minimum level must be less than or equal to the selected maximum level");
                    //l_nSelectedMinLevel = l_nSelectedMaxLevel;
                    // set back to the previous value
                    comboMinLevel.SelectedItem = m_nSelectedMinLevel.ToString();
                }
                else
                {
                    if (l_nSelectedMinLevel > MAX_NON_AUTHORIZED_LEVEL)
                    {
                        // don't allow setting the min level higher than the max authorized level
                        l_str = MAX_NON_AUTHORIZED_LEVEL.ToString();
                        comboMinLevel.SelectedItem = l_str;
                        l_nSelectedMaxLevel = MAX_NON_AUTHORIZED_LEVEL;

                    }
                    else
                    {
                        m_nSelectedMinLevel = l_nSelectedMinLevel;
                    }

                }
            }

        }

    }
}
