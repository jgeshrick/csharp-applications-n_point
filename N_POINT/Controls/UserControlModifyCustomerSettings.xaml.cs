﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace N_POINT.Controls
{
    /// <summary>
    /// Interaction logic for UserControlModifyCustomerSettings.xaml
    /// </summary>
    public partial class UserControlModifyCustomerSettings : UserControl
    {
        string m_strCustomerNumber;
        string m_strOptionKey;
        string m_strDetailedMapKey;
        string m_strSiteID;
        string m_strHostServiceURL;

        const string m_str_combined_Host_URL_Part = "combinedHost/FIXED_NETWORK";
        const string m_str_combined_URL_part = "combinedHost";
        const string m_str_Fixed_Network_URL_Part = "FIXED_NETWORK";

        public UserControlModifyCustomerSettings()
        {
            InitializeComponent();
            DataContext = this;
            CustomerNumber = App.ConfigurationSettingsMgr.CustomerNumber;
            HostWebServiceURL = App.ConfigurationSettingsMgr.HostWebServiceURL;
            SiteID = App.ConfigurationSettingsMgr.SiteID;
            OptionKey = App.ConfigurationSettingsMgr.OptionKey;
            DetailedMapKey = App.ConfigurationSettingsMgr.DetailedMapKey;
            
            DlgModifySettings.EventValidateData += ValidateData;
            DlgModifySettings.EventSaveData += SaveData;
        }

        private bool SaveData()
        {
            App.ConfigurationSettingsMgr.CustomerNumber = CustomerNumber;
            App.ConfigurationSettingsMgr.OptionKey = OptionKey;
            App.ConfigurationSettingsMgr.DetailedMapKey = DetailedMapKey;
            App.ConfigurationSettingsMgr.SiteID = SiteID;
            App.ConfigurationSettingsMgr.HostWebServiceURL = HostWebServiceURL;
            App.ConfigurationSettingsMgr.SaveSettings();
            return true;
        }

        private bool ValidateData()
        {
            ValidateHostURL();
            return true;
        }

        public string CustomerNumber
        {
            get { return m_strCustomerNumber; }
            set
            {
                m_strCustomerNumber = value;
            }
        }

        public string OptionKey
        {
            get { return m_strOptionKey; }
            set { m_strOptionKey = value; }
        }

        public string DetailedMapKey
        {
            get { return m_strDetailedMapKey; }
            set { m_strDetailedMapKey = value; }
        }

        public string SiteID
        {
            get { return m_strSiteID; }
            set 
            { 
                m_strSiteID = value;
            }
        }

        public string HostWebServiceURL
        {
            get { return m_strHostServiceURL; }
            set
            {
                m_strHostServiceURL = value;
            }
        }
        /*
        private bool ValidateDetailedMapKey()
        {
            bool l_boolResult = true;
            if (App.ConfigurationSettingsMgr.DetailedMapKey.Trim() != "")
            {
                l_boolResult = App.ConfigurationSettingsMgr.IsUseDetailedMapOptionAllowed();
            }

            return l_boolResult;
        }
        */

        private bool ValidateHostURLParts(string a_strURL)
        {
            string[] strParts;
            bool l_boolValid = false;
            string strTest;
            // Skip past the 'Http://' portion of the URL
            int nIndex = a_strURL.IndexOf("http://");
            if (nIndex == -1)
            {
                nIndex = a_strURL.IndexOf("https://");
                if (nIndex != -1)
                {
                    nIndex += "https://".Length;
                }
            }
            else
            {
                nIndex += "http://".Length;
            }

            // if the URL had an 'http' portion, check how many URL parts followed it.
            if (nIndex != -1)
            {
                strTest = a_strURL.Substring(nIndex);
                strParts = strTest.Split(new char[] { '/' });
                // we only care about 3 parts. Doesn't matter what they are.
                if ((strParts.Length == 3) || (strParts.Length == 2))
                {
                    l_boolValid = true;
                }
            }

            return l_boolValid;
        }

        private bool ValidateHostURL()
        {
            bool l_boolResult = true;
            string l_strTest;
            string l_strPortNumber = "8080";
            string l_strOrig;


            l_strOrig = HostWebServiceURL.Trim();

            if (l_strOrig.Length > 0)
            {
                //
                // Check for the 'http://' part
                //
                int l_nIndex = 0;
                int l_nLength;
                int l_nHttpIndex;
                l_strTest = l_strOrig.ToLower();

                if (!l_strTest.Contains("//"))
                {
                    l_strOrig = l_strOrig.Insert(0, "http://");
                    //l_nIndex = l_strOrig.Length-1;
                    l_nIndex = "http://".Length + 1;
                }
                else
                {
                    l_nIndex = l_strOrig.IndexOf("//");

                    // if it's not "http:" and not "https:"
                    if ((l_nIndex != 5) && (l_nIndex != 6))
                    {
                        // assume 'http'
                        l_strOrig = l_strOrig.Remove(0, l_nIndex);
                        l_strOrig = l_strOrig.Insert(0, "http:");
                    }

                    // advance the index beyond the "http://"
                    l_nIndex = "http://".Length + 1;
                }

                l_nHttpIndex = l_nIndex;

                //
                // Check for the web service port number part
                //
                l_nIndex = l_strOrig.IndexOf(":", l_nIndex);

                if (l_nIndex == -1)
                {
                    l_nIndex = l_strOrig.IndexOf("/", l_nHttpIndex);

                    if (l_nIndex == -1)
                    {
                        l_nIndex = l_strOrig.Length;
                    }
                    // the web service port number doesn't exist

                    if (l_nHttpIndex != -1)
                    {
                        // the web service address exists, but there is no port number
                        l_strOrig = l_strOrig.Insert(l_nIndex, string.Format(":{0}", l_strPortNumber));
                    }
                    else
                    {
                        l_strOrig += string.Format(":{0}", l_strPortNumber);
                    }
                }
                else
                {
                    // port number exists - find it
                    int l_nIndex2;

                    l_nIndex++; // advance past the ':'
                    l_nIndex2 = l_strOrig.IndexOf("/", l_nIndex);
                    if (l_nIndex2 == -1)
                    {
                        // didn't find a forward slash, assume there is nothing following the port number
                        l_nIndex2 = l_strOrig.Length;
                    }

                    l_nLength = l_nIndex2 - l_nIndex;
                    l_strPortNumber = l_strOrig.Substring(l_nIndex, l_nLength);
                }

                // The first part of the URL is validated, now check the rest
                if (!ValidateHostURLParts(l_strOrig))
                {
                    //
                    //Check to see if the host service URL is there
                    //
                    bool l_boolSkipFixedCheck = false;

                    if (!l_strOrig.Contains(m_str_combined_URL_part))
                    {
                        // by this point the base URL should be built
                        l_nIndex = l_strOrig.IndexOf(string.Format(":{0}", l_strPortNumber));
                        if (l_nIndex != -1)
                        {
                            l_nIndex += string.Format(":{0}", l_strPortNumber).Length;

                            // remove everything after the port number
                            if (l_strOrig.Length != l_nIndex)
                            {
                                l_nLength = l_strOrig.Length - l_nIndex;
                                l_strOrig = l_strOrig.Remove(l_nIndex, l_nLength);

                                // l_nIndex is now equal to the length of the URL string
                            }
                        }

                        // in case the URL already contained the string ":8080", check for the backslash after
                        if (l_nIndex != l_strOrig.Length)
                        {

                            if (l_strOrig.Substring(l_nIndex, 1) != "/")
                            {
                                l_strOrig.Insert(l_nIndex + 1, "/");
                            }
                        }

                        if (l_strOrig.Substring(l_strOrig.Length - 1, 1) != "/")
                        {
                            l_strOrig += "/";
                        }

                        l_strOrig += m_str_combined_URL_part;
                    }
                    else
                    {
                        // we found the combined host part. assume the checks are done
                        l_boolSkipFixedCheck = true;
                    }

                    if (!l_boolSkipFixedCheck && !l_strOrig.Contains(m_str_Fixed_Network_URL_Part))
                    {
                        //by this point, the 'combinedHost' string should be there
                        l_strOrig += "/";
                        l_strOrig += m_str_Fixed_Network_URL_Part;
                    }
                    else if (!l_boolSkipFixedCheck)
                    {
                        // remove anything that comes after the "combined host part
                        l_nIndex = l_strOrig.IndexOf(m_str_combined_URL_part);
                        l_nIndex += m_str_combined_URL_part.Length;
                        l_nLength = l_strOrig.Length - l_nIndex;
                        if (l_nLength > 0)
                        {
                            l_strOrig = l_strOrig.Remove(l_nIndex, l_nLength);
                        }
                    }
                }

                HostWebServiceURL = l_strOrig;
            }
            else
            {
                l_boolResult = false;
            }
            return l_boolResult;
        }

    }
}
