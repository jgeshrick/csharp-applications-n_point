﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace N_POINT.Controls
{
    /// <summary>
    /// Interaction logic for UserControlShowMapAreaInfo.xaml
    /// </summary>
    public partial class UserControlShowMapAreaInfo : UserControl
    {
        MapTools.MapAreaInfo m_mapArea;
        public UserControlShowMapAreaInfo()
        {
            InitializeComponent();
            m_mapArea = new MapTools.MapAreaInfo();
            this.DataContext = m_mapArea;
        }

        public void UpdateMapAreaInfo(MapTools.MapAreaInfo a_maparea)
        {
            m_mapArea.UpdateMapAreInfo(a_maparea);
        }
    }
}
