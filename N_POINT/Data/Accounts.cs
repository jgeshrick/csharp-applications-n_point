﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace N_POINT.Data
{
    public class Accounts : ViewModelBase
    {
        string m_strName;
        string m_strAddress;
        string m_strAccount;
        string m_strMeterNum;
        int m_nMIUID;
        bool m_boolIsGeocoded;
        double m_dblLatitude;
        double m_dblLongitude;
        int m_nUniqueIndex;
        string m_strPremiseKey;
        string m_strCity;
        string m_strState;
        string m_strZipCode;
        string m_strGEOResult;
        bool m_boolChangesMade;
        string m_strHowCaptured;
        string m_strStatusToolTip;
        int m_nCaptureAccuracy;
        MapTools.GeocodedResult m_geoResult;
        const string GEOCODE_FAILURE = "pack://application:,,,/N_POINT;component/Images/GeocodeFailed.png";
        const string GEOCODE_NO_VALID_DATA = "pack://application:,,,/N_POINT;component/Images/ngd.png";
        const string GEOCODE_VALID = "pack://application:,,,/N_POINT;component/Images/GeocodeOK.png";
        const string GEOCODED_ITEM = "pack://application:,,,/N_POINT;component/Images/Location.png";


        public Accounts()
        {
            m_strName = "";
            m_strAddress = "";
            m_strAccount = "";
            m_strMeterNum = "";
            m_strPremiseKey = "";
            m_strPremiseKey = "";
            m_strCity = "";
            m_strState = "";
            m_strZipCode = "";
            m_strGEOResult = "";
            m_strStatusToolTip = "";
            m_nMIUID = 0;
            m_dblLatitude = 0.0;
            m_dblLongitude = 0.0;
            m_boolIsGeocoded = false;
            m_boolChangesMade = false;
            m_nUniqueIndex = 0;
            m_nCaptureAccuracy = 0;
            m_geoResult = new MapTools.GeocodedResult();         
        }

        public int UniqueIndex
        {
            get { return m_nUniqueIndex; }
            set { m_nUniqueIndex = value; }
        }
        public string PremiseKey
        {
            get { return m_strPremiseKey; }
            set { m_strPremiseKey = value; }
        }

        public string City
        {
            get { return m_strCity; }
            set { m_strCity = value; }
        }

        public string State
        {
            get { return m_strState; }
            set { m_strState = value; }
        }

        public string ZipCode
        {
            get { return m_strZipCode; }
            set { m_strZipCode = value; }
        }

        public string Name
        {
            get { return m_strName; }
            set { m_strName = value; }
        }

        public string Address
        {
            get { return m_strAddress; }
            set { m_strAddress = value; }
        }

        public string Account
        {
            get { return m_strAccount; }
            set { m_strAccount = value; }
        }

        public string MeterNumber
        {
            get { return m_strMeterNum; }
            set { m_strMeterNum = value; }
        }

        public int MIUID
        {
            get { return m_nMIUID; }
            set { m_nMIUID = value; }
        }

        public double Latitude
        {
            get { return m_dblLatitude; }
            set 
            {
                m_boolChangesMade = true;
                m_dblLatitude = value;
            }
        }

        public double Longitude
        {
            get { return m_dblLongitude; }
            set 
            { 
                m_dblLongitude = value; 
                m_boolChangesMade = true; 
            }
        }

        public string GeoResult
        {
            get { return m_strGEOResult; }
            set { m_strGEOResult = value; }
        }

        public bool IsGeocoded
        {
            get { return m_boolIsGeocoded; }
            set { m_boolIsGeocoded = value; }
        }

        public object Geocoded
        {
            get
            {
                string l_strImgName = "";
                if (m_boolIsGeocoded)
                {
                    l_strImgName = GEOCODED_ITEM;
                }

                if (string.IsNullOrEmpty(l_strImgName))
                {
                    return System.Windows.DependencyProperty.UnsetValue;
                }

                return l_strImgName;
            }
        }

        public MapTools.GeocodedResult GeocodeResult
        {
            get { return m_geoResult; }
        }

        public int CaptureAccuracy
        {
            get { return m_nCaptureAccuracy; }
            set { m_nCaptureAccuracy = value; }
        }

        /// <summary>
        /// Indicates how the geocoded value was captured
        /// </summary>
        public string HowGeocodeWasCaptured
        {
            get { return m_strHowCaptured; }
            set 
            { 
                m_strHowCaptured = value; 
                m_boolChangesMade = true; 
            }
        }

        /// <summary>
        /// Whether or not any changes were made to the data set
        /// </summary>
        public bool ChangesMade
        {
            get { return (m_boolChangesMade | m_geoResult.ResultValid); }
            set { m_boolChangesMade = value; }
        }

        public bool SubmittedToGeocoder
        {
            get
            {
                return m_geoResult.SubmittedToGeocoder;
            }

            set
            {
                m_geoResult.SubmittedToGeocoder = value;
                OnPropertyChanged("Status");
            }
        }

        public string StatusToolTip
        {
            get 
            {
                return m_strStatusToolTip;
            }

            set
            {
                m_strStatusToolTip = value;
                OnPropertyChanged("StatusToolTip");
            }

        }

        public object Status
        {
            get
            {
                string l_strImgName = "";
                // if the address, latitude, and longitude are not set then indicate no valid data for geocoding
                if ((m_strAddress.Trim().Length == 0) && (m_geoResult.Latitude == 0.0) && (m_geoResult.Longitude == 0.0))
                {
                    l_strImgName = GEOCODE_NO_VALID_DATA;
                    StatusToolTip = "The account address is not set.\r\nThis account cannot be geocoded";
                    return l_strImgName;
                }
                else if ((Latitude == 0) && (Longitude == 0))
                {
                    if ((m_strCity.Trim().Length == 0) &&
                        (m_strState.Trim().Length == 0) &&
                         (App.ConfigurationSettingsMgr.DefaultCity.Trim().Length == 0) &&
                         (App.ConfigurationSettingsMgr.DefaultState.Trim().Length == 0))
                    {
                        l_strImgName = GEOCODE_NO_VALID_DATA;
                        StatusToolTip = "Unable to geocode this account. (Default city and state are not set).";
                    }
                }

                if (GeocodeResult != null)
                {
                    string lExtraInfo = "";
                    if (GeocodeResult.AddressInputParametersDictionary != null)
                    {
                        foreach (string lKey in GeocodeResult.AddressInputParametersDictionary.Keys)
                        {
                            lExtraInfo += string.Format("{0}: {1}\n", lKey, GeocodeResult.AddressInputParametersDictionary[lKey]);
                        }
                    }
                    lExtraInfo += "Score: " + GeocodeResult.BestScore.ToString();

                    if (GeocodeResult.SubmittedToGeocoder && GeocodeResult.ResultValid)
                    {
                        l_strImgName = GEOCODE_VALID;
                        StatusToolTip = "Valid Geocode.\n" + lExtraInfo;
                    }
                    else if (GeocodeResult.SubmittedToGeocoder && !GeocodeResult.ResultValid)
                    {
                        l_strImgName = GEOCODE_FAILURE;                        
                        StatusToolTip = "Could not geocode.\n" + lExtraInfo;
                    }
                }

                // make sure string isn't null or empty before returning value
                if (string.IsNullOrEmpty(l_strImgName))
                {
                    return System.Windows.DependencyProperty.UnsetValue;
                }

                return l_strImgName;
            }
        }
    }
}
