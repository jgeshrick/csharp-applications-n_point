﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
using System.IO;
using System.Xml;

using System.Diagnostics;

namespace N_POINT.Data
{
    //
    //When requests are made to the service, the results of the query will be stored here.
    //
    public class DataManager : ViewModelBase
    {
        const int MAX_BATCH_SAVES = 382; // 478 is the maximum that will succeed. Set to 80 percent of the maximum = 382
        public enum SHOW_STATUS
        {
            GEOCODED,
            NON_GEOCODED,
            ALL_ACCOUNTS
        }

        string m_strLastSearchTerm = "";
        bool m_boolIncludeGeocoded = false;

        DataSetInfo m_dsInfo;

        object m_dataLock = null;

        bool m_boolRequestInProgress = false;

        bool m_boolChangesSaved = false;

        // Default page size for viewing data on the map
        public const int DEFAULT_NUM_PER_PAGE = 25;

        // Page size for performing batch geocodes.
        // NOTE: Changed it to match DEFAULT_NUM_PER_PAGE in order to not burn through 
        //       credits too fast. This should give the Esri reporting service enough time to keep
        //       up with usage in order have a better chance to get the the email alert out before 
        //       all of the credits are used up
        public const int BATCH_GEOCODE_PAGE_SIZE = DEFAULT_NUM_PER_PAGE;

        int m_nNumItemsPerPage;

        //int m_nNumPages;
        List<Accounts> m_MasterList;
        int m_nSavedActivePage = 0;
        ObservableCollection<Accounts> m_ocAccounts;
        List<Accounts> m_SaveList; // list of accounts to save

        public DataManager()
        {
            m_dataLock = new object();
            //init account list
            m_dsInfo = new DataSetInfo();
            m_MasterList = new List<Accounts>();
            m_ocAccounts = new ObservableCollection<Accounts>();
            NumberOfPages = 0;
            m_nNumItemsPerPage = DEFAULT_NUM_PER_PAGE;
        }

        void TestFunction()
        {
            //TestData();
            //ProcessImportFile("C:\\Projects\\CSharp\\MX900Dev\\MX900ImportFiles\\ROADMAPS.IMP");
            //SelfTest();

        }

        public ObservableCollection<Accounts> AccountData
        {
            get
            {
                return m_ocAccounts;
            }
        }

        public List<Accounts> QueuedAccountsForSave
        {
            get
            {
                if (m_SaveList == null)
                {
                    m_SaveList = new List<Accounts>();
                }

                return m_SaveList;
            }
        }

        Accounts m_selectedItem = null;

        public Accounts SelectedItem
        {
            get { return m_selectedItem; }
            set
            {
                if ((m_selectedItem != value) && (value != null))
                {
                    m_selectedItem = value;
                    OnPropertyChanged("SelectedItem");
                }
                else
                {
                    //OnPropertyChanged("SelectedItem");
                }

            }

        }

        /// <summary>
        /// Number of pages contained in the current data set 
        /// </summary>
        public int NumberOfPages
        {
            get
            {
                if (m_dsInfo != null)
                {
                    return m_dsInfo.TotalPages;
                }
                else
                {
                    return 0;
                }
            }

            set
            {
                m_dsInfo.TotalPages = value;
                OnPropertyChanged("NumberOfPagesDisplay");
            }
        }

        public string NumberOfPagesDisplay
        {
            get
            {
                string l_strData;
                l_strData = string.Format("of {0}", m_dsInfo.TotalPages);
                return l_strData;
            }
        }

        /// <summary>
        /// Total number of items on the current pag
        /// </summary>
        public int TotalItemsOnCurrentPage
        {
            get { return AccountData.Count; }
        }


        /// <summary>
        /// Total number of accounts that exist in the request for data from the host
        /// </summary>
        public int TotalAccountsInRequest
        {
            get
            {
                if (m_dsInfo == null)
                {
                    return 0;
                }

                return m_dsInfo.NumberOfResults;
            }
        }

        /// <summary>
        /// The current page that is showing in the display
        /// </summary>
        public int CurrentPage
        {
            get 
            {
                if (m_dsInfo == null)
                {
                    return 0;
                }

                return m_dsInfo.PageNumber; 
            }
        }

        /// <summary>
        /// Indicates whether or not changes in the stored data set have been saved back to the host.
        /// </summary>
        public bool ChangesSaved
        {
            get { return m_boolChangesSaved; }
            set { m_boolChangesSaved = value; }
        }

        /// <summary>
        /// Clears the data collection that is mapped to the list view display
        /// </summary>
        /// <remarks>
        /// Revision History
        /// Date     Who Defect# Description
        /// -------- --- ------- ----------------------------------------    
        /// 12/04/14 BJC         Initial Version
        /// </remarks>
        public void ClearData()
        {
            AccountData.Clear();
            m_dsInfo.InitInfo();

            // Force label bound to the NumberOfPages attribute to update to reflect the new
            // number of pages
            NumberOfPages = 0;
        }

        /// <summary>
        /// Checks to see if there are any unsaved pages
        /// </summary>
        /// <returns></returns>
        /// <remarks>
        /// Revision History
        /// Date     Who Defect# Description
        /// -------- --- ------- ----------------------------------------    
        /// 12/04/14 BJC         Initial Version
        /// </remarks>
        bool AnyUnsavedChanges()
        {
            bool l_boolResult = false;
            if (m_boolChangesSaved)
            {
                return false;
            }

            foreach (Accounts l_acct in m_ocAccounts)
            {
                if (l_acct.ChangesMade)
                {
                    l_boolResult = true;
                    break;
                }
            }

            return l_boolResult;
        }

        /// <summary>
        /// Calculates the number of changed accounts on the current page.
        /// </summary>
        /// <returns>The number of changed accounts</returns>
        /// <remarks>
        /// Revision History
        /// Date     Who Defect# Description
        /// -------- --- ------- ----------------------------------------    
        /// 12/04/14 BJC         Initial Version
        /// </remarks>
        public int NumberOfChangedAccountsOnPage()
        {
            int l_nCount = 0;
            foreach (Accounts l_acct in m_ocAccounts)
            {
                if (l_acct.ChangesMade)
                {
                    l_nCount++;
                }
            }

            return l_nCount;
        }

        /// <summary>
        /// Returns the number of changed accounts for the current data set
        /// </summary>
        /// <returns>number of changed accounts</returns>
        public int NumberOfChangedAccounts()
        {
            int l_nCount = NumberOfChangedAccountsOnPage();
            l_nCount += QueuedAccountsForSave.Count;

            return l_nCount;
        }

        /// <summary>
        /// If there are changes on the current page, save them into the queued accounts list.
        /// </summary>
        public void QueueChanges()
        {
            foreach (Accounts l_acct in m_ocAccounts)
            {
                if (l_acct.ChangesMade)
                {
                    int nIndex = IndexOfQueuedItem(l_acct);
                    if (nIndex == -1)
                    {
                        // doesn't exist in the list, add it
                        QueuedAccountsForSave.Add(l_acct);
                    }
                    else
                    {
                        // it's already in the list, update it
                        QueuedAccountsForSave[nIndex] = l_acct;
                    }
                }
            }
        }

        /// <summary>
        /// Retrieves and displays the first page in the data set
        /// </summary>
        /// <returns>true if able to go to first page or already on first page, false otherwise</returns>
        /// <remarks>
        /// Revision History
        /// Date     Who Defect# Description
        /// -------- --- ------- ----------------------------------------    
        /// 12/04/14 BJC         Initial Version
        /// </remarks>
        public bool FirstPage(int a_nNewNumPerPage = DEFAULT_NUM_PER_PAGE)
        {
            bool bResult = false;
            // if changing the number of items per page, refresh the data set so that the server has
            // the correct page count and we can reference the data correctly
            if (m_nNumItemsPerPage != a_nNewNumPerPage)
            {
                RefreshDataSetAfterChanges(a_nNewNumPerPage);
            }

            if (m_dsInfo.TotalPages > 0)
            {
                if (m_dsInfo.PageNumber != 1)
                {
                    QueueChanges();
                    bResult = RequestDataSetPage(1);
                }
                else
                {
                    bResult = true;
                }
            }

            return bResult;
        }

        /// <summary>
        /// Retrieves and displays the enxt page in the data set
        /// </summary>
        /// <returns></returns>
        /// <remarks>
        /// Revision History
        /// Date     Who Defect# Description
        /// -------- --- ------- ----------------------------------------    
        /// 12/04/14 BJC         Initial Version
        /// </remarks>
        public bool NextPage()
        {
            bool bResult = false;
            if (m_dsInfo.PageNumber < m_dsInfo.TotalPages)
            {
                QueueChanges();
                bResult = RequestDataSetPage(m_dsInfo.PageNumber + 1);
            }

            return bResult;
        }

        /// <summary>
        /// Retrieves and displays the previous page in the data set
        /// </summary>
        /// <returns>true if able to activate the previous page, false otherwise</returns>
        /// <remarks>
        /// Revision History
        /// Date     Who Defect# Description
        /// -------- --- ------- ----------------------------------------    
        /// 12/04/14 BJC         Initial Version
        /// </remarks>
        public bool PreviousPage()
        {
            bool bResult = false;
            if (m_dsInfo.PageNumber > 1)
            {
                QueueChanges();
                bResult = RequestDataSetPage(m_dsInfo.PageNumber - 1);
            }

            return bResult;
        }

        /// <summary>
        /// Scrolls to the last page in the data set requested from the host
        /// </summary>
        /// <returns>true if able to scroll to last page, false otherwise</returns>
        /// <remarks>
        /// Revision History
        /// Date     Who Defect# Description
        /// -------- --- ------- ----------------------------------------    
        /// 12/04/14 BJC         Initial Version
        /// </remarks>
        public bool LastPage()
        {
            bool bResult = false;
            if (m_dsInfo.TotalPages > 0)
            {
                // Check to see if we're already on the last page
                if (m_dsInfo.PageNumber < m_dsInfo.TotalPages)
                {
                    QueueChanges();
                    bResult = RequestDataSetPage(m_dsInfo.TotalPages);
                }
            }

            return bResult;
        }

        /// <summary>
        /// Stores the current active page number so it can be returned to later.
        /// </summary>
        /// <remarks>
        /// Revision History
        /// Date     Who Defect# Description
        /// -------- --- ------- ----------------------------------------    
        /// 12/04/14 BJC         Initial Version
        /// </remarks>
        public void RememberActivePage()
        {
            m_nSavedActivePage = m_dsInfo.PageNumber;
        }

        /// <summary>
        /// Restores the page that was remembered
        /// </summary>
        /// <remarks>
        /// Revision History
        /// Date     Who Defect# Description
        /// -------- --- ------- ----------------------------------------    
        /// 12/04/14 BJC         Initial Version
        /// </remarks>
        public void RestoreRememberedPage()
        {
            RequestDataSetPage(m_nSavedActivePage);
        }

        /// <summary>
        /// Moves to a specific page in a data set.
        /// </summary>
        /// <param name="a_nPage">A valid page in the data set</param>
        /// <remarks>
        /// Revision History
        /// Date     Who Defect# Description
        /// -------- --- ------- ----------------------------------------    
        /// 12/04/14 BJC         Initial Version
        /// </remarks>
        public void GotoPage(int a_nPage)
        {
            if ((a_nPage > 0) && (a_nPage <= m_dsInfo.TotalPages))
            {
                RequestDataSetPage(a_nPage);
            }
        }

        /// <summary>
        /// Request a new data set from the host system.  The host system will cache the data and 
        /// transfer the data to this application one page at a time.  There are 25 records per
        /// page. 
        /// </summary>
        /// <param name="a_strSearchTerm"></param>
        /// <param name="a_boolIncludeGeocoded"></param>
        /// <remarks>
        /// Revision History
        /// Date     Who Defect# Description
        /// -------- --- ------- ----------------------------------------    
        /// 12/04/14 BJC         Initial Version
        /// </remarks>
        public bool RequestDataSet(string a_strSearchTerm, bool a_boolIncludeGeocoded)
        {
            bool l_boolResult = false;
            if (!m_boolRequestInProgress)
            {
                m_boolRequestInProgress = true;

                if (App.HostInterfaceMgr.RequestDataSetFromHost(a_strSearchTerm, a_boolIncludeGeocoded, out m_dsInfo))
                {
                    // save the search parameters.
                    m_strLastSearchTerm = a_strSearchTerm;
                    m_boolIncludeGeocoded = a_boolIncludeGeocoded;

                    if (m_dsInfo != null)
                    {
                        // Set to true even if there isn't any data in the data set
                        l_boolResult = true;
                        int l_nResultsPerPage = m_nNumItemsPerPage;
                        if (m_dsInfo.ResultsPerPage > 0)
                        {
                            l_nResultsPerPage = m_dsInfo.ResultsPerPage;
                        }

                        if (m_dsInfo.NumberOfResults > 0)
                        {
                            int nValue = (int)(m_dsInfo.NumberOfResults / l_nResultsPerPage);

                            if ((m_dsInfo.NumberOfResults % m_nNumItemsPerPage) != 0)
                            {
                                nValue++;
                            }

                            NumberOfPages = nValue;

                            if (NumberOfPages == 0)
                            {
                                NumberOfPages = 1;
                            }

                            //
                            // It seems that sending the request for the first page of data too soon after requesting
                            // a data set results in a server protocol violation.
                            //
                            System.Threading.Thread.Sleep(1000);

                            // request the first page of data
                            l_boolResult = RequestDataSetPage(1);
                        }
                        else
                        {
                            // set to trigger the OnPropertyChanged method
                            NumberOfPages = 0;
                        }
                    }
                }
                else
                {
                    System.Windows.MessageBox.Show("Error requesting data set from host.");
                }

                m_boolRequestInProgress = false;
            }

            return l_boolResult;
        }

        /// <summary>
        /// Requests a specific page from the host server
        /// </summary>
        /// <param name="nPageNum">the page to retrieve</param>
        /// <remarks>
        /// Revision History
        /// Date     Who Defect# Description
        /// -------- --- ------- ----------------------------------------    
        /// 12/04/14 BJC         Initial Version
        /// </remarks>
        private bool RequestDataSetPage(int nPageNum)
        {
            XmlDocument l_xml;
            bool l_boolSuccess = false;

            try
            {
                if (App.HostInterfaceMgr.RequestGeocodeList(m_dsInfo.QueryNumber, nPageNum, m_nNumItemsPerPage, out l_xml))
                {
                    l_boolSuccess = true;
                    if (MainWindow.MyDispatcher != null)
                    {
                        MainWindow.MyDispatcher.Invoke(new Action(() => RetrieveAccountsFromXML(l_xml)));
                    }
                    else
                    {
                        RetrieveAccountsFromXML(l_xml);
                    }
                    m_dsInfo.PageNumber = nPageNum;

                    m_boolChangesSaved = false;
                }
                else
                {
                    System.Windows.MessageBox.Show("Unable to load requested data");
                }
            }
            catch (Exception ex)
            {
                App.AppLogger.LogMsg(Misc.Logger.enumProcessType.GEOCODER, Misc.Logger.enumLogType.ERROR, string.Format("Error retrieving data set from host: {0}", ex.Message));
            }


            return l_boolSuccess;        
        }

        /// <summary>
        /// Saves any changes made to an account back to the host system.
        /// </summary>
        /// <param name="a_boolRefreshDataset"></param>
        /// <returns>true if successful, false otherwise</returns>
        /// <remarks>
        /// Revision History
        /// Date     Who Defect# Description
        /// -------- --- ------- ----------------------------------------    
        /// 12/04/14 BJC         Initial Version
        /// </remarks>
        public bool SaveChangesBackToHost(bool a_boolRefreshDataset = true)
        {
            if( (a_boolRefreshDataset) && (MainWindow.MyDispatcher != null) )
            {
                MainWindow.MyDispatcher.Invoke(new Action(() => MainWindow.SelectMouseCursor(System.Windows.Input.Cursors.Wait)));
            }
            bool l_boolResult = true;
            List<Accounts> l_geocodeChanges = new List<Accounts>();

            // save any changes made on the current page
            foreach (Accounts l_acct in m_ocAccounts)
            {
                if (l_acct.ChangesMade)
                {
                    int nIndex = -1;
                    nIndex = IndexOfQueuedItem(l_acct);
                    if (nIndex < 0)
                    {
                        // not in list, add it to the list to save
                        l_geocodeChanges.Add(l_acct);
                    }
                    else
                    {
                        // already in list, just update it in queued items. It'll be added below
                        QueuedAccountsForSave[nIndex] = l_acct;
                    }
                }
            }

            // save any queued changes now
            foreach (Accounts l_queuedAcct in QueuedAccountsForSave)
            {
                l_geocodeChanges.Add(l_queuedAcct);
            }

            Debug.WriteLine("Save count = " + l_geocodeChanges.Count.ToString());
            if (l_geocodeChanges.Count > 0)
            {
                // found some changed records that needs saving
                List<Accounts> l_batchChanges = new List<Accounts>();
                int nBatch = 0;
                for (int count = 0; count < l_geocodeChanges.Count; count++)
                {
                    l_batchChanges.Add(l_geocodeChanges[count]);

                    nBatch++;
                    if ((nBatch == MAX_BATCH_SAVES) || (count == l_geocodeChanges.Count - 1))
                    {
                        Debug.WriteLine("SaveUpdatedRecordsToHost");

                        // if there are MAX_BATCH_SAVES records to save or we've reached the end, save the changes
                        l_boolResult = App.HostInterfaceMgr.SaveUpdatedRecordsToHost(l_batchChanges);

                        // Allow some time between saves. If this isn't done, the service throws an
                        // exception indicating a protocol violation.
                        //System.Threading.Thread.Sleep(1000);

                        // reset the counter
                        nBatch = 0;

                        // clear the batch list
                        l_batchChanges.Clear();
                    }
                }

                if (l_boolResult)
                {
                    // clear the queued list if successfully able to save accounts
                   QueuedAccountsForSave.Clear();
                }

                // refresh the dataset if updates were successful and the refresh flag is set
                if (l_boolResult && a_boolRefreshDataset)
                {
                    RefreshDataSetAfterChanges();
                }
            }

            m_boolChangesSaved = l_boolResult;
            if( (a_boolRefreshDataset) && (MainWindow.MyDispatcher != null) )
            {
                MainWindow.MyDispatcher.Invoke(new Action(() => MainWindow.SelectMouseCursor(System.Windows.Input.Cursors.Wait)));
            }
            return l_boolResult;
        }

        /// <summary>
        /// Refreshes the current data set by re-issuing the last used search criteria to the
        /// host system.
        /// </summary>
        /// <remarks>
        /// Revision History
        /// Date     Who Defect# Description
        /// -------- --- ------- ----------------------------------------    
        /// 12/04/14 BJC         Initial Version
        /// </remarks>
        public void RefreshDataSetAfterChanges(int a_nNewNumPerPage = DEFAULT_NUM_PER_PAGE)
        {
            // save the current page number
            int l_nCurrentPage = m_dsInfo.PageNumber;

            int l_nPreValue = m_nNumItemsPerPage;

            if (a_nNewNumPerPage == 0)
            {
                m_nNumItemsPerPage = DEFAULT_NUM_PER_PAGE;
            }
            else
            {
                m_nNumItemsPerPage = a_nNewNumPerPage;
            }

            if (RequestDataSet(m_strLastSearchTerm, m_boolIncludeGeocoded))
            {
                if (l_nPreValue != m_nNumItemsPerPage)
                {
                    // if the number of items per page changes, just fetch the first page.  Any process
                    // that needs a different page should already have stored the page it wants to fetch
                    // and will (or should) fetch it following the refresh.
                    l_nCurrentPage = 1;
                }

                RequestDataSetPage(l_nCurrentPage);
            }
        }

        /// <summary>
        /// Adds an account record to the collection.
        /// </summary>
        /// <param name="l_xnode"></param>
        /// <remarks>
        /// Revision History
        /// Date     Who Defect# Description
        /// -------- --- ------- ----------------------------------------    
        /// 12/04/14 BJC         Initial Version
        /// </remarks>
        void AddAccountRecord(XmlNode l_xnode)
        {
            XmlNode item;
            string l_strName;
            string l_strData;
            double dValue;
            Accounts acct = new Accounts();
            try
            {
                item = l_xnode.FirstChild;
                do
                {
                    l_strName = item.Name;
                    l_strData = item.InnerText;
                    switch(l_strName)
                    {
                        case "MeterNumber":
                            acct.MeterNumber = l_strData;
                            break;
                        case "PremiseKey":
                            acct.PremiseKey = l_strData;
                            break;
                        case "Account":
                            acct.Account = l_strData;
                            break;
                        case "Name":
                            acct.Name = l_strData;
                            break;
                        case "Address":
                            acct.Address = l_strData;
                            break;
                        case "City":
                            acct.City = l_strData;
                            break;
                        case "State":
                            acct.State = l_strData;
                            break;
                        case "Zip":
                            acct.ZipCode = l_strData;
                            break;
                        case "Latitude":
                            double.TryParse(l_strData, out dValue);
                            acct.Latitude = dValue;
                            break;
                        case "Longitude":
                            double.TryParse(l_strData, out dValue);
                            acct.Longitude = dValue;
                            break;
                        case "GeoResult":
                            break;
                    }
                    item = item.NextSibling;
                } while (item != null);

                if ((acct.Latitude != 0) && (acct.Longitude != 0))
                {
                    acct.IsGeocoded = true;
                }

                acct.ChangesMade = false;
                acct = ValidateItemInQueuedList(acct);
                acct.UniqueIndex = AccountData.Count;
                AccountData.Add(acct);

            }
            catch(Exception)
            {
            }
        }

        int IndexOfQueuedItem(Accounts a_acct)
        {
            int l_nResult = -1;
            int index = 0;
            for (index = 0; index < QueuedAccountsForSave.Count; index++)
            {
                if ((a_acct.Account == QueuedAccountsForSave[index].Account) &&
                    (a_acct.PremiseKey == QueuedAccountsForSave[index].PremiseKey) &&
                    (a_acct.MeterNumber == QueuedAccountsForSave[index].MeterNumber))
                {
                    l_nResult = index;
                    break;
                }
            }

            return l_nResult;
        }

        bool IsAccountInQueuedList(Accounts a_acct)
        {
            bool l_boolResult = false;
            Accounts result = a_acct;
            int count =  (from i in QueuedAccountsForSave
                       where i.PremiseKey == a_acct.PremiseKey &&
                           i.MeterNumber == a_acct.MeterNumber &&
                           i.Account == a_acct.Account
                       select i).Count();
            if (count > 0)
            {
                l_boolResult = true;
            }
            return l_boolResult;
        }

        Accounts ValidateItemInQueuedList(Accounts a_item)
        {
            Accounts result = a_item;
            var rslt = from i in QueuedAccountsForSave where i.PremiseKey == a_item.PremiseKey && 
                           i.MeterNumber == a_item.MeterNumber && 
                           i.Account == a_item.Account select i;

            if (rslt.Count() > 0)
            {
                result = rslt.ElementAt(0);
            }

            return result;
        }

        /// <summary>
        /// Processes an XML document that contains a list of accounts from the host system.
        /// </summary>
        /// <param name="a_xmlDoc"></param>
        /// <remarks>
        /// Revision History
        /// Date     Who Defect# Description
        /// -------- --- ------- ----------------------------------------    
        /// 12/04/14 BJC         Initial Version
        /// </remarks>
        void RetrieveAccountsFromXML(XmlDocument a_xmlDoc)
        {
            XmlNodeList xl;

            //
            //The document was valid so now we can try and extract the number of results and query number from the document
            //
            xl = a_xmlDoc.GetElementsByTagName("Point");

            if ((xl != null) && (xl.Count > 0))
            {
                lock (m_dataLock)
                {
                    AccountData.Clear();
                    foreach (XmlNode node in xl)
                    {
                        AddAccountRecord(node);
                    }
                    m_boolChangesSaved = false;

                    SelectedItem = null;
                }
            }
        }

        /// <summary>
        /// Retrieves an account item at the specified index, if the index is valid
        /// </summary>
        /// <param name="a_nIndex">a valid index offset into the collection of account records</param>
        /// <returns></returns>
        /// <remarks>
        /// Revision History
        /// Date     Who Defect# Description
        /// -------- --- ------- ----------------------------------------    
        /// 12/04/14 BJC         Initial Version
        /// </remarks>
        public Accounts GetAccountItem(int a_nIndex)
        {
            if ((a_nIndex >= 0) && (a_nIndex < m_ocAccounts.Count))
            {
                Accounts item;
                lock (m_dataLock)
                {
                    item = m_ocAccounts[a_nIndex];
                }

                return item;
            }

            return null;
        }

        /// <summary>
        /// Sets the account item object at the specified index offset.
        /// </summary>
        /// <param name="a_nIndex"></param>
        /// <param name="a_item"></param>
        /// <remarks>
        /// Revision History
        /// Date     Who Defect# Description
        /// -------- --- ------- ----------------------------------------    
        /// 12/04/14 BJC         Initial Version
        /// </remarks>
        public void SetAccountItem(int a_nIndex, Accounts a_item)
        {
            if ((a_nIndex >= 0) && (a_nIndex < m_ocAccounts.Count))
            {
                lock (m_dataLock)
                {
                    m_ocAccounts[a_nIndex] = a_item;
                }
                // Indicate that we have unsaved changes
                m_boolChangesSaved = false;
            }
            
        }

        public void SelfTest()
        {
            BuildXMLTestString();
            XmlDocument xml = new XmlDocument();
            xml.LoadXml(m_strXMLTest);

            RetrieveAccountsFromXML(xml);

        }

        string m_strXMLTest;
        void BuildXMLTestString()
        {
            m_strXMLTest = "<tns:rowset xmlns:tns=\"http://10.8.75.254:8080/fixed_network\">";
            m_strXMLTest += "<tns:row>";
            m_strXMLTest += "<tns:lc_x005F_xml_out><GeocodeList>";
            m_strXMLTest += "<Point><MeterNumber></MeterNumber><PremiseKey>MAINTSHOP</PremiseKey><Account>28</Account><Name>SCHOOL DIST-MAINT SHOP</Name><Address>1214 AMB THOMPSON BLVD</Address><City></City><State></State><Zip></Zip><Latitude></Latitude><Longitude></Longitude><GeoResult></GeoResult></Point>";
            m_strXMLTest += "<Point><MeterNumber></MeterNumber><PremiseKey>945BENT</PremiseKey><Account>1344</Account><Name>DALLAS C MAIN</Name><Address>945 BENT AVENUE</Address><City></City><State></State><Zip></Zip><Latitude></Latitude><Longitude></Longitude><GeoResult></GeoResult></Point>";
            m_strXMLTest += "<Point><MeterNumber></MeterNumber><PremiseKey>01-00900-00    10001</PremiseKey><Account>01-00900-00</Account><Name>BEDINGFIELD, STEPHEN</Name><Address>101 MAIN E</Address><City></City><State></State><Zip></Zip><Latitude></Latitude><Longitude></Longitude><GeoResult></GeoResult></Point>";
            m_strXMLTest += "<Point><MeterNumber></MeterNumber><PremiseKey>01-00885-01    10001</PremiseKey><Account>01-00885-01</Account><Name>SCHOPFER, DORIS</Name><Address>104 MAIN E</Address><City></City><State></State><Zip></Zip><Latitude></Latitude><Longitude></Longitude><GeoResult></GeoResult></Point>";
            m_strXMLTest += "<Point><MeterNumber></MeterNumber><PremiseKey>01-00890-01    10001</PremiseKey><Account>01-00890-01</Account><Name>ROUSE, MELANIE</Name><Address>105 MAIN E</Address><City></City><State></State><Zip></Zip><Latitude></Latitude><Longitude></Longitude><GeoResult></GeoResult></Point>";
            m_strXMLTest += "<Point><MeterNumber></MeterNumber><PremiseKey>01-00880-00    10001</PremiseKey><Account>01-00880-00</Account><Name>LATHAM, ROSE</Name><Address>106 MAIN E</Address><City></City><State></State><Zip></Zip><Latitude></Latitude><Longitude></Longitude><GeoResult></GeoResult></Point>";
            m_strXMLTest += "<Point><MeterNumber></MeterNumber><PremiseKey>01-00855-08    10001</PremiseKey><Account>01-00855-08</Account><Name>WEIGART, TAMMY</Name><Address>107 MAIN E</Address><City></City><State></State><Zip></Zip><Latitude></Latitude><Longitude></Longitude><GeoResult></GeoResult></Point>";
            m_strXMLTest += "<Point><MeterNumber></MeterNumber><PremiseKey>01-00860-01    10001</PremiseKey><Account>01-00860-01</Account><Name>SPICER, HEATHER</Name><Address>201 MAIN E</Address><City></City><State></State><Zip></Zip><Latitude></Latitude><Longitude></Longitude><GeoResult></GeoResult></Point>";
            m_strXMLTest += "<Point><MeterNumber></MeterNumber><PremiseKey>01-00865-02    10001</PremiseKey><Account>01-00865-02</Account><Name>CHRISTMAS, MARYLEE</Name><Address>203 MAIN E</Address><City></City><State></State><Zip></Zip><Latitude></Latitude><Longitude></Longitude><GeoResult></GeoResult></Point>";
            m_strXMLTest += "<Point><MeterNumber></MeterNumber><PremiseKey>01-00870-00    10001</PremiseKey><Account>01-00870-00</Account><Name>MILLER, KENNETH</Name><Address>205 MAIN E</Address><City></City><State></State><Zip></Zip><Latitude></Latitude><Longitude></Longitude><GeoResult></GeoResult></Point>";
            m_strXMLTest += "<Point><MeterNumber></MeterNumber><PremiseKey>1005813</PremiseKey><Account>00025533</Account><Name>ERICA CAIN</Name><Address>4 MAIN ST</Address><City></City><State></State><Zip></Zip><Latitude></Latitude><Longitude></Longitude><GeoResult></GeoResult></Point>";
            m_strXMLTest += "<Point><MeterNumber></MeterNumber><PremiseKey>1362</PremiseKey><Account>1362</Account><Name>Cheryl&apos;s test</Name><Address>123 main street.</Address><City>tallasse</City><State>al</State><Zip>36078</Zip><Latitude>32.425825</Latitude><Longitude>-85.705293</Longitude><GeoResult></GeoResult></Point>";
            m_strXMLTest += "<Point><MeterNumber></MeterNumber><PremiseKey>018</PremiseKey><Account>0-18</Account><Name>R450v1a Base 300.1</Name><Address>1 Main Street</Address><City>Test City</City><State>AL</State><Zip>12345</Zip><Latitude></Latitude><Longitude></Longitude><GeoResult></GeoResult></Point>";
            m_strXMLTest += "<Point><MeterNumber></MeterNumber><PremiseKey>019</PremiseKey><Account>0-19</Account><Name>R450v1a Base 300.1</Name><Address>1 Main Street</Address><City>Test City</City><State>AL</State><Zip>12345</Zip><Latitude></Latitude><Longitude></Longitude><GeoResult></GeoResult></Point>";
            m_strXMLTest += "<Point><MeterNumber></MeterNumber><PremiseKey>020</PremiseKey><Account>0-20</Account><Name>R450v1a Base 300.1</Name><Address>1 Main Street</Address><City>Test City</City><State>AL</State><Zip>12345</Zip><Latitude></Latitude><Longitude></Longitude><GeoResult></GeoResult></Point>";
            m_strXMLTest += "<Point><MeterNumber></MeterNumber><PremiseKey>021</PremiseKey><Account>0-21</Account><Name>R450v1a Base 300.1</Name><Address>1 Main Street</Address><City>Test City</City><State>AL</State><Zip>12345</Zip><Latitude></Latitude><Longitude></Longitude><GeoResult></GeoResult></Point>";
            m_strXMLTest += "<Point><MeterNumber></MeterNumber><PremiseKey>022</PremiseKey><Account>0-22</Account><Name>R450v1a Base 300.1</Name><Address>1 Main Street</Address><City>Test City</City><State>AL</State><Zip>12345</Zip><Latitude></Latitude><Longitude></Longitude><GeoResult></GeoResult></Point>";
            m_strXMLTest += "<Point><MeterNumber></MeterNumber><PremiseKey>023</PremiseKey><Account>0-23</Account><Name>R450v1a Base 300.1</Name><Address>1 Main Street</Address><City>Test City</City><State>AL</State><Zip>12345</Zip><Latitude></Latitude><Longitude></Longitude><GeoResult></GeoResult></Point>";
            m_strXMLTest += "<Point><MeterNumber></MeterNumber><PremiseKey>024</PremiseKey><Account>0-24</Account><Name>R450v1a Base 300.1</Name><Address>1 Main Street</Address><City>Test City</City><State>AL</State><Zip>12345</Zip><Latitude></Latitude><Longitude></Longitude><GeoResult></GeoResult></Point>";
            m_strXMLTest += "<Point><MeterNumber></MeterNumber><PremiseKey>024</PremiseKey><Account>0-24</Account><Name>R450v1a Base 300.1</Name><Address>1 Main Street</Address><City>Test City</City><State>AL</State><Zip>12345</Zip><Latitude></Latitude><Longitude></Longitude><GeoResult></GeoResult></Point>";
            m_strXMLTest += "<Point><MeterNumber></MeterNumber><PremiseKey>025</PremiseKey><Account>0-25</Account><Name>R450v1a Base 300.1</Name><Address>1 Main Street</Address><City>Test City</City><State>AL</State><Zip>12345</Zip><Latitude></Latitude><Longitude></Longitude><GeoResult></GeoResult></Point>";
            m_strXMLTest += "<Point><MeterNumber></MeterNumber><PremiseKey>025</PremiseKey><Account>0-25</Account><Name>R450v1a Base 300.1</Name><Address>1 Main Street</Address><City>Test City</City><State>AL</State><Zip>12345</Zip><Latitude></Latitude><Longitude></Longitude><GeoResult></GeoResult></Point>";
            m_strXMLTest += "<Point><MeterNumber></MeterNumber><PremiseKey>026</PremiseKey><Account>0-26</Account><Name>R450v1a Base 300.1</Name><Address>1 Main Street</Address><City>Test City</City><State>AL</State><Zip>12345</Zip><Latitude></Latitude><Longitude></Longitude><GeoResult></GeoResult></Point>";
            m_strXMLTest += "<Point><MeterNumber></MeterNumber><PremiseKey>027</PremiseKey><Account>0-27</Account><Name>R450v1a Base 300.1</Name><Address>1 Main Street</Address><City>Test City</City><State>AL</State><Zip>12345</Zip><Latitude></Latitude><Longitude></Longitude><GeoResult></GeoResult></Point>";
            m_strXMLTest += "<Point><MeterNumber></MeterNumber><PremiseKey>0251</PremiseKey><Account>025-1</Account><Name>R450v1a Base 250.2</Name><Address>1 Main Street</Address><City>Test City</City><State>AL</State><Zip>36078</Zip><Latitude></Latitude><Longitude></Longitude><GeoResult></GeoResult></Point>";
            m_strXMLTest += "<Point><MeterNumber></MeterNumber><PremiseKey>0252</PremiseKey><Account>025-2</Account><Name>R450v1a Base 250.2</Name><Address>1 Main Street</Address><City>Test City</City><State>AL</State><Zip>36078</Zip><Latitude></Latitude><Longitude></Longitude><GeoResult></GeoResult></Point>";
            m_strXMLTest += "<Point><MeterNumber></MeterNumber><PremiseKey>0253</PremiseKey><Account>025-3</Account><Name>R450v1a Base 250.2</Name><Address>1 Main Street</Address><City>Test City</City><State>AL</State><Zip>36078</Zip><Latitude></Latitude><Longitude></Longitude><GeoResult></GeoResult></Point>";
            m_strXMLTest += "<Point><MeterNumber></MeterNumber><PremiseKey>0254</PremiseKey><Account>025-4</Account><Name>R450v1a Base 250.2</Name><Address>1 Main Street</Address><City>Test City</City><State>AL</State><Zip>36078</Zip><Latitude></Latitude><Longitude></Longitude><GeoResult></GeoResult></Point>";
            m_strXMLTest += "<Point><MeterNumber></MeterNumber><PremiseKey>0255</PremiseKey><Account>025-5</Account><Name>R450v1a Base 250.2</Name><Address>1 Main Street</Address><City>Test City</City><State>AL</State><Zip>36078</Zip><Latitude></Latitude><Longitude></Longitude><GeoResult></GeoResult></Point>";
            m_strXMLTest += "<Point><MeterNumber></MeterNumber><PremiseKey>0255</PremiseKey><Account>025-5</Account><Name>R450v1a Base 250.2</Name><Address>1 Main Street</Address><City>Test City</City><State>AL</State><Zip>36078</Zip><Latitude></Latitude><Longitude></Longitude><GeoResult></GeoResult></Point>";
            m_strXMLTest += "<Point><MeterNumber></MeterNumber><PremiseKey>0256</PremiseKey><Account>025-6</Account><Name>R450v1a Base 250.2</Name><Address>1 Main Street</Address><City>Test City</City><State>AL</State><Zip>36078</Zip><Latitude></Latitude><Longitude></Longitude><GeoResult></GeoResult></Point>";
            m_strXMLTest += "<Point><MeterNumber></MeterNumber><PremiseKey>0257</PremiseKey><Account>025-7</Account><Name>R450v1a Base 250.2</Name><Address>1 Main Street</Address><City>Test City</City><State>AL</State><Zip>36078</Zip><Latitude></Latitude><Longitude></Longitude><GeoResult></GeoResult></Point>";
            m_strXMLTest += "<Point><MeterNumber></MeterNumber><PremiseKey>0258</PremiseKey><Account>025-8</Account><Name>R450v1a Base 250.2</Name><Address>1 Main Street</Address><City>Test City</City><State>AL</State><Zip>36078</Zip><Latitude></Latitude><Longitude></Longitude><GeoResult></GeoResult></Point>";
            m_strXMLTest += "<Point><MeterNumber></MeterNumber><PremiseKey>0258</PremiseKey><Account>025-8</Account><Name>R450v1a Base 250.2</Name><Address>1 Main Street</Address><City>Test City</City><State>AL</State><Zip>36078</Zip><Latitude></Latitude><Longitude></Longitude><GeoResult></GeoResult></Point>";
            m_strXMLTest += "<Point><MeterNumber></MeterNumber><PremiseKey>033</PremiseKey><Account>0-33</Account><Name>R450v1a Base 300.2</Name><Address>3 Main Street</Address><City>Test City</City><State>AL</State><Zip>12345</Zip><Latitude></Latitude><Longitude></Longitude><GeoResult></GeoResult></Point>";
            m_strXMLTest += "<Point><MeterNumber></MeterNumber><PremiseKey>038</PremiseKey><Account>0-38</Account><Name>R450v1a Base 300.2</Name><Address>3 Main Street</Address><City>Test City</City><State>AL</State><Zip>12345</Zip><Latitude></Latitude><Longitude></Longitude><GeoResult></GeoResult></Point>";
            m_strXMLTest += "<Point><MeterNumber></MeterNumber><PremiseKey>039</PremiseKey><Account>0-39</Account><Name>R450v1a Base 300.2</Name><Address>3 Main Street</Address><City>Test City</City><State>AL</State><Zip>12345</Zip><Latitude></Latitude><Longitude></Longitude><GeoResult></GeoResult></Point>";
            m_strXMLTest += "<Point><MeterNumber></MeterNumber><PremiseKey>44334343</PremiseKey><Account>44334343</Account><Name>Inactive Account</Name><Address>123 Main Street</Address><City>Montgomery</City><State>AL</State><Zip>36116</Zip><Latitude>32.253642</Latitude><Longitude>-86.235149</Longitude><GeoResult></GeoResult></Point>";
            m_strXMLTest += "<Point><MeterNumber></MeterNumber><PremiseKey>9990</PremiseKey><Account>9990</Account><Name>ccottrell</Name><Address>123 main street</Address><City>test city</City><State>33</State><Zip>36078</Zip><Latitude></Latitude><Longitude></Longitude><GeoResult></GeoResult></Point>";
            m_strXMLTest += "<Point><MeterNumber></MeterNumber><PremiseKey>9990</PremiseKey><Account>9990</Account><Name>ccottrell</Name><Address>123 main street</Address><City>test city</City><State>33</State><Zip>36078</Zip><Latitude></Latitude><Longitude></Longitude><GeoResult></GeoResult></Point>";
            m_strXMLTest += "<Point><MeterNumber></MeterNumber><PremiseKey>9990</PremiseKey><Account>9990</Account><Name>ccottrell</Name><Address>123 main street</Address><City>test city</City><State>33</State><Zip>36078</Zip><Latitude></Latitude><Longitude></Longitude><GeoResult></GeoResult></Point>";
            m_strXMLTest += "<Point><MeterNumber></MeterNumber><PremiseKey>9990</PremiseKey><Account>9990</Account><Name>ccottrell</Name><Address>123 main street</Address><City>test city</City><State>33</State><Zip>36078</Zip><Latitude></Latitude><Longitude></Longitude><GeoResult></GeoResult></Point>";
            m_strXMLTest += "<Point><MeterNumber></MeterNumber><PremiseKey>9990</PremiseKey><Account>9990</Account><Name>ccottrell</Name><Address>123 main street</Address><City>test city</City><State>33</State><Zip>36078</Zip><Latitude></Latitude><Longitude></Longitude><GeoResult></GeoResult></Point>";
            m_strXMLTest += "<Point><MeterNumber></MeterNumber><PremiseKey>9990</PremiseKey><Account>9990</Account><Name>ccottrell</Name><Address>123 main street</Address><City>test city</City><State>33</State><Zip>36078</Zip><Latitude></Latitude><Longitude></Longitude><GeoResult></GeoResult></Point>";
            m_strXMLTest += "<Point><MeterNumber></MeterNumber><PremiseKey>110401</PremiseKey><Account>110401</Account><Name>Testing new acount</Name><Address>123 Main Street</Address><City>Tallassee</City><State>AL</State><Zip>36078</Zip><Latitude></Latitude><Longitude></Longitude><GeoResult></GeoResult></Point>";
            m_strXMLTest += "<Point><MeterNumber></MeterNumber><PremiseKey>110401</PremiseKey><Account>110401</Account><Name>Testing new acount</Name><Address>123 Main Street</Address><City>Tallassee</City><State>AL</State><Zip>36078</Zip><Latitude></Latitude><Longitude></Longitude><GeoResult></GeoResult></Point>";
            m_strXMLTest += "<Point><MeterNumber></MeterNumber><PremiseKey>110902</PremiseKey><Account>110902</Account><Name>New Account 2</Name><Address>123 Main Street</Address><City>Montgomery, AL</City><State>AL</State><Zip>36106</Zip><Latitude></Latitude><Longitude></Longitude><GeoResult></GeoResult></Point>";
            m_strXMLTest += "<Point><MeterNumber>12064</MeterNumber><PremiseKey>12064</PremiseKey><Account>12064</Account><Name>FBLoad XML</Name><Address>123 Main Street</Address><City>Tallassee</City><State>AL</State><Zip>36078</Zip><Latitude>32.535968</Latitude><Longitude>-85.893292</Longitude><GeoResult></GeoResult></Point>";
            m_strXMLTest += "<Point><MeterNumber>12064</MeterNumber><PremiseKey>12070</PremiseKey><Account>12070</Account><Name>FBLoad XML</Name><Address>123 Main Street</Address><City>Tallassee</City><State>AL</State><Zip>36078</Zip><Latitude>32.535968</Latitude><Longitude>-85.893292</Longitude><GeoResult></GeoResult></Point>";
            m_strXMLTest += "<Point><MeterNumber>12064</MeterNumber><PremiseKey>GEN_0000000103</PremiseKey><Account>12064</Account><Name>FBLoad XML</Name><Address>123 Main Street</Address><City>Tallassee</City><State>AL</State><Zip>36078</Zip><Latitude>32.535968</Latitude><Longitude>-85.893292</Longitude><GeoResult></GeoResult></Point>";
            m_strXMLTest += "<Point><MeterNumber>54467</MeterNumber><PremiseKey>111019</PremiseKey><Account>111019</Account><Name>2.0.111018 New Account</Name><Address>123 Main Street</Address><City>Tallassee</City><State>AL</State><Zip>36078</Zip><Latitude>32.526589</Latitude><Longitude>-85.235684</Longitude><GeoResult></GeoResult></Point>";
            m_strXMLTest += "<Point><MeterNumber>110906</MeterNumber><PremiseKey>110906-2</PremiseKey><Account>110906-2</Account><Name>Test Account 2</Name><Address>123 Main Street</Address><City>Nashville</City><State>TN</State><Zip></Zip><Latitude></Latitude><Longitude></Longitude><GeoResult></GeoResult></Point>";
            m_strXMLTest += "<Point><MeterNumber>111208</MeterNumber><PremiseKey>111208</PremiseKey><Account>111208</Account><Name>FBLoad XML</Name><Address>123 Main Street</Address><City>Tallassee</City><State>AL</State><Zip>36078</Zip><Latitude>32.535968</Latitude><Longitude>-85.893292</Longitude><GeoResult></GeoResult></Point>";
            m_strXMLTest += "<Point><MeterNumber>111208</MeterNumber><PremiseKey>111208-2</PremiseKey><Account>111208-2</Account><Name>FBLoad XML</Name><Address>123 Main Street</Address><City>Tallassee</City><State>AL</State><Zip>36078</Zip><Latitude>32.535968</Latitude><Longitude>-85.893292</Longitude><GeoResult></GeoResult></Point>";
            m_strXMLTest += "<Point><MeterNumber>111208</MeterNumber><PremiseKey>111208-3</PremiseKey><Account>111208-3</Account><Name>FBLoad XML</Name><Address>123 Main Street</Address><City>Tallassee</City><State>AL</State><Zip>36078</Zip><Latitude>32.535968</Latitude><Longitude>-85.893292</Longitude><GeoResult></GeoResult></Point>";
            m_strXMLTest += "<Point><MeterNumber>120611</MeterNumber><PremiseKey>120611</PremiseKey><Account>120611</Account><Name>Test 1 FBLoad XML</Name><Address>123 Main Street</Address><City>Tallassee</City><State>AL</State><Zip>36078</Zip><Latitude>32.535968</Latitude><Longitude>-85.893292</Longitude><GeoResult></GeoResult></Point>";
            m_strXMLTest += "<Point><MeterNumber>120811</MeterNumber><PremiseKey>120811-4</PremiseKey><Account>120811-4</Account><Name>Test 1 Add Group</Name><Address>123 Main Street</Address><City>Tallassee</City><State>AL</State><Zip>36078</Zip><Latitude>32.535968</Latitude><Longitude>-85.893292</Longitude><GeoResult></GeoResult></Point>";
            m_strXMLTest += "<Point><MeterNumber>120811</MeterNumber><PremiseKey>120811-5</PremiseKey><Account>120811-5</Account><Name>Test 1 Add Group</Name><Address>123 Main Street</Address><City>Tallassee</City><State>AL</State><Zip>36078</Zip><Latitude>32.535968</Latitude><Longitude>-85.893292</Longitude><GeoResult></GeoResult></Point>";
            m_strXMLTest += "<Point><MeterNumber>120811</MeterNumber><PremiseKey>120811-6</PremiseKey><Account>120811-6</Account><Name>Test 1 Add Group</Name><Address>123 Main Street</Address><City>Tallassee</City><State>AL</State><Zip>36078</Zip><Latitude>32.535968</Latitude><Longitude>-85.893292</Longitude><GeoResult></GeoResult></Point>";
            m_strXMLTest += "<Point><MeterNumber>120811</MeterNumber><PremiseKey>120811-7</PremiseKey><Account>120811-7</Account><Name>Test 2 Add Group</Name><Address>123 Main Street</Address><City>Tallassee</City><State>AL</State><Zip>36078</Zip><Latitude>32.535968</Latitude><Longitude>-85.893292</Longitude><GeoResult></GeoResult></Point>";
            m_strXMLTest += "<Point><MeterNumber>120811</MeterNumber><PremiseKey>GEN_0000000104</PremiseKey><Account>120911</Account><Name>Test 2 Add Group</Name><Address>123 Main Street</Address><City>Tallassee</City><State>AL</State><Zip>36078</Zip><Latitude>32.535968</Latitude><Longitude>-85.893292</Longitude><GeoResult></GeoResult></Point>";
            m_strXMLTest += "<Point><MeterNumber>120911</MeterNumber><PremiseKey>1209112</PremiseKey><Account>1209112</Account><Name>Test 2 Add Group</Name><Address>123 Main Street</Address><City>Tallassee</City><State>AL</State><Zip>36078</Zip><Latitude>32.535968</Latitude><Longitude>-85.893292</Longitude><GeoResult></GeoResult></Point>";
            m_strXMLTest += "<Point><MeterNumber>123545</MeterNumber><PremiseKey>110707</PremiseKey><Account>110707</Account><Name>Move MIU History</Name><Address>123 Main Street</Address><City>Plano</City><State>TX</State><Zip></Zip><Latitude></Latitude><Longitude></Longitude><GeoResult></GeoResult></Point>";
            m_strXMLTest += "<Point><MeterNumber>554434</MeterNumber><PremiseKey>022012</PremiseKey><Account>022012</Account><Name>Soft Disconnect</Name><Address>123 Main Street</Address><City>Ozark</City><State>AL</State><Zip>36063</Zip><Latitude>31.459058</Latitude><Longitude>-85.640493</Longitude><GeoResult></GeoResult></Point>";
            m_strXMLTest += "<Point><MeterNumber></MeterNumber><PremiseKey>05-05075-01    10001</PremiseKey><Account>05-05075-01</Account><Name>HAWKINS, JAMES/GAIL</Name><Address>1403 MAIN TERR W</Address><City></City><State></State><Zip></Zip><Latitude></Latitude><Longitude></Longitude><GeoResult></GeoResult></Point>";
            m_strXMLTest += "<Point><MeterNumber></MeterNumber><PremiseKey>05-05230-02    10001</PremiseKey><Account>05-05230-02</Account><Name>MCCURLEY, SIDNEY</Name><Address>1404 MAIN TERR W</Address><City></City><State></State><Zip></Zip><Latitude></Latitude><Longitude></Longitude><GeoResult></GeoResult></Point>";
            m_strXMLTest += "<Point><MeterNumber></MeterNumber><PremiseKey>05-05080-00    10001</PremiseKey><Account>05-05080-00</Account><Name>WHITMAN, HARVEY</Name><Address>1405 MAIN TERR W</Address><City></City><State></State><Zip></Zip><Latitude></Latitude><Longitude></Longitude><GeoResult></GeoResult></Point>";
            m_strXMLTest += "<Point><MeterNumber></MeterNumber><PremiseKey>05-05225-00    10001</PremiseKey><Account>05-05225-00</Account><Name>WALTERS, BETTY</Name><Address>1406 MAIN TERR W</Address><City></City><State></State><Zip></Zip><Latitude></Latitude><Longitude></Longitude><GeoResult></GeoResult></Point>";
            m_strXMLTest += "<Point><MeterNumber></MeterNumber><PremiseKey>05-05085-01    10001</PremiseKey><Account>05-05085-01</Account><Name>O&apos;CONOR, IRENE</Name><Address>1407 MAIN TERR W</Address><City></City><State></State><Zip></Zip><Latitude></Latitude><Longitude></Longitude><GeoResult></GeoResult></Point>";
            m_strXMLTest += "<Point><MeterNumber></MeterNumber><PremiseKey>05-05090-00    10001</PremiseKey><Account>05-05090-00</Account><Name>BRILL, CHARLES</Name><Address>1409 MAIN TERR W</Address><City></City><State></State><Zip></Zip><Latitude></Latitude><Longitude></Longitude><GeoResult></GeoResult></Point>";
            m_strXMLTest += "<Point><MeterNumber></MeterNumber><PremiseKey>05-05215-03    10001</PremiseKey><Account>05-05215-03</Account><Name>VIOLETT, AIMEE</Name><Address>1500 MAIN TERR W</Address><City></City><State></State><Zip></Zip><Latitude></Latitude><Longitude></Longitude><GeoResult></GeoResult></Point>";
            m_strXMLTest += "<Point><MeterNumber></MeterNumber><PremiseKey>05-05210-00    10001</PremiseKey><Account>05-05210-00</Account><Name>WILMES, NORBERT</Name><Address>1502 MAIN TERR W</Address><City></City><State></State><Zip></Zip><Latitude></Latitude><Longitude></Longitude><GeoResult></GeoResult></Point>";
            m_strXMLTest += "<Point><MeterNumber></MeterNumber><PremiseKey>05-05095-02    10001</PremiseKey><Account>05-05095-02</Account><Name>WILLIAMS, CURTIS</Name><Address>1503 MAIN TERR W</Address><City></City><State></State><Zip></Zip><Latitude></Latitude><Longitude></Longitude><GeoResult></GeoResult></Point>";
            m_strXMLTest += "<Point><MeterNumber></MeterNumber><PremiseKey>05-05205-00    10001</PremiseKey><Account>05-05205-00</Account><Name>HAGAN, JIM</Name><Address>1504 MAIN TERR W</Address><City></City><State></State><Zip></Zip><Latitude></Latitude><Longitude></Longitude><GeoResult></GeoResult></Point>";
            m_strXMLTest += "<Point><MeterNumber></MeterNumber><PremiseKey>05-05100-00    10001</PremiseKey><Account>05-05100-00</Account><Name>COMPTON, JOHN</Name><Address>1505 MAIN TERR W</Address><City></City><State></State><Zip></Zip><Latitude></Latitude><Longitude></Longitude><GeoResult></GeoResult></Point>";
            m_strXMLTest += "<Point><MeterNumber></MeterNumber><PremiseKey>05-05200-00    10001</PremiseKey><Account>05-05200-00</Account><Name>CLOWSER, JOAN</Name><Address>1506 MAIN TERR W</Address><City></City><State></State><Zip></Zip><Latitude></Latitude><Longitude></Longitude><GeoResult></GeoResult></Point>";
            m_strXMLTest += "<Point><MeterNumber></MeterNumber><PremiseKey>05-05105-04    10001</PremiseKey><Account>05-05105-04</Account><Name>MARCUM, JAMIE</Name><Address>1507 MAIN TERR W</Address><City></City><State></State><Zip></Zip><Latitude></Latitude><Longitude></Longitude><GeoResult></GeoResult></Point>";
            m_strXMLTest += "<Point><MeterNumber></MeterNumber><PremiseKey>05-05110-02    10001</PremiseKey><Account>05-05110-02</Account><Name>WILSON, JAMES</Name><Address>1509 MAIN TERR W</Address><City></City><State></State><Zip></Zip><Latitude></Latitude><Longitude></Longitude><GeoResult></GeoResult></Point>";
            m_strXMLTest += "<Point><MeterNumber></MeterNumber><PremiseKey>05-05155-00    10001</PremiseKey><Account>05-05155-00</Account><Name>THORBURN, CHARLES</Name><Address>1601 MAIN TERR W</Address><City></City><State></State><Zip></Zip><Latitude></Latitude><Longitude></Longitude><GeoResult></GeoResult></Point>";
            m_strXMLTest += "<Point><MeterNumber></MeterNumber><PremiseKey>05-05190-01    10001</PremiseKey><Account>05-05190-01</Account><Name>TUCKER, DAVID &amp; LEA</Name><Address>1602 MAIN TERR W</Address><City></City><State></State><Zip></Zip><Latitude></Latitude><Longitude></Longitude><GeoResult></GeoResult></Point>";
            m_strXMLTest += "<Point><MeterNumber></MeterNumber><PremiseKey>05-05160-00    10001</PremiseKey><Account>05-05160-00</Account><Name>HECTOR, DONALD</Name><Address>1603 MAIN TERR W</Address><City></City><State></State><Zip></Zip><Latitude></Latitude><Longitude></Longitude><GeoResult></GeoResult></Point>";
            m_strXMLTest += "<Point><MeterNumber></MeterNumber><PremiseKey>05-05185-01    10001</PremiseKey><Account>05-05185-01</Account><Name>VESSAR, DON AND JOANI</Name><Address>1604 MAIN TERR W</Address><City></City><State></State><Zip></Zip><Latitude></Latitude><Longitude></Longitude><GeoResult></GeoResult></Point>";
            m_strXMLTest += "<Point><MeterNumber></MeterNumber><PremiseKey>05-05165-01    10001</PremiseKey><Account>05-05165-01</Account><Name>BARSCH, CLETUS</Name><Address>1605 MAIN TERR W</Address><City></City><State></State><Zip></Zip><Latitude></Latitude><Longitude></Longitude><GeoResult></GeoResult></Point>";
            m_strXMLTest += "<Point><MeterNumber></MeterNumber><PremiseKey>05-05180-00    10001</PremiseKey><Account>05-05180-00</Account><Name>PRATER, DANA</Name><Address>1606 MAIN TERR W</Address><City></City><State></State><Zip></Zip><Latitude></Latitude><Longitude></Longitude><GeoResult></GeoResult></Point>";
            m_strXMLTest += "<Point><MeterNumber></MeterNumber><PremiseKey>05-05175-04    10001</PremiseKey><Account>05-05175-04</Account><Name>DIERENFELDT, DANIELLE</Name><Address>1608 MAIN TERR W</Address><City></City><State></State><Zip></Zip><Latitude></Latitude><Longitude></Longitude><GeoResult></GeoResult></Point>";
            m_strXMLTest += "<Point><MeterNumber></MeterNumber><PremiseKey>05-05170-06    10001</PremiseKey><Account>05-05170-06</Account><Name>POLSKY, BRUCE</Name><Address>1610 MAIN TERR W</Address><City></City><State></State><Zip></Zip><Latitude></Latitude><Longitude></Longitude><GeoResult></GeoResult></Point>";
            m_strXMLTest += "<Point><MeterNumber></MeterNumber><PremiseKey>01-00920-03    10001</PremiseKey><Account>01-00920-03</Account><Name>BYE, AMBER</Name><Address>101 MAIN W</Address><City></City><State></State><Zip></Zip><Latitude></Latitude><Longitude></Longitude><GeoResult></GeoResult></Point>";
            m_strXMLTest += "<Point><MeterNumber></MeterNumber><PremiseKey>01-01000-03    10001</PremiseKey><Account>01-01000-03</Account><Name>HEDRICK, MARC</Name><Address>102 MAIN W</Address><City></City><State></State><Zip></Zip><Latitude></Latitude><Longitude></Longitude><GeoResult></GeoResult></Point>";
            m_strXMLTest += "<Point><MeterNumber></MeterNumber><PremiseKey>01-00925-00    10001</PremiseKey><Account>01-00925-00</Account><Name>ROBERTSON, RONDLE</Name><Address>103 MAIN W</Address><City></City><State></State><Zip></Zip><Latitude></Latitude><Longitude></Longitude><GeoResult></GeoResult></Point>";
            m_strXMLTest += "<Point><MeterNumber></MeterNumber><PremiseKey>01-00930-01    10001</PremiseKey><Account>01-00930-01</Account><Name>KNECHTENHOFER, ERIEN</Name><Address>105 MAIN W</Address><City></City><State></State><Zip></Zip><Latitude></Latitude><Longitude></Longitude><GeoResult></GeoResult></Point>";
            m_strXMLTest += "<Point><MeterNumber></MeterNumber><PremiseKey>01-00990-01    10001</PremiseKey><Account>01-00990-01</Account><Name>DAVIS, MARY/MELVIN</Name><Address>106 MAIN W</Address><City></City><State></State><Zip></Zip><Latitude></Latitude><Longitude></Longitude><GeoResult></GeoResult></Point>";
            m_strXMLTest += "<Point><MeterNumber></MeterNumber><PremiseKey>01-00935-00    10001</PremiseKey><Account>01-00935-00</Account><Name>METHODIST CHURCH</Name><Address>107 MAIN W</Address><City></City><State></State><Zip></Zip><Latitude></Latitude><Longitude></Longitude><GeoResult></GeoResult></Point>";
            m_strXMLTest += "<Point><MeterNumber></MeterNumber><PremiseKey>01-00985-04    10001</PremiseKey><Account>01-00985-04</Account><Name>MURPHY, CLARISSA</Name><Address>108 MAIN W</Address><City></City><State></State><Zip></Zip><Latitude></Latitude><Longitude></Longitude><GeoResult></GeoResult></Point>";
            m_strXMLTest += "<Point><MeterNumber></MeterNumber><PremiseKey>01-00940-00    10001</PremiseKey><Account>01-00940-00</Account><Name>WORTHWINE, RANDI</Name><Address>201 MAIN W</Address><City></City><State></State><Zip></Zip><Latitude></Latitude><Longitude></Longitude><GeoResult></GeoResult></Point>";
            m_strXMLTest += "<Point><MeterNumber></MeterNumber><PremiseKey>01-00980-02    10001</PremiseKey><Account>01-00980-02</Account><Name>SHAVNORE, JACKIE</Name><Address>202 MAIN W</Address><City></City><State></State><Zip></Zip><Latitude></Latitude><Longitude></Longitude><GeoResult></GeoResult></Point>";
            m_strXMLTest += "<Point><MeterNumber></MeterNumber><PremiseKey>01-00975-06    10001</PremiseKey><Account>01-00975-06</Account><Name>GARCIA, GABRIEL</Name><Address>204 MAIN W</Address><City></City><State></State><Zip></Zip><Latitude></Latitude><Longitude></Longitude><GeoResult></GeoResult></Point>";
            m_strXMLTest += "<Point><MeterNumber></MeterNumber><PremiseKey>01-00970-02    10001</PremiseKey><Account>01-00970-02</Account><Name>SMITH, JANET</Name><Address>206 MAIN W</Address><City></City><State></State><Zip></Zip><Latitude></Latitude><Longitude></Longitude><GeoResult></GeoResult></Point>";
            m_strXMLTest += "<Point><MeterNumber></MeterNumber><PremiseKey>01-00965-00    10001</PremiseKey><Account>01-00965-00</Account><Name>DAVISON, BILL</Name><Address>208 MAIN W</Address><City></City><State></State><Zip></Zip><Latitude></Latitude><Longitude></Longitude><GeoResult></GeoResult></Point>";
            m_strXMLTest += "<Point><MeterNumber></MeterNumber><PremiseKey>01-00960-02    10001</PremiseKey><Account>01-00960-02</Account><Name>JOHNSON, WALTER</Name><Address>210 MAIN W</Address><City></City><State></State><Zip></Zip><Latitude></Latitude><Longitude></Longitude><GeoResult></GeoResult></Point>";
            m_strXMLTest += "<Point><MeterNumber></MeterNumber><PremiseKey>01-00955-00    10001</PremiseKey><Account>01-00955-00</Account><Name>HUMMER, RODNEY</Name><Address>212 MAIN W</Address><City></City><State></State><Zip></Zip><Latitude></Latitude><Longitude></Longitude><GeoResult></GeoResult></Point>";
            m_strXMLTest += "<Point><MeterNumber></MeterNumber><PremiseKey>05-05585-00    10001</PremiseKey><Account>05-05585-00</Account><Name>BALES, EUGENE</Name><Address>1000 MAIN W</Address><City></City><State></State><Zip></Zip><Latitude></Latitude><Longitude></Longitude><GeoResult></GeoResult></Point>";
            m_strXMLTest += "<Point><MeterNumber></MeterNumber><PremiseKey>05-05580-04    10001</PremiseKey><Account>05-05580-04</Account><Name>KERNES, ALISHA</Name><Address>1002 MAIN W</Address><City></City><State></State><Zip></Zip><Latitude></Latitude><Longitude></Longitude><GeoResult></GeoResult></Point>";
            m_strXMLTest += "<Point><MeterNumber></MeterNumber><PremiseKey>05-04070-00    10001</PremiseKey><Account>05-04070-00</Account><Name>POLSGROVE, RICK</Name><Address>1003 MAIN W</Address><City></City><State></State><Zip></Zip><Latitude></Latitude><Longitude></Longitude><GeoResult></GeoResult></Point>";
            m_strXMLTest += "<Point><MeterNumber></MeterNumber><PremiseKey>05-04075-02    10001</PremiseKey><Account>05-04075-02</Account><Name>JONES, DAVID</Name><Address>1005 MAIN W</Address><City></City><State></State><Zip></Zip><Latitude></Latitude><Longitude></Longitude><GeoResult></GeoResult></Point>";
            m_strXMLTest += "<Point><MeterNumber></MeterNumber><PremiseKey>05-05575-02    10001</PremiseKey><Account>05-05575-02</Account><Name>MILSAP, CASS</Name><Address>1006 MAIN W</Address><City></City><State></State><Zip></Zip><Latitude></Latitude><Longitude></Longitude><GeoResult></GeoResult></Point>";
            m_strXMLTest += "<Point><MeterNumber></MeterNumber><PremiseKey>05-05570-01    10001</PremiseKey><Account>05-05570-01</Account><Name>OLIVER, GARY</Name><Address>1100 MAIN W</Address><City></City><State></State><Zip></Zip><Latitude></Latitude><Longitude></Longitude><GeoResult></GeoResult></Point>";
            m_strXMLTest += "<Point><MeterNumber></MeterNumber><PremiseKey>05-04080-00    10001</PremiseKey><Account>05-04080-00</Account><Name>CORMAN, DAVE</Name><Address>1101 MAIN W</Address><City></City><State></State><Zip></Zip><Latitude></Latitude><Longitude></Longitude><GeoResult></GeoResult></Point>";
            m_strXMLTest += "<Point><MeterNumber></MeterNumber><PremiseKey>05-05565-02    10001</PremiseKey><Account>05-05565-02</Account><Name>KERNS, HAROLD</Name><Address>1102 MAIN W</Address><City></City><State></State><Zip></Zip><Latitude></Latitude><Longitude></Longitude><GeoResult></GeoResult></Point>";
            m_strXMLTest += "<Point><MeterNumber></MeterNumber><PremiseKey>05-04085-03    10001</PremiseKey><Account>05-04085-03</Account><Name>SILKETT, JOHNNIE</Name><Address>1103 MAIN W</Address><City></City><State></State><Zip></Zip><Latitude></Latitude><Longitude></Longitude><GeoResult></GeoResult></Point>";
            m_strXMLTest += "<Point><MeterNumber></MeterNumber><PremiseKey>05-05560-03    10001</PremiseKey><Account>05-05560-03</Account><Name>OHRT, STACY</Name><Address>1104 MAIN W</Address><City></City><State></State><Zip></Zip><Latitude></Latitude><Longitude></Longitude><GeoResult></GeoResult></Point>";
            m_strXMLTest += "<Point><MeterNumber></MeterNumber><PremiseKey>05-04090-02    10001</PremiseKey><Account>05-04090-02</Account><Name>KATHMAN-SMITH, KATHY</Name><Address>1105 MAIN W</Address><City></City><State></State><Zip></Zip><Latitude></Latitude><Longitude></Longitude><GeoResult></GeoResult></Point>";
            m_strXMLTest += "<Point><MeterNumber></MeterNumber><PremiseKey>05-05555-02    10001</PremiseKey><Account>05-05555-02</Account><Name>STACKHOUSE, BRIAN K</Name><Address>1106 MAIN W</Address><City></City><State></State><Zip></Zip><Latitude></Latitude><Longitude></Longitude><GeoResult></GeoResult></Point>";
            m_strXMLTest += "<Point><MeterNumber></MeterNumber><PremiseKey>05-04095-02    10001</PremiseKey><Account>05-04095-02</Account><Name>ZIMMERMAN, JOE</Name><Address>1107 MAIN W</Address><City></City><State></State><Zip></Zip><Latitude></Latitude><Longitude></Longitude><GeoResult></GeoResult></Point>";
            m_strXMLTest += "<Point><MeterNumber></MeterNumber><PremiseKey>05-05010-01    10001</PremiseKey><Account>05-05010-01</Account><Name>INGERSOLL, ROSS</Name><Address>1202 MAIN W</Address><City></City><State></State><Zip></Zip><Latitude></Latitude><Longitude></Longitude><GeoResult></GeoResult></Point>";
            m_strXMLTest += "<Point><MeterNumber></MeterNumber><PremiseKey>05-05015-01    10001</PremiseKey><Account>05-05015-01</Account><Name>THUMAN, ALAN &amp; SHARI</Name><Address>1204 MAIN W</Address><City></City><State></State><Zip></Zip><Latitude></Latitude><Longitude></Longitude><GeoResult></GeoResult></Point>";
            m_strXMLTest += "<Point><MeterNumber></MeterNumber><PremiseKey>05-05020-00    10001</PremiseKey><Account>05-05020-00</Account><Name>MORAN, JAMES</Name><Address>1206 MAIN W</Address><City></City><State></State><Zip></Zip><Latitude></Latitude><Longitude></Longitude><GeoResult></GeoResult></Point>";
            m_strXMLTest += "<Point><MeterNumber></MeterNumber><PremiseKey>05-05025-00    10001</PremiseKey><Account>05-05025-00</Account><Name>ESELY, CINDY</Name><Address>1300 MAIN W</Address><City></City><State></State><Zip></Zip><Latitude></Latitude><Longitude></Longitude><GeoResult></GeoResult></Point>";
            m_strXMLTest += "<Point><MeterNumber></MeterNumber><PremiseKey>05-05030-02    10001</PremiseKey><Account>05-05030-02</Account><Name>NELSON, SEAN</Name><Address>1302 MAIN W</Address><City></City><State></State><Zip></Zip><Latitude></Latitude><Longitude></Longitude><GeoResult></GeoResult></Point>";
            m_strXMLTest += "<Point><MeterNumber></MeterNumber><PremiseKey>05-05035-00    10001</PremiseKey><Account>05-05035-00</Account><Name>ERNST, CYNTHIA L.</Name><Address>1304 MAIN W</Address><City></City><State></State><Zip></Zip><Latitude></Latitude><Longitude></Longitude><GeoResult></GeoResult></Point>";
            m_strXMLTest += "<Point><MeterNumber></MeterNumber><PremiseKey>05-05040-00    10001</PremiseKey><Account>05-05040-00</Account><Name>COFFMAN, BONNIE</Name><Address>1306 MAIN W</Address><City></City><State></State><Zip></Zip><Latitude></Latitude><Longitude></Longitude><GeoResult></GeoResult></Point>";
            m_strXMLTest += "<Point><MeterNumber></MeterNumber><PremiseKey>05-05045-00    10001</PremiseKey><Account>05-05045-00</Account><Name>DURHAM, DONNIE</Name><Address>1308 MAIN W</Address><City></City><State></State><Zip></Zip><Latitude></Latitude><Longitude></Longitude><GeoResult></GeoResult></Point>";
            m_strXMLTest += "<Point><MeterNumber></MeterNumber><PremiseKey>01-01055-00    10001</PremiseKey><Account>01-01055-00</Account><Name>JERMAIN, GARY</Name><Address>101 MARKET W</Address><City></City><State></State><Zip></Zip><Latitude></Latitude><Longitude></Longitude><GeoResult></GeoResult></Point>";
            m_strXMLTest += "</GeocodeList></tns:lc_x005F_xml_out>";
            m_strXMLTest += "</tns:row>";
            m_strXMLTest += "</tns:rowset>";



        }

        public void ShowAccounts(SHOW_STATUS a_status)
        {
            m_ocAccounts.Clear();
            foreach (Accounts item in m_MasterList)
            {

                if ((a_status == SHOW_STATUS.ALL_ACCOUNTS) ||
                   ((a_status == SHOW_STATUS.GEOCODED) && item.IsGeocoded) ||
                   ((a_status == SHOW_STATUS.NON_GEOCODED) && !item.IsGeocoded) )
                {
                    m_ocAccounts.Add(item);
                }
            }
        }
    }


    public class DataSetInfo
    {
        int m_nQueryNumber;
        int m_nPageNum;
        int m_nNumResults;
        int m_nNumPerPage;
        int m_nTotalPages;


        public DataSetInfo()
        {
            InitInfo();
        }

        public void InitInfo()
        {
            m_nQueryNumber = 0;
            m_nPageNum = 0;
            m_nNumPerPage = 0;
            m_nNumResults = 0;
            m_nTotalPages = 0;
        }

        public int QueryNumber
        {
            get
            {
                return m_nQueryNumber;
            }

            set
            {
                m_nQueryNumber = value;
            }
        }

        public int PageNumber
        {
            get
            {
                return m_nPageNum;
            }

            set
            {
                m_nPageNum = value;
            }
        }

        public int ResultsPerPage
        {
            get
            {
                return m_nNumPerPage;
            }
            set
            {
                m_nNumPerPage = value;
            }
        }

        public int NumberOfResults
        {
            get
            {
                return m_nNumResults;
            }

            set
            {
                m_nNumResults = value;
            }
        }

        public int TotalPages
        {
            get { return m_nTotalPages; }
            set { m_nTotalPages = value; }
        }



    }
}
