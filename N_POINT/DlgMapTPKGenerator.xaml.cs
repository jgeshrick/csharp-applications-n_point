﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace N_POINT
{
    /// <summary>
    /// Interaction logic for DlgMapTPKGenerator.xaml
    /// </summary>
    public partial class DlgMapTPKGenerator : UserControl
    {
        bool m_boolJobExecuting = false;
        bool m_boolJobAborted = false;
        MapTools.MapTPKGenerator m_tpkGenerator = null;
        int mMinLevel;
        int mMaxLevel;
        private const string strDefaultMapDownloadFolder = "\\Users\\Public\\Neptune Technology Group, Inc.\\MX900\\Mapping\\Layers";

        Object mLastMainWindowControl = null;
        Action mCloseAction;

        public DlgMapTPKGenerator(
            Action aCloseAction,
            int aMinLevel, 
            int aMaxLevel, 
            bool aIncludeSatelliteImagery, 
            Esri.ArcGISRuntime.Geometry.Envelope aMapEnvelope,
            IReadOnlyList<Esri.ArcGISRuntime.ArcGISServices.Lod> aMapLevelsOfDetail)
        {
            InitializeComponent();

            // display in the main window
            mLastMainWindowControl = Application.Current.MainWindow.Content;
            Application.Current.MainWindow.Content = this;

            mCloseAction = aCloseAction;


            StackPanelNumberOfTiles.Visibility = System.Windows.Visibility.Collapsed;

            mMinLevel = aMinLevel;
            mMaxLevel = aMaxLevel;
            m_boolJobAborted = false;
            m_boolJobExecuting = true;

            // estimate the number of tiles and required space.
            if (m_tpkGenerator == null)
            {
                m_tpkGenerator = new MapTools.MapTPKGenerator(aMapLevelsOfDetail);
                m_tpkGenerator.EventTaskStatus += TaskStatus;
            }
            m_tpkGenerator.EstimateCacheSize(aMinLevel, aMaxLevel, aIncludeSatelliteImagery, aMapEnvelope);

            ListViewTasks.ItemsSource = m_tpkGenerator.Tasks;
        }

        /// <summary>
        /// Request from the user the name of the TPK file or set of files.
        /// </summary>
        /// <returns>true if the user selected a name, false otherwise</returns>
        /// <remarks>
        /// Revision History
        /// Date     Who Defect# Description
        /// -------- --- ------- ----------------------------------------    
        /// 12/04/14 BJC         Initial Version
        /// </remarks>
        private bool GetTPKFileNameAndPath(int aNumTiles)
        {
            MapTools.MapTPKGenerator.TPKFileDestination = DefaultMapTPKPath;
            DlgTPKDestinationSettings l_tpk = new DlgTPKDestinationSettings(aNumTiles);
            l_tpk.Owner = Window.GetWindow(Application.Current.MainWindow);
            bool? l_boolResult = l_tpk.ShowDialog();
            if (l_boolResult.HasValue)
            {
                if (l_boolResult == true) // user chose to proceed with download if true
                {
                    // save the destination chosen by the user.
                    MapTools.MapTPKGenerator.TPKFileDestination = l_tpk.TPKFileDestination;
                    MapTools.MapTPKGenerator.IsMX900DownloadLocation = l_tpk.IsMX900Location;
                    return l_boolResult.Value;
                }
            }
            m_boolJobAborted = true;
            m_tpkGenerator.CancelOperation();

            return false;
        }

        /// <summary>
        /// Performs post-TPK file download clean up. 
        /// </summary>
        /// <remarks>
        /// Revision History
        /// Date     Who Defect# Description
        /// -------- --- ------- ----------------------------------------    
        /// 12/04/14 BJC         Initial Version
        /// </remarks>
        private void DownloadComplete(MapTools.TilePackageStatus a_MPS)
        {
            MapTools.TilePackageStatus l_tps = new MapTools.TilePackageStatus();
            if (a_MPS == null)
            {
                l_tps.TaskStatus = MapTools.MapTPKGenerator.TASKS.TASK_DOWNLOAD_COMPLETE;
                l_tps.NumberOfTiles = 0;
                l_tps.ProcessingStatus = "";
            }
            else
            {
                l_tps.UpdateTilePackageStatus(a_MPS);

                if (l_tps.TaskStatus == MapTools.MapTPKGenerator.TASKS.TASK_INVALID_REQUEST)
                {
                    l_tps.TaskStatus = MapTools.MapTPKGenerator.TASKS.TASK_DOWNLOAD_COMPLETE;
                }
            }

            // Clear the "area of interest" box and reset the button text
            //Dispatcher.Invoke(new Action(() => m_EsriMapInterface.ClearAreaOfInterest()));
            m_boolJobExecuting = false;

            // Check if a TPK file was created with restricted levels            
            // if the user didn't cancel the operation and the operation was successful
            if (!m_boolJobAborted && m_tpkGenerator.OperationSuccess)
            {
                if (mMaxLevel > Controls.UserControlMapTileGenerator.MAX_NON_AUTHORIZED_LEVEL)
                {
                    // Consume the key if restricted levels included.
                    App.ConfigurationSettingsMgr.ConsumeCurrentDetailedMapKey();
                }

                MessageBox.Show(
                    Window.GetWindow(Application.Current.MainWindow),
                    "Download complete.");
            }
            else
            {
                // show error message if the operation wasn't aborted by the user
                if (!m_tpkGenerator.OperationSuccess && !m_tpkGenerator.Cancelled)
                {
                    string strMsg;
                    if (l_tps.NumberOfTiles > MapTools.MapTPKGenerator.MAX_TILES_ALLOWED)
                    {
                        strMsg = "Unable to complete operation.\n\n";
                        strMsg += string.Format("The number of tiles requested is {0}.", l_tps.NumberOfTiles);
                        strMsg += string.Format("The maximum number of allowed tiles of {0} has been exceeded.\n\n", MapTools.MapTPKGenerator.MAX_TILES_ALLOWED);
                        strMsg += "Please reduce the level of detail or reduce the size of the area you want to generate a map for.";
                    }
                    else
                    {
                        if (m_tpkGenerator.LastErrorMessage.Length > 0)
                        {
                            strMsg = string.Format("Error requesting tile package.\n{0}", m_tpkGenerator.LastErrorMessage);
                        }
                        else
                        {
                            strMsg = "Unable to complete operation.";
                        }
                    }

                    MessageBox.Show(
                        Window.GetWindow(Application.Current.MainWindow),
                        strMsg, 
                        "Tile caching error");

                    // Clear the status after determining the failure reason
                    l_tps.NumberOfTiles = 0;
                    l_tps.ProcessingStatus = "";
                }
            }

            // close this dialog
            //Close();
            Application.Current.MainWindow.Content = mLastMainWindowControl;
            mCloseAction?.Invoke();
        }

        /// <summary>
        /// Kicks off the tile cache download
        /// </summary>
        /// <remarks>
        /// Revision History
        /// Date     Who Defect# Description
        /// -------- --- ------- ----------------------------------------    
        /// 12/04/14 BJC         Initial Version
        /// </remarks>
        private void DownloadTilePackageFile(int aNumTiles)
        {
            if (m_boolJobExecuting)
            {
                string strDownload = strDefaultMapDownloadFolder;
                //m_EsriMapInterface.TestBuildTPK();
                if (GetTPKFileNameAndPath(aNumTiles))
                {
                    strDownload = MapTools.MapTPKGenerator.TPKFileDestination;
                    try
                    {
                        //m_EsriMapInterface.GenerateTileCacheAndDownload(strDownload);
                        m_tpkGenerator.StartDownload(strDownload);
                    }
                    catch (Exception)
                    {
                        App.AppLogger.LogMsg(Misc.Logger.enumProcessType.TILECACHER, Misc.Logger.enumLogType.DEBUG, string.Format("Aborted"));
                    }
                }
                else
                {
                    DownloadComplete(null);
                }
            }
        }

        string DefaultMapTPKPath
        {
            get
            {
                string l_strPath = "";
                try
                {
                    l_strPath = Misc.Utility.GetDefaultMapTPKPath();
                    if (!Misc.Utility.PathExists(l_strPath))
                    {
                        l_strPath = "";
                    }
                }
                catch (Exception)
                {
                    l_strPath = "";
                }
                return l_strPath;
            }
        }

        public void Cancel()
        {
            if (m_boolJobExecuting)
            {
                m_boolJobAborted = true;
                m_tpkGenerator.CancelOperation();
                MapTools.TilePackageStatus l_tps = new MapTools.TilePackageStatus();
                l_tps.ProcessingStatus = "Cancel Pending...";
                TaskStatus(l_tps);
            }
        }

        private void ButtonCancel_Click(object sender, RoutedEventArgs e)
        {
            Cancel();
        }

        /// <summary>
        /// Performs the actual update of the display status
        /// </summary>
        /// <param name="a_MPS"></param>
        /// <remarks>
        /// Revision History
        /// Date     Who Defect# Description
        /// -------- --- ------- ----------------------------------------    
        /// 12/04/14 BJC         Initial Version
        /// </remarks>
        private void TaskStatus(MapTools.TilePackageStatus a_MPS)
        {
            if (a_MPS != null)
            {
                Dispatcher.BeginInvoke(new Action(() => { SetProgressInternal(a_MPS.ProcessingStatus, 0, 100, a_MPS.PercentComplete, a_MPS.NumberOfTiles); }));
            }

            switch (a_MPS.TaskStatus)
            {
                case MapTools.MapTPKGenerator.TASKS.TASK_CACHE_ESTIMATION:
                    break;
                case MapTools.MapTPKGenerator.TASKS.TASK_PACKAGE_DOWNLOAD:
                    DownloadTilePackageFile(a_MPS.NumberOfTiles);
                    break;
                case MapTools.MapTPKGenerator.TASKS.TASK_INVALID_REQUEST:
                    //DownloadComplete(a_MPS);
                    Dispatcher.Invoke(new Action(() => DownloadComplete(a_MPS)));
                    break;
                case MapTools.MapTPKGenerator.TASKS.TASK_STATUS:
                    break;
                case MapTools.MapTPKGenerator.TASKS.TASK_DOWNLOAD_COMPLETE:
                    //DownloadComplete(a_MPS);
                    Dispatcher.Invoke(new Action(() => DownloadComplete(a_MPS)));
                    break;
            }
        }

        // don't call this, call SetProgress()
        private void SetProgressInternal(String aMessage, int aProgressMin, int aProgressMax, int aProgressCurrent, int aNumTiles)
        {
            if (aNumTiles > 0)
            {
                StackPanelNumberOfTiles.Visibility = System.Windows.Visibility.Visible;
                TextBlockNumberOfTiles.Text = aNumTiles.ToString();
            }

            TextBlockStatus.Text = aMessage;
            if( (aProgressMin == aProgressMax) || (aProgressCurrent <= 0))
            {
                ProgressCurrentTask.IsIndeterminate = true;
            }
            else
            {
                ProgressCurrentTask.IsIndeterminate = false;
                ProgressCurrentTask.Minimum = aProgressMin;
                ProgressCurrentTask.Maximum = aProgressMax;
                ProgressCurrentTask.Value = aProgressCurrent;
            }
        }
    }
}
