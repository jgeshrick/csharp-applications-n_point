﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace N_POINT
{
    /// <summary>
    /// Interaction logic for DlgModifySettings.xaml
    /// </summary>
    public partial class DlgModifySettings : Window
    {
        Controls.UserControlModifyCustomerSettings m_mcs;
        public delegate bool ValidateDataEvent();
        public static event ValidateDataEvent EventValidateData;

        public delegate bool SaveDataEvent();
        public static event SaveDataEvent EventSaveData;

        public DlgModifySettings(string a_strType)
        {
            
            InitializeComponent();
            if (a_strType == "CUSTOMER")
            {
                m_mcs = new Controls.UserControlModifyCustomerSettings();
                dlgContent.Content = m_mcs;
            }
        }

        private void OnSaveSettings(object sender, RoutedEventArgs e)
        {
            bool l_boolResult = true;
            if (EventValidateData != null)
            {
                
                l_boolResult = EventValidateData();

                if (!l_boolResult)
                {
                    // indicate invalid settings 
                    DialogResult = false;
                    MessageBox.Show("Invalid Customer Settings.");
                }
                else
                {
                }
            }

            if (l_boolResult && (EventSaveData != null))
            {
                EventSaveData();
            }
            DialogResult = true;
            this.Close();

        }

        private void OnCancel(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
            this.Close();
        }
    }
}
