﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace N_POINT
{
    /// <summary>
    /// Interaction logic for DlgRequestNeptuneAuthorization.xaml
    /// </summary>
    public partial class DlgRequestNeptuneAuthorization : Window
    {
        bool m_boolValidKey = false;
        bool m_boolOperationCanceled = false;
        public DlgRequestNeptuneAuthorization()
        {
            InitializeComponent();

            msgTextBlock.Text = "Please contact Neptune Support and request a temporary key in order to access to this feature.\r\n\r\nEnter the key in the text box below and press 'Validate' to validate the key.";
        }

        private void OnAuthenticateKey(object sender, RoutedEventArgs e)
        {
            Misc.FeatureAccessKeys validation;
            string strKey = tbPassKey.Text;
            validation = new Misc.FeatureAccessKeys();

            if (validation.ValidateKeyValue(strKey))
            {
                m_boolValidKey = true;
            }

            this.Close();
        }

        private void OnCancel(object sender, RoutedEventArgs e)
        {
            m_boolOperationCanceled = true;
            this.Close();
        }

        public bool ValidKey
        {
            get
            {
                return m_boolValidKey;
            }
        }


        public bool OperationCanceled
        {
            get
            {
                return m_boolOperationCanceled;
            }
        }

        private void OnClosing(object sender, System.ComponentModel.CancelEventArgs e)
        {

        }
    }
}
