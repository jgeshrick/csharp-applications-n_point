﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace N_POINT
{
    /// <summary>
    /// Interaction logic for DlgTPKDestinationSettings.xaml
    /// </summary>
    public partial class DlgTPKDestinationSettings : Window
    {
        string m_strTPKFileDestination;
        bool m_boolIsMX900Location;
        string m_strCurrentRoot;
        public DlgTPKDestinationSettings(int aNumTiles)
        {
            InitializeComponent();
            TextBlockNumberOfTiles.Text = aNumTiles.ToString();
            lbItems.ItemsSource = MapTools.MapTPKGenerator.TPKFiles;
            tbTPKDestination.Text = MapTools.MapTPKGenerator.TPKFileDestination;
            //tbMapRootName.Text = Properties.Settings.Default.MapRootName;
            tbMapRootName.Text = App.ConfigurationSettingsMgr.MapRootName;
            m_strCurrentRoot = tbMapRootName.Text;
            //m_strCurrentRoot = Properties.Settings.Default.MapRootName;

            rbMX900.IsChecked = false;
            rbOther.IsChecked = true;
            m_boolIsMX900Location = false;
            SetDestinationCellVisibility();
        }

        public string TPKFileDestination
        {
            get { return m_strTPKFileDestination; }
            set { m_strTPKFileDestination = value; }
        }

        public bool IsMX900Location
        {
            get { return m_boolIsMX900Location; }
            set { m_boolIsMX900Location = value; }
        }

        private void OnBtnSelectDestination(object sender, RoutedEventArgs e)
        {
            System.Windows.Forms.FolderBrowserDialog l_fbd = new System.Windows.Forms.FolderBrowserDialog();
            l_fbd.SelectedPath = tbTPKDestination.Text;
            l_fbd.ShowDialog();
            tbTPKDestination.Text = l_fbd.SelectedPath;
        }

        private void OnAcceptTPKInfo(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
            m_strTPKFileDestination = tbTPKDestination.Text;

            //Properties.Settings.Default.MapRootName = m_strCurrentRoot;
            App.ConfigurationSettingsMgr.MapRootName = m_strCurrentRoot;

            this.Close();
        }

        private void OnAbortTPKRequest(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
            this.Close();
        }
        /*
        Map_Old_Map
        */

        private void OnRootNameTextChanged(object sender, TextChangedEventArgs e)
        {
            int count;
            if (m_strCurrentRoot != null)
            {
                string strCurrent = m_strCurrentRoot;
                string strTemp;
                string strRoot = "";
                strRoot = tbMapRootName.Text;

                for (count = 0; count < MapTools.MapTPKGenerator.TPKFiles.Count; count++)
                {
                    N_POINT.MapTools.TilePackageRequest tpr = MapTools.MapTPKGenerator.TPKFiles[count];

                    int l_nIndex, l_nLength;

                    // find the current root and remove it
                    l_nIndex = tpr.TPKFileName.IndexOf(strCurrent);
                    l_nLength = strCurrent.Length;

                    strTemp = tpr.TPKFileName.Remove(l_nIndex, l_nLength);
                    strTemp = strRoot + strTemp;
                    tpr.TPKFileName = strTemp;
                    MapTools.MapTPKGenerator.TPKFiles[count] = tpr;
                }
                m_strCurrentRoot = tbMapRootName.Text;
            }
        }


        private void OnChangeRootName(object sender, RoutedEventArgs e)
        {
            if (tbMapRootName.Text.Trim().Length > 0)
            {
                string strRoot = tbMapRootName.Text.Trim();
                string strCurrent = App.ConfigurationSettingsMgr.MapRootName;
                string strTemp;
                int count;
                for (count = 0; count < MapTools.MapTPKGenerator.TPKFiles.Count; count++)
                {
                    N_POINT.MapTools.TilePackageRequest tpr = MapTools.MapTPKGenerator.TPKFiles[count];

                    int l_nIndex, l_nLength;

                    // find the current root and remove it
                    l_nIndex = tpr.TPKFileName.IndexOf(strCurrent);
                    l_nLength = strCurrent.Length;

                    strTemp = tpr.TPKFileName.Remove(l_nIndex, l_nLength);
                    strTemp = strRoot + strTemp;
                    tpr.TPKFileName = strTemp;
                    MapTools.MapTPKGenerator.TPKFiles[count] = tpr;
                }

                // Make the new setting the default for the next request
                App.ConfigurationSettingsMgr.MapRootName = strRoot;
                //Properties.Settings.Default.MapRootName = strRoot;
                //Properties.Settings.Default.Save();
            }
        }

        private void SetDestinationCellVisibility()
        {
            if (rbMX900.IsChecked.Value)
            {
                StackPanelFolderDestination.Visibility = System.Windows.Visibility.Collapsed;
            }
            else
            {
                StackPanelFolderDestination.Visibility = System.Windows.Visibility.Visible;
            }
        }

        private void OnRBMX900Checked(object sender, RoutedEventArgs e)
        {
            m_boolIsMX900Location = true;
            SetDestinationCellVisibility();
        }

        private void OnRBSelectDestinationChecked(object sender, RoutedEventArgs e)
        {
            m_boolIsMX900Location = false;
            SetDestinationCellVisibility();
        }

    }
}
