﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using N_POINT.Data;
using System.Diagnostics;

namespace N_POINT.HostCommunication
{
    public class HostInterface
    {
        // How many times to retry a command to the host service if a previous attempt fails
        static public int SERVICE_COMMAND_RETRY = 3;

        enum HostResponseType
        {
            Needs_Token,
            Invalid_Response,
            Valid_Response,
        }
        public HostInterface()
        {
            //SelfTest();            
        }

        public void SelfTest()
        {
            //object obj = NeptuneWebService.fixed_networkSoapPortClient.CacheSetting;
            TestTokenValid();
            //string str;
            //str = "<tns:rowset xmlns:tns=\"http://10.8.75.254:8080/fixed_network/"> <tns:row>  <tns:lc_x005F_xml_out>  <GeocodeQuery>  <NumResults>122</NumResults>   <QueryNumber>1006</QueryNumber>   </GeocodeQuery></tns:lc_x005F_xml_out> </tns:row></tns:rowset>";
            //DataSetInfo dsi = ParseDataSetRequestResponse(str);
        }

        string BuildQueryDataRequest(string a_strSearch, bool a_boolIncludeGeocoded)
        {
            XmlDocument l_xml = new XmlDocument();
            XmlElement l_element;
            XmlText l_text;
            XmlElement l_group;

            XmlAttribute attr;

            string strIncludeGeocoded = "N";

            if (a_boolIncludeGeocoded)
            {
                strIncludeGeocoded = "Y";
            }

            l_group = l_xml.CreateElement("GeocodeQuery");

            attr = l_xml.CreateAttribute("token");
            attr.Value = App.AuthenticationMgr.AuthenticationToken;
            l_group.Attributes.Append(attr);

            l_element = l_xml.CreateElement("SiteId");
            l_text = l_xml.CreateTextNode(App.ConfigurationSettingsMgr.SiteID);
            l_element.AppendChild(l_text);
            l_group.AppendChild(l_element);


            l_element = l_xml.CreateElement("SearchString");
            l_text = l_xml.CreateTextNode(a_strSearch);
            l_element.AppendChild(l_text);
            l_group.AppendChild(l_element);

            l_element = l_xml.CreateElement("IncludeGeocoded");
            l_text = l_xml.CreateTextNode(strIncludeGeocoded);
            l_element.AppendChild(l_text);

            l_group.AppendChild(l_element);

            l_xml.AppendChild(l_group);

            string strtest;
            strtest = l_xml.InnerXml;

            return l_xml.InnerXml;
        }

        public bool RequestDataSetFromHost(string a_strSearch, bool a_boolIncludeGeocoded, out DataSetInfo a_dsi)
        {
            bool l_boolSuccess = false;
            a_dsi = null;
            if (App.AuthenticationMgr.ServiceAvailable)
            {
                string strRequest;
                string l_strResult = "";
                int l_nCode;

                // build the request xml string
                strRequest = BuildQueryDataRequest(a_strSearch, a_boolIncludeGeocoded);

                HostResponseType l_resp = HostResponseType.Invalid_Response;

                XmlDocument l_xmlDoc = null;

                int nRetry = SERVICE_COMMAND_RETRY;

                do
                {
                    strRequest = BuildQueryDataRequest(a_strSearch, a_boolIncludeGeocoded);

                    // place try catch here instead of surrounding the loop. Testing locally showed the service
                    // throwing a "protocol violation" exception.
                    try
                    {
                        l_strResult = App.NeptuneClient.geocode_query(strRequest, out l_nCode);

                        l_resp = ValidateResponse(l_strResult, out l_xmlDoc);

                        if (l_resp == HostResponseType.Valid_Response)
                        {
                            l_boolSuccess = true;
                        }
                    }
                    catch (Exception ex)
                    {
                        App.AppLogger.LogMsg(Misc.Logger.enumProcessType.GEOCODER, Misc.Logger.enumLogType.ERROR, string.Format("Error requesting data set from host. Message: {0}", ex.Message));

                        // assume an invalid response when an exception occurs
                        l_resp = HostResponseType.Invalid_Response;
                    }

                    nRetry--;

                } while (!l_boolSuccess && (nRetry > 0));

                if ((l_resp != HostResponseType.Invalid_Response) && l_boolSuccess)
                {
                    a_dsi = ParseDataSetRequestResponse(l_strResult, l_xmlDoc);
                }

                if (!l_boolSuccess)
                {
                    a_dsi = new DataSetInfo();                    
                }
            }

            return l_boolSuccess;
        }

        bool GetXMLDocument(string a_strXML, out XmlDocument a_xmlDoc)
        {
            bool bResult = true;

            a_xmlDoc = new XmlDocument();

            try
            {                
                a_xmlDoc.LoadXml(a_strXML);
            }
            catch (Exception)
            {
                bResult = false;
            }

            return bResult;
        }

        void TestTokenValid()
        {
            string str;
            XmlDocument xml;
            str = "<tns:rowset xmlns:tns=\"http://10.8.75.254:8080/fixed_network\"> <tns:row>  <tns:lc_x005F_xml_out><error code=\"1000\">Invalid Token</error></tns:lc_x005F_xml_out> </tns:row></tns:rowset>";
            ValidateResponse(str, out xml);
        }

        /// <summary>
        /// Validates the response XML document.
        /// This function also requests a new token if the response was an invalid token response
        /// </summary>
        /// <param name="a_strResponse"></param>
        /// <param name="a_xmlDoc"></param>
        /// <returns></returns>
        HostResponseType ValidateResponse(string a_strResponse, out XmlDocument a_xmlDoc)
        {
            HostResponseType l_resp = HostResponseType.Valid_Response;
            bool l_boolResult = true;
            XmlNodeList xl;
            a_xmlDoc = new XmlDocument();
            string l_strResult = "";
            string l_strErrorCode;
            try
            {
                a_xmlDoc.LoadXml(a_strResponse);
                xl = a_xmlDoc.GetElementsByTagName("error");
                if ((xl != null) && (xl.Count > 0))
                {
                    l_boolResult = false;
                    l_strResult = xl[0].InnerText;

                    l_strErrorCode = xl[0].Attributes["code"].Value;
                }
            }
            catch (Exception ex)
            {
                // attempt to scan the response for the string "Invalid Token" if xml document failed to load the response string
                l_strResult = a_strResponse;
                l_boolResult = false;
                App.AppLogger.LogMsg(Misc.Logger.enumProcessType.GEOCODER, 
                                     Misc.Logger.enumLogType.ERROR, 
                                     string.Format("ERROR: Unable to validate XML document. Failed to load: {0}", ex.Message));

            }

            if (!l_boolResult)
            {
                // Assume that the response is invalid unless we determine a new token is all that's needed
                l_resp = HostResponseType.Invalid_Response;

                if (l_strResult.IndexOf("Invalid Token") >= 0)
                {
                    App.AppLogger.LogMsg(Misc.Logger.enumProcessType.GEOCODER, Misc.Logger.enumLogType.GENERAL, "Token is invalid or has expired.  Requesting a new token...");
                    l_boolResult = App.AuthenticationMgr.RequestNewToken();

                    if (l_boolResult)
                    {
                        // Indicate to the caller that a new token was needed
                        l_resp = HostResponseType.Needs_Token;
                    }
                }
            }

            return l_resp;
   
        }

        /// <summary>
        /// Parse the response from a request to generate a data set on the host.  A data set is a set of data that satisfies
        /// the search criteria for addresses.
        /// </summary>
        /// <param name="a_strResponse"></param>
        /// <param name="a_xmlDoc"></param>
        /// <returns></returns>
        public DataSetInfo ParseDataSetRequestResponse(string a_strResponse, XmlDocument a_xmlDoc)
        {
            string l_strData;
            DataSetInfo l_dsi = new DataSetInfo();

            XmlNodeList xl;
            int nValue;
            try
            {
                // The document was valid so now we can try and extract the number of results and query number from the document
                xl = a_xmlDoc.GetElementsByTagName("NumResults");

                if ((xl != null) && (xl.Count > 0))
                {

                    l_strData = xl[0].InnerText;
                    if (int.TryParse(l_strData, out nValue))
                    {
                        l_dsi.NumberOfResults = nValue;
                    }
                }

                xl = a_xmlDoc.GetElementsByTagName("QueryNumber");
                if ((xl != null) && (xl.Count > 0))
                {
                    l_strData = xl[0].InnerText;
                    if (int.TryParse(l_strData, out nValue))
                    {
                        l_dsi.QueryNumber = nValue;
                    }
                }
            }
            catch (Exception ex)
            {
                App.AppLogger.LogMsg(Misc.Logger.enumProcessType.GEOCODER, 
                                     Misc.Logger.enumLogType.ERROR, 
                                     string.Format("Error parsing response to data set request. Message: {0}", ex.Message));
            }

            return l_dsi;
        }

        /// <summary>
        /// Builds the XML document that contains a request to retrieve a page of data from the host system.  
        /// The data set should have been previously generated.  The Query Number is number assigned to the 
        /// query that generated the data set
        /// </summary>
        /// <param name="a_nQueryNumber">Query number associated with the request</param>
        /// <param name="a_nPageNumber">Page number from the data set</param>
        /// <param name="a_nNumPerPage">Number of records per page</param>
        /// <returns></returns>
        string BuildGeocodeListRequest(int a_nQueryNumber, int a_nPageNumber, int a_nNumPerPage)
        {
            XmlDocument l_xml = new XmlDocument();
            XmlElement l_element;
            XmlText l_text;
            XmlElement l_group;

            XmlAttribute attr;

            // build the xml request document
            l_group = l_xml.CreateElement("GeocodeList");

            attr = l_xml.CreateAttribute("token");
            attr.Value = App.AuthenticationMgr.AuthenticationToken;
            l_group.Attributes.Append(attr);

            l_element = l_xml.CreateElement("SiteId");
            l_text = l_xml.CreateTextNode(App.ConfigurationSettingsMgr.SiteID);
            l_element.AppendChild(l_text);
            l_group.AppendChild(l_element);


            l_element = l_xml.CreateElement("QueryNumber");
            l_text = l_xml.CreateTextNode(a_nQueryNumber.ToString());
            l_element.AppendChild(l_text);
            l_group.AppendChild(l_element);

            l_element = l_xml.CreateElement("Page");
            l_text = l_xml.CreateTextNode(a_nPageNumber.ToString());
            l_element.AppendChild(l_text);
            l_group.AppendChild(l_element);

            l_element = l_xml.CreateElement("NumPerPage");
            l_text = l_xml.CreateTextNode(a_nNumPerPage.ToString());
            l_element.AppendChild(l_text);
            l_group.AppendChild(l_element);

            l_xml.AppendChild(l_group);

            string strtest;
            strtest = l_xml.InnerXml;

            return l_xml.InnerXml;
        }

        /// <summary>
        /// Request a page of data from the host. 
        /// </summary>
        /// <param name="a_nQueryNumber">The query number associated with the request</param>
        /// <param name="a_nPageNumber">The page number to retrieve</param>
        /// <param name="a_nNumPerPage">Number of records per page</param>
        /// <param name="a_xmlDoc">XML document containing the records received from the host</param>
        /// <returns></returns>
        /// <remarks>
        /// Revision History
        /// Date     Who Defect# Description
        /// -------- --- ------- ----------------------------------------    
        /// 10/07/14 BJC         Initial Version
        /// </remarks>
        public bool RequestGeocodeList(int a_nQueryNumber, int a_nPageNumber, int a_nNumPerPage, out XmlDocument a_xmlDoc)
        {
            bool l_bResult = false;
            int l_nCode;
            string l_strResult;
            a_xmlDoc = null;
            HostResponseType l_resp;
            int l_nRetry = SERVICE_COMMAND_RETRY;

            // Build the XML document for the geocode list request
            string l_strRequest;

            do
            {
                // Allow the request to be built on each loop iteration just in case a new token is requested
                l_strRequest = BuildGeocodeListRequest(a_nQueryNumber, a_nPageNumber, a_nNumPerPage);
                try
                {
                    l_strResult = App.NeptuneClient.geocode_list(l_strRequest, out l_nCode);
                    l_resp = ValidateResponse(l_strResult, out a_xmlDoc);

                    // If the response is needs token, the token was already requested by ValidateResponse.
                    // Stay in this loop and try again with the new token.
                    if (l_resp == HostResponseType.Valid_Response)
                    {
                        l_bResult = true;
                        break;
                    }
                }
                catch (Exception ex)
                {
                    bool lAborting = (l_nRetry == 1);
                    App.AppLogger.LogMsg(
                        Misc.Logger.enumProcessType.GEOCODER,
                        lAborting == true ? Misc.Logger.enumLogType.ERROR : Misc.Logger.enumLogType.WARNING,
                        string.Format(
                            "Error requesting geocode list data. ({0}) Message: {1}", 
                            lAborting == true ? "Retrying" : "Aborting",
                            ex.Message));
                }


                l_nRetry--;

            } while (!l_bResult && (l_nRetry > 0));



            return l_bResult;
        }

        /// <summary>
        /// Retrieves an item that has updated coordinate values from the local list of data.
        /// </summary>
        /// <param name="a_Account"></param>
        /// <param name="a_xml"></param>
        /// <returns></returns>
        /// <remarks>
        /// Revision History
        /// Date     Who Defect# Description
        /// -------- --- ------- ----------------------------------------    
        /// 10/07/14 BJC         Initial Version
        /// </remarks>
        XmlElement GetUpdateGeocodedItem(Accounts a_Account, XmlDocument a_xml)
        {
            XmlElement l_element, l_item;
            XmlText l_text;
            l_element = a_xml.CreateElement("Point");

            l_item = a_xml.CreateElement("PremiseKey");
            l_text = a_xml.CreateTextNode(a_Account.PremiseKey);
            l_item.AppendChild(l_text);
            l_element.AppendChild(l_item);

            l_item = a_xml.CreateElement("Latitude");
            l_text = a_xml.CreateTextNode(a_Account.GeocodeResult.Latitude.ToString());
            l_item.AppendChild(l_text);
            l_element.AppendChild(l_item);

            l_item = a_xml.CreateElement("Longitude");
            l_text = a_xml.CreateTextNode(a_Account.GeocodeResult.Longitude.ToString());
            l_item.AppendChild(l_text);
            l_element.AppendChild(l_item);

            // Indicate how the geocode value was captured
            l_item = a_xml.CreateElement("GeoResult");
            string l_str;
            l_str = string.Format("{0}{1}", a_Account.GeocodeResult.HowGeocodeWasCaptured, a_Account.GeocodeResult.GeocodeScore);
            l_text = a_xml.CreateTextNode(l_str);
            l_item.AppendChild(l_text);
            l_element.AppendChild(l_item);

            return l_element;
        }

        private void GenerateSaveUpdatedRecordsXML(List<Accounts> a_Accounts, out string a_strXML)
        {
            XmlDocument l_xml = new XmlDocument();
            XmlElement l_element;
            XmlText l_text;
            XmlElement l_group;
            XmlElement l_item;

            XmlAttribute attr;

            // build the xml document to request updating records on the host
            l_group = l_xml.CreateElement("Geocode");

            attr = l_xml.CreateAttribute("token");
            attr.Value = App.AuthenticationMgr.AuthenticationToken;
            l_group.Attributes.Append(attr);

            l_element = l_xml.CreateElement("SiteId");
            l_text = l_xml.CreateTextNode(App.ConfigurationSettingsMgr.SiteID);
            l_element.AppendChild(l_text);
            l_group.AppendChild(l_element);

            l_element = l_xml.CreateElement("OverwriteCaptured");
            l_text = l_xml.CreateTextNode("N");
            l_element.AppendChild(l_text);
            l_group.AppendChild(l_element);

            l_element = l_xml.CreateElement("PointList");

            foreach (Accounts l_acct in a_Accounts)
            {
                l_item = GetUpdateGeocodedItem(l_acct, l_xml);

                l_element.AppendChild(l_item);
            }

            l_group.AppendChild(l_element);

            l_xml.AppendChild(l_group);

            a_strXML = l_xml.InnerXml;

        }

        /// <summary>
        /// Sends records that have updated coordinate values to the host.
        /// </summary>
        /// <param name="a_Accounts"></param>
        /// <returns></returns>
        /// <remarks>
        /// Revision History
        /// Date     Who Defect# Description
        /// -------- --- ------- ----------------------------------------    
        /// 10/07/14 BJC         Initial Version
        /// </remarks>
        public bool SaveUpdatedRecordsToHost(List<Accounts> a_Accounts)
        {
            //Debug.WriteLine("Not saving to the host");
            //Debug.Assert(false);
            //return true;

            
            bool bResult = false;
            XmlDocument l_xml;
            HostResponseType l_resp = HostResponseType.Invalid_Response;

            string strRequest;
            string strResponse;

            try
            {
                l_xml = null;

                // try the save up to 3 times
                int MAX_ATTEMPTS = 3;
                for (int i = 0; i < MAX_ATTEMPTS; i++)
                {
                    int nCode = 0;
                    GenerateSaveUpdatedRecordsXML(a_Accounts, out strRequest);

                    try
                    {
                        strResponse = App.NeptuneClient.geocode(strRequest, out nCode);
                    }
                    catch (Exception ex)
                    {
                        Misc.Logger.enumLogType lLogType = (i == (MAX_ATTEMPTS - 1)) ? Misc.Logger.enumLogType.ERROR : Misc.Logger.enumLogType.WARNING;
                        string lMsg = string.Format(
                            "Error calling App.NeptuneClient.geocode ({0}) Exception: {1} SQLCode: {2} XML: {3}", 
                            lLogType == Misc.Logger.enumLogType.WARNING ? "Retrying" : "Aborting",
                            ex.Message,
                            nCode,
                            strRequest);
                        App.AppLogger.LogMsg(Misc.Logger.enumProcessType.GEOCODER, lLogType, lMsg);
                        continue;
                    }

                    l_resp = ValidateResponse(strResponse, out l_xml);

                    // stop looping on a valid response
                    if (l_resp == HostResponseType.Valid_Response) 
                    {
                        bResult = true;
                        break; 
                    }
                    else
                    {                        
                        Misc.Logger.enumLogType lLogType = (i == (MAX_ATTEMPTS - 1)) ? Misc.Logger.enumLogType.ERROR : Misc.Logger.enumLogType.WARNING;
                        string lMsg = string.Format(
                            "Bad response from the host while saving ({0}) Response: {1}",
                            lLogType == Misc.Logger.enumLogType.WARNING ? "Retrying" : "Aborting",
                            l_resp.ToString());
                        App.AppLogger.LogMsg(
                            Misc.Logger.enumProcessType.GEOCODER,
                            lLogType,
                            lMsg);
                    }
                }
            }
            catch (Exception ex)
            {
                App.AppLogger.LogMsg(Misc.Logger.enumProcessType.GEOCODER, Misc.Logger.enumLogType.ERROR, string.Format("Error saving changes to host: {0}", ex.Message));
            }
            
            return bResult;
        }

    }
}

