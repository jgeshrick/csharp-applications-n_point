﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using System.Threading.Tasks;
using Esri.ArcGISRuntime.Geometry;
using Esri.ArcGISRuntime.Layers;
using Esri.ArcGISRuntime.Security;
using Esri.ArcGISRuntime.Tasks.Geoprocessing;
using Esri.ArcGISRuntime.Tasks.Offline;
using System.Threading;
using System.Diagnostics;
using N_POINT.Controls;

namespace N_POINT
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        Controls.UserControlMainTabControl m_UCTabs;
        Controls.UserControlAuthenticationParameters m_Authentication;
        Controls.UserControlAppLogin m_AppLogin;
        public delegate void EnableDisableTabEvent(string a_strName, bool a_boolEnable);
        public static event EnableDisableTabEvent EventEnableDisableTab;
        static private MainWindow sThis = null;

        public delegate void ApplicationAccessEvent(bool a_boolAllow);
        public static event ApplicationAccessEvent EventAllowApplicationAccess;

        public static Dispatcher MyDispatcher { get { return sThis.Dispatcher; } }

        // ClientID - G4jOAUcqbwiYC8zg
        private string GetClientID1()
        {
            return "G4jO";
        }
        private string GetClientID2()
        {
            return "AUcq";
        }
        private string GetClientID3()
        {
            return "bwiY";
        }
        private string GetClientID4()
        {
            return "C8zg";
        }

        // Client Secret - 7d176010d33e416cb8f8f9d7af862fa1
        private string GetClientSecret1()
        {
            return "7d17";
        }
        private string GetClientSecret2()
        {
            return "6010";
        }
        private string GetClientSecret3()
        {
            return "d33e416cb8f";
        }
        private string GetClientSecret4()
        {
            return "8f9d7af862fa1";
        }

        private Credential m_esriCredentials = null;
        private volatile bool m_awaitingCredentils = false;
        public void MyChallengeWithClientID()
        {
            MyChallenge(null, false);
            while (! m_awaitingCredentils)
            {
                Thread.Sleep(50);
            }
        }
        public async Task<Credential> MyChallengeWithUserPassword(CredentialRequestInfo cri)
        {
            return await sThis.Dispatcher.Invoke(() => MyChallenge(cri, true));
        }
        public async Task<Credential> MyChallenge(CredentialRequestInfo cri, bool a_userPasswordLogin)
        {
            if (a_userPasswordLogin)
            {
                // this needs fixed in the future if we allow login by user/password
                Debug.Assert(false);
                //string ESRI_TOKEN_SERVICES = "https://www.arcgis.com/sharing/rest/generatetoken";
                //m_awaitingCredentils = true;
                //m_esriCredentials = await IdentityManager.Current.GenerateCredentialAsync(ESRI_TOKEN_SERVICES, "Neptunegeo", "N3ptun3tg1892");
                return m_esriCredentials;
            }
            else
            {
                // from https://developers.arcgis.com/net/desktop/guide/use-oauth-2-0-authentication.htm
                // also look at https://developers.arcgis.com/net/phone/guide/security.htm
                var serverInfo = new ServerInfo
                {
                    ServerUri = "https://www.arcgis.com/sharing/rest",
                    TokenAuthenticationType = TokenAuthenticationType.OAuthClientCredentials,
                    OAuthClientInfo = new OAuthClientInfo
                    {
                        ClientId = GetClientID1() + GetClientID2() + GetClientID3() + GetClientID4(),
                        ClientSecret = GetClientSecret1() + GetClientSecret2() + GetClientSecret3() + GetClientSecret4(),

                        // Bennie's initial client ID which no longer has credits
                        // User = Neptunegeo
                        // Password = N3ptun3tg1892;
                        //ClientId = "2e03fjINePdhOG1G",
                        //ClientSecret = "476893238e2f411f97359509a77f78c9",

                        RedirectUri = "urn:ietf:wg:oauth:2.0:oob",
                    }
                };
                IdentityManager.Current.RegisterServer(serverInfo);

                // get a credential (app login, user will not be prompted)                    
                var credRequestInfo = new CredentialRequestInfo { ServiceUri = serverInfo.ServerUri };
                m_awaitingCredentils = true;
                m_esriCredentials = await IdentityManager.Current.GetCredentialAsync(credRequestInfo, false);
                Token = ((Esri.ArcGISRuntime.Security.OAuthTokenCredential)m_esriCredentials).Token;
                return m_esriCredentials;
            }
        }

        /// <summary>
        /// static token for holding the token from ESRI 
        /// </summary>
        public static string Token = "";

        public MainWindow()
        {
            InitializeComponent();

            sThis = this;

            N_POINT.MapTools.EsriArcgisRuntimeInterface.EventMapLoadFailed += EsriArcgisRuntimeInterface_EventMapLoadFailed;

            //App.AppLogger.LogMsg(Misc.Logger.enumProcessType.MISC, Misc.Logger.enumLogType.GENERAL, "Application starting.");
            try
            {
                // set the client id and initialize the ArcGIS Runtime
                Esri.ArcGISRuntime.ArcGISRuntimeEnvironment.ClientId = GetClientID1() + GetClientID2() + GetClientID3() + GetClientID4();
                Esri.ArcGISRuntime.ArcGISRuntimeEnvironment.Initialize();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Unable to initialize the ArcGIS Runtime with the provided client id: " + ex.Message);
                App.AppLogger.LogMsg(Misc.Logger.enumProcessType.MISC, Misc.Logger.enumLogType.ERROR, string.Format("unable to set up ESRI security info. Message: {0}", ex.Message));
            }

            bool l_userPasswordLogin = false;
            if (l_userPasswordLogin)
            {
                IdentityManager.Current.ChallengeHandler = new ChallengeHandler(MyChallengeWithUserPassword);
            }
            else
            {
                MyChallengeWithClientID();
            }

            // set the title
            Title = "N_POINT v" + Properties.Resources.Version;

            Misc.Utility.VerifyMX900FolderPaths();

            m_UCTabs = new UserControlMainTabControl();
            m_Authentication = new UserControlAuthenticationParameters();
            m_AppLogin = new UserControlAppLogin();

            EventAllowApplicationAccess += MainWindow_EventAllowApplicationAccess;

            InitializeDisplay();

            if (Properties.Settings.Default.MapRootName.Trim().Length == 0)
            {
                // default map root name
                App.ConfigurationSettingsMgr.MapRootName = "Map";
            }

            var rgn = new RegionInfo(CultureInfo.CurrentCulture.Name);

            //  if we find a matching country code, save the display name 
            if (Properties.Settings.Default.CountryCode.Trim().Length == 0)
            {
                Properties.Settings.Default.CountryCode = rgn.DisplayName;
            }

            Application.Current.DispatcherUnhandledException += UnHandledExceptionHandler;

            //App.AppLogger.LogMsg(Misc.Logger.enumProcessType.MISC, Misc.Logger.enumLogType.GENERAL, "Main window initialization done");
        }

        private void EsriArcgisRuntimeInterface_EventMapLoadFailed(bool a_permissionDenied)
        {
            if (a_permissionDenied)
            {
                ShowMapSystemError();
            }
            else
            {
                sThis.Dispatcher.BeginInvoke(new Action(() => ShowMessageInUIThread("Unknown error while loading the map")));
            }
        }

        static public void ShowMapSystemError()
        {
            sThis.Dispatcher.BeginInvoke(new Action(() => ShowMapSystemErrorInUIThread()));
        }

        static private void ShowMapSystemErrorInUIThread()
        {
            string strDispMsg;
            strDispMsg = "Map System Error. Please Contact Neptune Support for assistance.\r\n";
            strDispMsg += "Email: hhsupp@neptunetg.com\r\n";
            strDispMsg += "Phone: (800) 647-4832";
            ShowMessageInUIThread(strDispMsg);
        }

        static private void ShowMessageInUIThread(string a_message)
        {
            MessageBox.Show(sThis, a_message);
        }

        /// <summary>
        /// Gives access to the main application if a_boolAllow is true or shows the 
        /// configuration screen if a_boolAllow is false.
        /// </summary>
        /// <param name="a_boolAllow"></param>
        private void MainWindow_EventAllowApplicationAccess(bool a_boolAllow)
        {
            if (a_boolAllow)
            {
                mainContent.Content = m_UCTabs;
            }
            else
            {
                mainContent.Content = m_AppLogin;                
            }

        }

        //trap unhandled exceptions - DEBUG
        private void UnHandledExceptionHandler(object sender, DispatcherUnhandledExceptionEventArgs e)
        {
            MessageBox.Show(string.Format("An exception occurred that caused the Mobile Mapping software to shutdown. See the log for more information.\nException: {0}", e.Exception.Message));
            App.AppLogger.LogMsg(Misc.Logger.enumProcessType.MISC, Misc.Logger.enumLogType.ERROR, string.Format("An exception occurred that caused the Mobil Mapping software to shutdown. Exception: {0}, Stack Trace: {1}", e.Exception.Message, e.Exception.StackTrace));

            e.Handled = true;
            //
            //Attempt to gracefully shutdown the application
            //
            App.Current.Shutdown();
        }

        private async void OnMainWindowLoaded(object sender, RoutedEventArgs e)
        {
            var b = await App.ConnectToWebService();

        }

        void InitializeDisplay()
        {
            bool l_boolVerified = false;

            NSP_HostHelpers.NSC_HostHelpers.HostOptions l_options;
            try
            {
                bool l_boolAllow = false;
                l_options = NSP_HostHelpers.NSC_HostHelpers.ParseOptionKey(App.ConfigurationSettingsMgr.CustomerNumber, App.ConfigurationSettingsMgr.OptionKey);

                if (l_options != null)
                {
                    l_boolAllow = l_options.m_mobileMapping;
                }

                if (l_boolAllow)
                {
                    l_boolVerified = true;
                    App.ConfigurationSettingsMgr.MappingLicense = "Yes";
                    App.ConfigurationSettingsMgr.GeocodingLicense = "Yes";
                }
                else
                {
                    App.ConfigurationSettingsMgr.MappingLicense = "No";
                    App.ConfigurationSettingsMgr.GeocodingLicense = "No";
                }

            }
            catch (Exception ex)
            {
                App.AppLogger.LogMsg(Misc.Logger.enumProcessType.MISC, Misc.Logger.enumLogType.ERROR, string.Format("Unable to process application option keys. Exception: {0}", ex.Message));
            }

            AllowAccessToApplication(l_boolVerified);

        }

        private void OnWindowInitialized(object sender, EventArgs e)
        {

        }

        public static void EnableDisableTab(string a_strName, bool a_boolEnable)
        {
            if (EventEnableDisableTab != null)
            {
                EventEnableDisableTab(a_strName, a_boolEnable);
            }
        }

        public static void AllowAccessToApplication(bool a_boolAllow)
        {
            if (EventAllowApplicationAccess != null)
            {
                EventAllowApplicationAccess(a_boolAllow);
            }
        }

        public static void SelectMouseCursor(Cursor a_cursor)
        {
            Mouse.OverrideCursor = a_cursor;
        }

        private void OnMainWindowClosing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            //App.AppLogger.LogMsg(Misc.Logger.enumProcessType.MISC, Misc.Logger.enumLogType.GENERAL, "Closing application");

            // Queue any changes here so that they will prompt to save them before exiting.
            App.DataManager.QueueChanges();
            if (App.DataManager.QueuedAccountsForSave.Count > 0)
            {
                // there are unsaved geocoded accounts.
                string str;
                str = "There are unsaved geocoded accounts.\r\n\r\n";
                str += "Unsaved changes will be lost if they are not saved now.\r\n\r\n";
                str += "Would you like to save changes now before exiting?";

                MessageBoxResult result = MessageBox.Show(str, "Unsaved Geocoded Accounts", MessageBoxButton.YesNo);
                if (result == MessageBoxResult.Yes)
                {
                    //Mouse.OverrideCursor = Cursors.Wait;
                    if (!App.DataManager.SaveChangesBackToHost(false))
                    {
                        System.Windows.MessageBox.Show("Error saving changes to host.", "Save Error");
                    }
                    //Mouse.OverrideCursor = null;
                }
            }
            // make sure configuration settings are saved before exiting the application
            App.ConfigurationSettingsMgr.SaveSettings(); App.ApplicationIsExiting();
        }

        private void OnMainWindowClosed(object sender, EventArgs e)
        {
            //App.AppLogger.LogMsg(Misc.Logger.enumProcessType.MISC, Misc.Logger.enumLogType.GENERAL, "Application closed.");
        }


    }
}
