﻿using System;
using System.Collections.ObjectModel;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Threading;
using System.Threading;
using Esri.ArcGISRuntime.Geometry;
using Esri.ArcGISRuntime.Layers;
using Esri.ArcGISRuntime.Security;
using Esri.ArcGISRuntime.Tasks.Geoprocessing;
using Esri.ArcGISRuntime.Tasks.Offline;
using Esri.ArcGISRuntime.Symbology;
using System.Xml;
using System.Diagnostics;
using System.Net;

namespace N_POINT.MapTools
{
    public class EsriArcgisRuntimeInterface
    {
        Esri.ArcGISRuntime.Controls.MapView m_MapView;
        Dispatcher m_Dispatcher;

        public Envelope CurrentExtent { get { return m_MapView.Extent; } }
        //public ArcGISTiledMapServiceLayer MapLayer { get { return m_OnlineTiledLayer; } }
        //public ArcGISTiledMapServiceLayer SatelliteLayer { get { return m_OnlineSatelliteLayer; } }

        public IReadOnlyList<Esri.ArcGISRuntime.ArcGISServices.Lod> LevelOfDetails { get { return m_OnlineTiledLayer.ServiceInfo.TileInfo.Lods; } }

        public const string DEFAULT_ONLINE_BASEMAP_URL_TILES = "https://tiledbasemaps.arcgis.com/arcgis/rest/services/World_Street_Map/MapServer";
        //public const string DEFAULT_ONLINE_BASEMAP_URL = "https://tiledbasemaps.arcgis.com/arcgis/rest/services/World_Street_Map/MapServer";
		public const string DEFAULT_ONLINE_BASEMAP_URL = "https://services.arcgisonline.com/ArcGIS/rest/services/World_Street_Map/MapServer";

		public const string DEFAULT_ONLINE_SATELLITE_BASEMAP_URL_TILES = "https://tiledbasemaps.arcgis.com/arcgis/rest/services/World_Imagery/MapServer";
		//public const string DEFAULT_ONLINE_SATELLITE_BASEMAP_URL = "https://tiledbasemaps.arcgis.com/arcgis/rest/services/World_Imagery/MapServer";
		public const string DEFAULT_ONLINE_SATELLITE_BASEMAP_URL = "https://services.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer";
		public const string ONLINE_LAYER_ID = "OnlineBasemap";
        public const string ONLINE_SATELLITE_LAYER_ID = "OnlineSatelliteBasemap";
        private const string LOCAL_LAYER_ID = "LocalTiles";
        private const string AOI_LAYER_ID = "AOI";
        private const string TILE_CACHE_FOLDER = "ExportTileCacheSample";
        private const string Image_MapPoint_uri = "pack://application:,,,/N_POINT;component/Images/Location.png";

        // default extent to show when there is no existing extent to show on the map.
        private const string DEFAULT_NORTH_AMERICA_EXTENT = "XMIN=-15914669.5452851, XMAX=-5895916.61065797, YMIN=1940918.80229648, YMAX=7621655.00234281";

        string m_strCurrentTileLayerURL;
        string m_strCurrentSatelliteLayerURL;

        PictureMarkerSymbol m_Graphic_MapPoint = new PictureMarkerSymbol();

        ObservableCollection<Graphic> m_Graphics;

        private ArcGISTiledMapServiceLayer m_OnlineTiledLayer;
        private ArcGISTiledMapServiceLayer m_OnlineSatelliteLayer;
        private GraphicsLayer m_AreaOfInterestLayer;

        GraphicsLayer m_MapPoints;
        
        private bool m_boolMapLoaded = false;
        private bool m_boolMapInitialized = false;
        private bool m_boolMapPermissionDenied = false;

        // uncomment to test split tpk files without using a lot of credits
        //const int MAX_TILES_THRESHOLD = 500;

        // used only in Mouse_Down 
        bool m_boolIsHitTesting;

        Esri.ArcGISRuntime.Symbology.SimpleRenderer m_renderer;
        SimpleRenderer m_PointsRenderer;

        public delegate void MapLoadFailedHandler(bool a_permissionDenied);
        static public event MapLoadFailedHandler EventMapLoadFailed;

        public delegate void NewLocationForPointEvent(object sender, int a_nIndex, double dLatitude, double dLongitude);
        public event NewLocationForPointEvent EventNewLocation;

        public delegate void SelectedLocationEvent(object sender, double dLatitude, double dLongitude);
        public event SelectedLocationEvent EventSelectedLocation;

        public delegate void MapExtentChangedEvent(object sender, MapAreaInfo l_mapArea);
        public event MapExtentChangedEvent EventMapExtentChanged;

        public EsriArcgisRuntimeInterface(Esri.ArcGISRuntime.Controls.MapView a_mapview, Dispatcher a_Dispatcher)
        {
            m_MapView = a_mapview;
            m_Dispatcher = a_Dispatcher;

            // Set the initial map view area
            SetInitialMapExtent();

            m_MapView.Loaded += m_MapView_Loaded;
            m_MapView.MouseDown += MapView_MouseDown;
            m_MapView.MouseDoubleClick += MapView_MouseDoubleClick;
        }

        public void SelfTest()
        {
            //m_nNumTilesNeeded = 282076;
            //CalculateNumberOfRequestsRequired();
            //BuildTilePackageRequestList();
            //GetTileCountFromErrorMsg("ERROR 001564: Requested tile count(278706) exceeds the maximum allowed number of tiles(100000) to be exported for service World_Street_Map:MapServer.");
        }

        /// <summary>
        /// Read the last active map extent saved in settings and restore it.
        /// </summary>
        /// <remarks>
        /// Revision History
        /// Date     Who Defect# Description
        /// -------- --- ------- ----------------------------------------    
        /// 10/27/14 BJC         Initial Version
        /// </remarks>
        private void SetInitialMapExtent()
        {
            string l_str;
            string l_strValue;

            try
            {
                l_str = App.ConfigurationSettingsMgr.LastMapExtent;
                MapAreaInfo mai = new MapAreaInfo();
                double l_dbl;

                if (l_str.Trim().Length == 0)
                {
                    l_str = DEFAULT_NORTH_AMERICA_EXTENT;
                }

                // Parse the map extent setting and determine the last shown map extent
                if (l_str.Trim().Length > 0)
                {
                    int l_nIndex, l_nLength, l_nEnd;
                    l_nIndex = l_str.IndexOf("XMIN=", 0);
                    if (l_nIndex >= 0)
                    {
                        l_nIndex += "XMIN=".Length;
                        l_nEnd = l_str.IndexOf(",", l_nIndex + 1);
                        l_nLength = l_nEnd - l_nIndex;
                        if (l_nLength > 0)
                        {
                            l_strValue = l_str.Substring(l_nIndex, l_nLength);
                            if (double.TryParse(l_strValue, out l_dbl))
                            {
                                mai.XMin = l_dbl;
                            }
                        }
                    }

                    l_nIndex = l_str.IndexOf("XMAX=", l_nIndex);
                    if (l_nIndex > 0)
                    {
                        l_nIndex += "XMAX=".Length;

                        l_nEnd = l_str.IndexOf(",", l_nIndex + 1);
                        l_nLength = l_nEnd - l_nIndex;
                        if (l_nLength > 0)
                        {
                            l_strValue = l_str.Substring(l_nIndex, l_nLength);
                            if (double.TryParse(l_strValue, out l_dbl))
                            {
                                mai.XMax = l_dbl;
                            }
                        }
                    }

                    l_nIndex = l_str.IndexOf("YMIN", l_nIndex);
                    if (l_nIndex > 0)
                    {
                        l_nIndex += "YMIN=".Length;
                        l_nEnd = l_str.IndexOf(",", l_nIndex + 1);
                        l_nLength = l_nEnd - l_nIndex;
                        if (l_nLength > 0)
                        {
                            l_strValue = l_str.Substring(l_nIndex, l_nLength);
                            if (double.TryParse(l_strValue, out l_dbl))
                            {
                                mai.YMin = l_dbl;
                            }
                        }
                    }

                    l_nIndex = l_str.IndexOf("YMAX=", l_nIndex);
                    if (l_nIndex > 0)
                    {
                        l_nIndex += "YMAX=".Length;
                        l_nEnd = l_str.Length;
                        l_nLength = l_nEnd - l_nIndex;
                        if (l_nLength > 0)
                        {
                            l_strValue = l_str.Substring(l_nIndex, l_nLength);
                            if (double.TryParse(l_strValue, out l_dbl))
                            {
                                mai.YMax = l_dbl;
                            }
                        }
                    }
                    double l_dblLong1, l_dblLong2, l_dblLat1, l_dblLat2;

                    MapTools.MercatorProjection.ReverseMercatorConversion2(mai.XMin, mai.YMin, out l_dblLong1, out l_dblLat1);
                    MapTools.MercatorProjection.ReverseMercatorConversion2(mai.XMax, mai.YMax, out l_dblLong2, out l_dblLat2);

                    m_MapView.Map.InitialViewpoint = new Esri.ArcGISRuntime.Controls.Viewpoint(new Envelope(mai.XMin, mai.YMin, mai.XMax, mai.YMax, SpatialReferences.WebMercator));

                    if (EventMapExtentChanged != null)
                    {
                        EventMapExtentChanged(this, mai);
                    }                    
                }


            }
            catch (Exception ex)
            {
                App.AppLogger.LogMsg(Misc.Logger.enumProcessType.MISC, Misc.Logger.enumLogType.GENERAL, string.Format("Error setting initial map extent: Message: {0}", ex.Message));
            }
        }

        /// <summary>
        /// Called anytime the map view extent has changed.  This function captures the current extent and saves it.
        /// The last active extent is restored the next time the application starts.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks>
        /// Revision History
        /// Date     Who Defect# Description
        /// -------- --- ------- ----------------------------------------    
        /// 10/27/14 BJC         Initial Version
        /// </remarks>
        private void MapView_MapExtentChanged(object sender, EventArgs e)
        {
            double l_dblWidth, l_dblHeight;
            l_dblHeight = m_MapView.MaxHeight;
            double l_dblLong1, l_dblLong2, l_dblLat1, l_dblLat2;
            double l_dblMercX, l_dblMercY;

            MapTools.MapAreaInfo mai = new MapAreaInfo();

            // calculate the east west distance of the area
            mai.XMin = m_MapView.Extent.XMin;
            mai.XMax = m_MapView.Extent.XMax;
            mai.YMin = m_MapView.Extent.YMin;
            mai.YMax = m_MapView.Extent.YMax;

            l_dblMercX = m_MapView.Extent.XMin;
            l_dblMercY = m_MapView.Extent.YMin;

            // The extent is in mertacor projection format. Convert to standard lat/long format since the
            // formulas that calculate distance require lat/long format
            MapTools.MercatorProjection.ReverseMercatorConversion2(mai.XMin, mai.YMin, out l_dblLong1, out l_dblLat1);

            l_dblMercX = m_MapView.Extent.XMin;
            l_dblMercY = m_MapView.Extent.YMax;

            MapTools.MercatorProjection.ReverseMercatorConversion2(mai.XMin, mai.YMax, out l_dblLong2, out l_dblLat2);

            l_dblHeight = NSP_GPSDataProvider.GPSUtility.CalculateDistanceBetweenLatLong(l_dblLat1, l_dblLong1, l_dblLat2, l_dblLong2);

            // Calculate the north-south distance of the area
            l_dblMercX = m_MapView.Extent.XMin;
            l_dblMercY = m_MapView.Extent.YMin;

            MapTools.MercatorProjection.ReverseMercatorConversion2(mai.XMin, mai.YMin, out l_dblLong1, out l_dblLat1);

            l_dblMercX = m_MapView.Extent.XMax;
            l_dblMercY = m_MapView.Extent.YMin;

            MapTools.MercatorProjection.ReverseMercatorConversion2(mai.XMax, mai.YMin, out l_dblLong2, out l_dblLat2);

            l_dblWidth = NSP_GPSDataProvider.GPSUtility.CalculateDistanceBetweenLatLong(l_dblLat1, l_dblLong1, l_dblLat2, l_dblLong2);

            l_dblHeight = MetersToMiles(l_dblHeight);
            l_dblWidth = MetersToMiles(l_dblWidth);

            mai.EastWestDistance = l_dblWidth;
            mai.NorthSouthDistance = l_dblHeight;
            mai.AreaSize = l_dblHeight * l_dblWidth;

            string l_strMapExtent;
            l_strMapExtent = string.Format("XMIN={0}, XMAX={1}, YMIN={2}, YMAX={3}",
                mai.XMin, mai.XMax, mai.YMin, mai.YMax);

            App.ConfigurationSettingsMgr.LastMapExtent = l_strMapExtent;


            if (EventMapExtentChanged != null)
            {
                EventMapExtentChanged(this, mai);
            }
        }


        double MetersToMiles(double a_value)
        {
            double dMiles;
            dMiles = (((a_value * 39.37) / 12) / 5280);

            return dMiles;
        }

        public bool MapInitialized
        {
            get { return m_boolMapInitialized; }
        }

        public bool MapPermissionDenied
        {
            get { return m_boolMapPermissionDenied; }
        }

        public string TileLayerBaseMapURL
        {
            get
            {
                return m_strCurrentTileLayerURL;
            }

            set
            {
                m_strCurrentTileLayerURL = value;
            }
        }

        public string SatelliteLayerBaseMapURL
        {
            get
            {
                return m_strCurrentSatelliteLayerURL;
            }

            set
            {
                m_strCurrentSatelliteLayerURL = value;
            }
        }

        


        /// <summary>
        /// Captures the point on the map where the user double clicked. Any component that is interested in
        /// knowing the location on the map the user chose will be notified via the EventSelectedLocation event.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks>
        /// Revision History
        /// Date     Who Defect# Description
        /// -------- --- ------- ----------------------------------------    
        /// 10/27/14 BJC         Initial Version
        /// </remarks>
        private void MapView_MouseDoubleClick(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            System.Windows.Point screenPoint = e.GetPosition(m_MapView);
            MapPoint mp = m_MapView.ScreenToLocation(screenPoint);

            // Let any component subscribed to this event know the user selected a point on the map
            if (EventSelectedLocation != null)
            {
                double dLat, dLong;
                MercatorProjection.ReverseMercatorConversion2(mp.X, mp.Y, out dLong, out dLat);
                EventSelectedLocation(sender, dLat, dLong);
            }
        }

        /// <summary>
        /// Prepare to move a graphic on the map from one location to another
        /// </summary>
        /// <param name="g"></param>
        /// <remarks>
        /// Revision History
        /// Date     Who Defect# Description
        /// -------- --- ------- ----------------------------------------    
        /// 10/27/14 BJC         Initial Version
        /// </remarks>
        public async void PrepareToMove(Graphic g)
        {
            try
            {
                MapPoint pt = await m_MapView.Editor.RequestPointAsync();
                m_MapView.Cursor = System.Windows.Input.Cursors.Arrow;
                if (EventNewLocation != null)
                {
                    int nIndex;
                    double dLatitude, dLongitude;
                    MapTools.MercatorProjection.ReverseMercatorConversion2(pt.X, pt.Y, out dLongitude, out dLatitude);
                    nIndex = (int)g.Attributes["ItemIndex"];
                    g.Geometry = pt;

                    EventNewLocation(this, nIndex, dLatitude, dLongitude);
                }
            }
            catch(Exception ex)
            {
                App.AppLogger.LogMsg(Misc.Logger.enumProcessType.MISC, Misc.Logger.enumLogType.ERROR, string.Format("Prepare to move point: An exception occurred. Message: {0}", ex.Message));
            }
        }

        Graphic m_MoveToLocation = null;

        /// <summary>
        /// Processes mouse down events and determines whether or not the mouse pointer
        /// is placed over a meter point on the screen.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void MapView_MouseDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            // do nothing if the user selected the right button.
            if (e.ChangedButton == System.Windows.Input.MouseButton.Middle)
            {
                return;
            }

            if ((m_boolIsHitTesting) && (e.ChangedButton == System.Windows.Input.MouseButton.Left))
            {
                if (m_MoveToLocation != null)
                {
                    PrepareToMove(m_MoveToLocation);
                    m_MoveToLocation = null;
                }

                m_boolIsHitTesting = false;
                return;
            }

            try
            {
                if (e.ChangedButton == System.Windows.Input.MouseButton.Right)
                {
                    m_boolIsHitTesting = true;

                    System.Windows.Point screenPoint = e.GetPosition(m_MapView);

                    var graphic = await m_MapPoints.HitTestAsync(m_MapView, screenPoint);

                    // if a move is queued up, cancel it.
                    if (m_MoveToLocation != null)
                    {
                        m_MapView.Cursor = System.Windows.Input.Cursors.Arrow;
                        graphic = null;
                        m_MoveToLocation = null;
                    }
                    if (graphic != null)
                    {
                        m_MapView.Cursor = System.Windows.Input.Cursors.SizeAll;
                        m_MoveToLocation = graphic;
                    }
                    else
                    {
                        m_boolIsHitTesting = false;
                    }
                }
            }
            catch
            {

            }
            finally
            {
                //m_boolIsHitTesting = false;
            }
        }

        /// <summary>
        /// Setups the images which mark account locations that will be placed onto the map 
        /// </summary>
        /// <remarks>
        /// Revision History
        /// Date     Who Defect# Description
        /// -------- --- ------- ----------------------------------------    
        /// 10/27/14 BJC         Initial Version
        /// </remarks>
        private void SetupImages()
        {
            m_Graphic_MapPoint = new PictureMarkerSymbol();
            System.Windows.Resources.StreamResourceInfo l_sri;
            l_sri = App.GetResourceStream(new Uri(Image_MapPoint_uri));
            m_Graphic_MapPoint.SetSourceAsync(l_sri.Stream);
        }

        /// <summary>
        /// Sets up the map layers
        /// </summary>
        /// <remarks>
        /// Revision History
        /// Date     Who Defect# Description
        /// -------- --- ------- ----------------------------------------    
        /// 10/27/14 BJC         Initial Version
        /// </remarks>
        private void SetupMapLayers()
        {
            m_MapView.Map.Layers.Clear();
            //
            //Start with the tiled layer first
            //
            m_MapView.Map.Layers.Add(m_OnlineTiledLayer);

            m_AreaOfInterestLayer = new GraphicsLayer();
            m_AreaOfInterestLayer.ID = AOI_LAYER_ID;

            m_MapPoints = new GraphicsLayer();

            m_MapPoints.GraphicsSource = m_Graphics;
            m_MapView.Map.Layers.Add(m_MapPoints);

            m_MapView.Map.Layers.Add(m_AreaOfInterestLayer);

        }

        private void SetupRenderer()
        {
            var lineSymbol = new Esri.ArcGISRuntime.Symbology.SimpleLineSymbol() { Color = System.Windows.Media.Colors.Black, Width = 0.5 };

            Esri.ArcGISRuntime.Symbology.SimpleFillSymbol sfs = new Esri.ArcGISRuntime.Symbology.SimpleFillSymbol();
            sfs.Color = System.Windows.Media.Color.FromArgb(60, 255, 0, 0);
            sfs.Outline = lineSymbol;
            sfs.Style = Esri.ArcGISRuntime.Symbology.SimpleFillStyle.Solid;

            m_renderer = new Esri.ArcGISRuntime.Symbology.SimpleRenderer();
            Esri.ArcGISRuntime.Symbology.SimpleFillSymbol l_symbol = new Esri.ArcGISRuntime.Symbology.SimpleFillSymbol();
            l_symbol.Color = System.Windows.Media.Colors.Red;
            l_symbol.Style = Esri.ArcGISRuntime.Symbology.SimpleFillStyle.Solid;
            m_renderer.Symbol = sfs;

            m_AreaOfInterestLayer.Renderer = m_renderer;


            m_PointsRenderer = new SimpleRenderer();
            m_PointsRenderer.Symbol = m_Graphic_MapPoint;

            m_MapPoints.Renderer = m_PointsRenderer;
        }

        /// <summary>
        /// Reinitializes the map after the user changes a setting that affects the map configuraiton.
        /// </summary>
        public async void ReinitializeMap()
        {
            m_MapView.Map.Layers.Clear();
            
            //m_MapView.Map = new Esri.ArcGISRuntime.Controls.Map();
            //m_MapView.Map.Dispatcher = m_Dispatcher;
            await InitializeOnlineBaseMap();

            //m_Graphics = new ObservableCollection<Graphic>();

            // setup images
            SetupImages();

            // setup map layers
            SetupMapLayers();

            // setup renderers
            SetupRenderer();

            SetInitialMapExtent();
        }

        /// <summary>
        /// Event triggered when the map view has finished initializing.  The 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks>
        /// Revision History
        /// Date     Who Defect# Description
        /// -------- --- ------- ----------------------------------------    
        /// 10/27/14 BJC         Initial Version
        /// </remarks>
        private async void m_MapView_Loaded(object sender, System.Windows.RoutedEventArgs e)
        {
            if (!m_boolMapLoaded)
            {
                //App.AppLogger.LogMsg(Misc.Logger.enumProcessType.MISC, Misc.Logger.enumLogType.GENERAL, "Mapview loaded");
                await InitializeOnlineBaseMap();

                if (m_boolMapInitialized)
                {
                    m_Graphics = new ObservableCollection<Graphic>();

                    // setup images
                    SetupImages();

                    // setup map layers
                    SetupMapLayers();

                    // setup renderers
                    SetupRenderer();
                    m_boolMapLoaded = true;

                    await m_MapView.LayersLoadedAsync();

                    SetInitialMapExtent();
                }
            }
        }

        public void ShutdownMap()
        {
            // this seems to keep the application from crashing when it exits
            if (m_MapView != null)
            {
                m_MapView.Map.Layers.Clear();
            }
        }

        /// <summary>
        /// Initializes the map layers responsible for displaying and downloading content. All of the authentication with 
        /// Esri's servers is handled here.
        /// </summary>
        /// <returns></returns>
        /// <remarks>
        /// Revision History
        /// Date     Who Defect# Description
        /// -------- --- ------- ----------------------------------------    
        /// 10/27/14 BJC         Initial Version
        /// </remarks>
        private async Task InitializeOnlineBaseMap()
        {
            //App.AppLogger.LogMsg(Misc.Logger.enumProcessType.MISC, Misc.Logger.enumLogType.GENERAL, "Initializing map");

            try
            {

                ServicePointManager.Expect100Continue = true;
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

                m_OnlineTiledLayer = new ArcGISTiledMapServiceLayer(new Uri(DEFAULT_ONLINE_BASEMAP_URL));
                m_OnlineTiledLayer.ID = m_OnlineTiledLayer.DisplayName = ONLINE_LAYER_ID;
                m_OnlineSatelliteLayer = new ArcGISTiledMapServiceLayer(new Uri(DEFAULT_ONLINE_SATELLITE_BASEMAP_URL));
                m_OnlineSatelliteLayer.ID = ONLINE_SATELLITE_LAYER_ID;

                // Initialize the tile and satellite layers
                int l_retryCount = 0;
                while(true)
                {
                    try
                    {
                        await m_OnlineTiledLayer.InitializeAsync();
                        break;
                    }
                    catch (Exception ex)
                    {
                        string strCompare = ex.Message.ToLower();
                        if( (l_retryCount < 3) && (strCompare.Contains("error code '499'")) )
                        {
                            App.AppLogger.LogMsg(
                                Misc.Logger.enumProcessType.MISC, 
                                Misc.Logger.enumLogType.GENERAL,
                                string.Format("InitializeAsync token error, retrying: Message: {0}", ex.Message));
                            l_retryCount++;
                        }
                        else
                        {
                            throw (ex);
                        }
                    }                    
                }

                await m_OnlineSatelliteLayer.InitializeAsync();

                // Subscribe to the map extent changed event. This is used to update the display of the
                // current map view extent and to save the current map view extent
                m_MapView.ExtentChanged += MapView_MapExtentChanged;
                m_boolMapInitialized = true;
            }
            catch (Exception ex)
            {
                Debug.Assert(false);
                App.AppLogger.LogMsg(Misc.Logger.enumProcessType.MISC, Misc.Logger.enumLogType.GENERAL, string.Format("Error initializing online base map: Message: {0}", ex.Message));
                m_boolMapInitialized = false;
                string strCompare = ex.Message.ToLower();
                if (strCompare.Contains("error code '500'") || strCompare.Contains("error code '403'"))
                {
                    m_boolMapPermissionDenied = true;
                }

                // only report map permission denied error
                // sometimes other errors occur, but seem to get cleared up with built in ESRI retries
                if( (m_boolMapPermissionDenied) && (EventMapLoadFailed != null) )
                {
                    EventMapLoadFailed(m_boolMapPermissionDenied);
                }
            }
        }

        /// <summary>
        /// Remove any graphics that have been placed on the graphics layer of the map.
        /// </summary>
        /// <remarks>
        /// Revision History
        /// Date     Who Defect# Description
        /// -------- --- ------- ----------------------------------------    
        /// 10/27/14 BJC         Initial Version
        /// </remarks>
        public void ClearGraphicsFromMap()
        {
            if (m_Graphics != null)
            {
                m_Graphics.Clear();
            }
        }

        /// <summary>
        /// Clears the layer that marks an area of interest for tile caching
        /// </summary>
        /// <remarks>
        /// Revision History
        /// Date     Who Defect# Description
        /// -------- --- ------- ----------------------------------------    
        /// 10/27/14 BJC         Initial Version
        /// </remarks>
        public void ClearAreaOfInterest()
        {
            if (m_AreaOfInterestLayer != null)
            {
                m_AreaOfInterestLayer.Graphics.Clear();
            }
        }

        /// <summary>
        /// Converts lat/long coordinates in to mercator projection points.
        /// </summary>
        /// <param name="dLong"></param>
        /// <param name="dLat"></param>
        /// <param name="dMercX"></param>
        /// <param name="dMercY"></param>
        /// <remarks>
        /// Revision History
        /// Date     Who Defect# Description
        /// -------- --- ------- ----------------------------------------    
        /// 10/27/14 BJC         Initial Version
        /// </remarks>
        void MercatorConversion2(double dLong, double dLat, out double dMercX, out double dMercY)
        {
            double RadiansPerDegree = Math.PI / 180;
            double Rad = dLat * RadiansPerDegree;
            double FSin = Math.Sin(Rad);

            dMercY = 6378137 / 2.0 * Math.Log((1.0 + FSin) / (1.0 - FSin));
            dMercX = dLong * 0.017453292519943 * 6378137;
        }

        /// <summary>
        /// Creates a graphic object to be placed on the map at the given coordinates.
        /// </summary>
        /// <param name="a_dblLat"></param>
        /// <param name="a_dblLong"></param>
        /// <returns></returns>
        /// <remarks>
        /// Revision History
        /// Date     Who Defect# Description
        /// -------- --- ------- ----------------------------------------    
        /// 10/27/14 BJC         Initial Version
        /// </remarks>
        public Graphic CreateGraphicObject(double a_dblLat, double a_dblLong)
        {
            double X, Y;
            MercatorConversion2(a_dblLong, a_dblLat, out X, out Y);

            Esri.ArcGISRuntime.Geometry.MapPoint mp = new Esri.ArcGISRuntime.Geometry.MapPoint(X, Y);
            Esri.ArcGISRuntime.Layers.Graphic item = new Esri.ArcGISRuntime.Layers.Graphic();

            item.Geometry = mp;

            return item;
        }

        /// <summary>
        /// Associates an attribute key/value pair with the given graphic
        /// </summary>
        /// <param name="a_graphic"></param>
        /// <param name="a_strKey"></param>
        /// <param name="a_objKeyValue"></param>
        /// <remarks>
        /// Revision History
        /// Date     Who Defect# Description
        /// -------- --- ------- ----------------------------------------    
        /// 10/27/14 BJC         Initial Version
        /// </remarks>
        public void AddAttributeToGraphic(ref Graphic a_graphic, string a_strKey, object a_objKeyValue)
        {
            a_graphic.Attributes.Add(a_strKey, a_objKeyValue);
        }

        /// <summary>
        /// Adds a point to the map with the given attributes contained within the 
        /// MapPointData object.
        /// </summary>
        /// <param name="a_mpd"></param>
        /// <remarks>
        /// Revision History
        /// Date     Who Defect# Description
        /// -------- --- ------- ----------------------------------------    
        /// 10/27/14 BJC         Initial Version
        /// </remarks>
        public void AddPointToMap(MapPointData a_mpd)
        {
            Graphic item;
            item = CreateGraphicObject(a_mpd.Latitude, a_mpd.Longitude);
            foreach (KeyValuePair<string, object> attr in a_mpd.Attributes)
            {
                item.Attributes.Add(attr);
            }
            AddPointToMap(item, true);

        }

        /// <summary>
        /// Adds the specified graphic item to the map and centers the map around the point
        /// if the a_boolCenter parameter is true.
        /// </summary>
        /// <param name="a_item"></param>
        /// <param name="a_boolCenter"></param>
        /// <remarks>
        /// Revision History
        /// Date     Who Defect# Description
        /// -------- --- ------- ----------------------------------------    
        /// 10/27/14 BJC         Initial Version
        /// </remarks>
        public void AddPointToMap(Graphic a_item, bool a_boolCenter)
        {
            if (a_item != null)
            {
                m_Graphics.Add(a_item);
                if (a_boolCenter)
                {
                    CenterMapAtPoint(a_item);
                }
            }
        }

        /// <summary>
        /// Center the map at the point defined by the geometry of the Graphic 'item'
        /// </summary>
        /// <param name="item"></param>
        /// <remarks>
        /// Revision History
        /// Date     Who Defect# Description
        /// -------- --- ------- ----------------------------------------    
        /// 10/27/14 BJC         Initial Version
        /// </remarks>
        public void CenterMapAtPoint(Graphic item)
        {
            m_MapView.SetView(item.Geometry);
        }

        /// <summary>
        /// Center the map at the specified lat/long coordinates
        /// </summary>
        /// <param name="a_dblLat"></param>
        /// <param name="a_dblLong"></param>
        /// <remarks>
        /// Revision History
        /// Date     Who Defect# Description
        /// -------- --- ------- ----------------------------------------    
        /// 10/27/14 BJC         Initial Version
        /// </remarks>
        public void CenterMapAtLocation(double a_dblLat, double a_dblLong)
        {
            double X, Y;
            MercatorConversion2(a_dblLong, a_dblLat, out X, out Y);

            Esri.ArcGISRuntime.Geometry.MapPoint mp = new Esri.ArcGISRuntime.Geometry.MapPoint(X, Y);

            m_MapView.SetView(mp);
            //ZoomMapToScale(150000);
        }

        private void ZoomMapToScale(double scale) //DEBUG function
        {
            m_MapView.ZoomToScaleAsync(scale);
        }

        /// <summary>
        /// Zooms the map to a specified zoom level
        /// </summary>
        /// <param name="a_nLevel"></param>
        /// <remarks>
        /// Revision History
        /// Date     Who Defect# Description
        /// -------- --- ------- ----------------------------------------    
        /// 10/27/14 BJC         Initial Version
        /// </remarks>
        public void ZoomToLevel(int a_nLevel)
        {
            try
            {
                if ((a_nLevel >= 0) && (a_nLevel <= 19))
                {
                    if (m_OnlineTiledLayer.ServiceInfo != null)
                    {
                        if (m_MapView.Scale != m_OnlineTiledLayer.ServiceInfo.TileInfo.Lods[a_nLevel].Scale)
                        {
                            m_MapView.ZoomToScaleAsync(m_OnlineTiledLayer.ServiceInfo.TileInfo.Lods[a_nLevel].Scale);
                        }
                    }
                    else
                    {
                        App.AppLogger.LogMsg(Misc.Logger.enumProcessType.TILECACHER, Misc.Logger.enumLogType.ERROR, "Unable to zoom to level.  The map levels of detail structure is not initialized.");
                    }
                }
            }
            catch (Exception ex)
            {
                App.AppLogger.LogMsg(Misc.Logger.enumProcessType.TILECACHER, Misc.Logger.enumLogType.ERROR, string.Format("Unable to zoom to level. Exception: {0}", ex.Message));
            }
        }

        /// <summary>
        /// Zooms the map by the factor defined by a_dblFactor
        /// </summary>
        /// <param name="a_dblFactor"></param>
        /// <remarks>
        /// Revision History
        /// Date     Who Defect# Description
        /// -------- --- ------- ----------------------------------------    
        /// 10/27/14 BJC         Initial Version
        /// </remarks>
        public void ZoomMap(double a_dblFactor)
        {
            m_MapView.ZoomAsync(a_dblFactor);
        }

        /// <summary>
        /// Show the map layer.
        /// </summary>
        public void ShowTileLayer()
        {
            if (m_OnlineTiledLayer != null)
            {
                // make sure to maintain the tile layer as the first layer in the collection
                m_MapView.Map.Layers.Insert(0, m_OnlineTiledLayer);
                m_MapView.Map.Layers.Remove(m_OnlineSatelliteLayer);
            }
        }

        /// <summary>
        /// Show the satellite layer
        /// </summary>
        public void ShowSatelliteLayer()
        {
            if (m_OnlineSatelliteLayer != null)
            {
                // make sure to maintain the satellite tile layer as the first layer in the collection
                m_MapView.Map.Layers.Insert(0, m_OnlineSatelliteLayer);
                m_MapView.Map.Layers.Remove(m_OnlineTiledLayer);
            }
        }
    }

}
