﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using Esri.ArcGISRuntime.Geometry;
using Esri.ArcGISRuntime.Layers;
using Esri.ArcGISRuntime.Symbology;
using Esri.ArcGISRuntime.Tasks.Geocoding;
using Esri.ArcGISRuntime.Http;

using System.Diagnostics;

namespace N_POINT.MapTools
{
    public class EsriGeocoder
    {
        LocatorTask _locatorTask;
        IList<LocatorGeocodeResult> _candidateResults;

        SpatialReference wgs84 = new SpatialReference(4326);
        SpatialReference webMercator = new SpatialReference(102100);
        const string Default_Geocoder_URL = "https://geocode.arcgis.com/arcgis/rest/services/World/GeocodeServer";
        public const string BATCH_GEOCODE_SERVICE_URL = "https://geocode.arcgis.com/arcgis/rest/services/World/GeocodeServer/geocodeAddresses";

        // https://developers.arcgis.com/net/desktop/guide/search-for-places.htm
        //const string CANDIDATE_FIELD__MATCH_ADDR = "Match_addr";
        const string CANDIDATE_FIELD_ADDR_TYPE = "Addr_type";

        //int m_nMinimumScore = 80;

        LocatorServiceInfo serviceInfo;
        RegionAlias m_regionAlias;

        
        public EsriGeocoder()
        {
            m_regionAlias = new RegionAlias();

            //SetGeocoderURL();

            InitGeocoder();
        }


        private async void InitGeocoder()
        {
            _locatorTask = new OnlineLocatorTask(new Uri(Default_Geocoder_URL), string.Empty);
            try
            {
                serviceInfo = await _locatorTask.GetInfoAsync();
            }
            catch (Exception _Ex)
            {
                Console.WriteLine(_Ex.Message);
            }
            //App.AppLogger.LogMsg(Misc.Logger.enumProcessType.GEOCODER, Misc.Logger.enumLogType.GENERAL, "The geocoding online locator service is initialized.");
        }

        /// <summary>
        /// Validates whether an account item has valid address information set.  
        /// </summary>
        /// <param name="a_AccountItem"></param>
        /// <returns>true if the address information is valid, false otherwise</returns>
        /// <remarks>
        /// Revision History
        /// Date     Who Defect# Description
        /// -------- --- ------- ----------------------------------------    
        /// 10/07/14 BJC         Initial Version
        /// </remarks>
        bool IsValidAddressInfo(Data.Accounts a_AccountItem)
        {
            bool bResult = false;

            // if the address is set, check for additional required parameters
            if (a_AccountItem.Address.Trim().Length > 0)
            {
                if( (!Properties.Settings.Default.ForceDefaultCityState) &&
                    ( ((!string.IsNullOrEmpty(a_AccountItem.City.Trim())) && (!string.IsNullOrEmpty(a_AccountItem.State.Trim()))) || 
                       (!string.IsNullOrEmpty(a_AccountItem.ZipCode.Trim())) ) )
                {
                    bResult = true;
                }
                // Or if the default city/state values are set
                else if ((!string.IsNullOrEmpty(Properties.Settings.Default.DefaultState.Trim())) && 
                         (!string.IsNullOrEmpty(Properties.Settings.Default.DefaultCity.Trim())))
                {
                    bResult = true;
                }
            }

            return bResult;
        }

        public class AddressFullInfo
        {
            public string Address { get; set; }
            public string City { get; set; }
            public string State { get; set; }
            public string ZipCode { get; set; }
            public string CountryCode { get; set; }
            public bool UseOnlyAddress { get; set; }
            public Data.Accounts Account { get; set; }

            public AddressFullInfo(string aAddress)
            {
                UseOnlyAddress = true;
                Address = aAddress.Trim();
                City = null;
                State = null;
                ZipCode = null;
                CountryCode = null;
                Account = null;

                if (!string.IsNullOrWhiteSpace(Properties.Settings.Default.CountryCode))
                {
                    CountryCode = Properties.Settings.Default.CountryCode;
                }
            }

            public AddressFullInfo(Data.Accounts aAccount)
            {
                UseOnlyAddress = false;

                // ESRI will bomb if an address contains a backslash
                // removing backslash below
                Address = aAccount.Address.Trim().Replace("\\", "");
                City = null;
                State = null;
                ZipCode = null;
                CountryCode = null;
                Account = aAccount;

                // set to defaults first
                if (!string.IsNullOrWhiteSpace(Properties.Settings.Default.DefaultCity))
                {
                    City = Properties.Settings.Default.DefaultCity;
                }
                if (!string.IsNullOrWhiteSpace(Properties.Settings.Default.DefaultState))
                {
                    State = Properties.Settings.Default.DefaultState;
                }
                if (!string.IsNullOrWhiteSpace(Properties.Settings.Default.CountryCode))
                {
                    CountryCode = Properties.Settings.Default.CountryCode;
                }

                // now apply account specific overrides
                if (!Properties.Settings.Default.ForceDefaultCityState)
                {
                    if (!string.IsNullOrWhiteSpace(aAccount.City))
                    {
                        City = aAccount.City.Trim();
                    }

                    if (!string.IsNullOrWhiteSpace(aAccount.State))
                    {
                        State = aAccount.State.Trim();
                    }

                    if (!string.IsNullOrWhiteSpace(aAccount.ZipCode))
                    {
                        ZipCode = aAccount.ZipCode.Trim();
                    }
                }
            }

            public override string ToString()
            {
                string lRet = "";
                if (!String.IsNullOrEmpty(City))
                {
                    lRet += City;
                }
                if (!String.IsNullOrEmpty(State))
                {
                    if (!String.IsNullOrEmpty(lRet))
                    {
                        lRet += ",";
                    }
                    lRet += State;
                }
                if (!String.IsNullOrEmpty(ZipCode))
                {
                    if (!String.IsNullOrEmpty(lRet))
                    {
                        lRet += ",";
                    }
                    lRet += ZipCode;
                }

                return lRet;
            }
        }

        /// <summary>
        /// Formats the address search string using the account address and the formatted region info
        /// </summary>
        /// <param name="a_AccountItem"></param>
        /// <param name="a_strRegionInfo"></param>
        /// <param name="a_strAddress"></param>
        /// <remarks>
        /// Revision History
        /// Date     Who Defect# Description
        /// -------- --- ------- ----------------------------------------    
        /// 10/07/14 BJC         Initial Version
        /// </remarks>
        //void FormatAddressSearchString(Data.Accounts a_AccountItem, RegionInfo aRegionInfo, out string a_strAddress)
        //{
        //    a_strAddress = a_AccountItem.Address;
        //    if (aRegionInfo.ToString().Trim().Length > 0)
        //    {
        //        a_strAddress += "," + aRegionInfo.ToString();
        //    }
        //}

        /// <summary>
        /// Generates a geocode result for an account item
        /// </summary>
        /// <param name="a_AccountItem"></param>
        /// <param name="a_intMinimumScore"></param>
        /// <returns></returns>
        /// <remarks>
        /// Revision History
        /// Date     Who Defect# Description
        /// -------- --- ------- ----------------------------------------    
        /// 10/07/14 BJC         Initial Version
        /// </remarks>
        public async Task<GeocodedResult> GeocodeAddressAsync(
            Data.Accounts a_AccountItem, 
            int a_intMinimumScore, 
            bool a_boolSingleGeocode = false)
        {
            if ((a_AccountItem.Address.Trim().Length == 0) &&
                (a_AccountItem.City.Trim().Length == 0) && 
                (a_AccountItem.State.Trim().Length == 0) &&
                (a_AccountItem.ZipCode.Trim().Length == 0))
            {
                // nothing to do - no 'location' provided to geocode
                return new GeocodedResult();
            }

            // only allow geocode if the address contains valid components
            if (IsValidAddressInfo(a_AccountItem))
            {
                AddressFullInfo lAddressFullInfo = new AddressFullInfo(a_AccountItem);
 
                //FormatAddressSearchString(a_AccountItem, "", out l_strAddress);
                //App.AppLogger.LogMsg(Misc.Logger.enumProcessType.GEOCODER, Misc.Logger.enumLogType.DEBUG, string.Format("**********************************************************************************************************"));
                //App.AppLogger.LogMsg(Misc.Logger.enumProcessType.GEOCODER, Misc.Logger.enumLogType.DEBUG, string.Format("GEOCODING ADDRESS: Address: {0} -- Region: {1}", l_strAddress, l_strRegionInfo));
                var result = await GeocodeAddress(lAddressFullInfo, webMercator, a_intMinimumScore);
                //App.AppLogger.LogMsg(Misc.Logger.enumProcessType.GEOCODER, Misc.Logger.enumLogType.DEBUG, string.Format("**********************************************************************************************************"));
                GeocodedResult gr = result;
                if (gr.ResultValid)
                {
                    gr.HowGeocodeWasCaptured = "G";
                    a_AccountItem.GeocodeResult.CopyItems(gr);
                    a_AccountItem.IsGeocoded = true;
                    gr.ResultValid = true;
                    a_AccountItem.CaptureAccuracy = gr.GeocodeScore;
                    //App.AppLogger.LogMsg(Misc.Logger.enumProcessType.GEOCODER, Misc.Logger.enumLogType.DEBUG, string.Format("RESULT: Lat: {0}, Long: {1}", gr.Latitude, gr.Longitude));

                    // single geocode does not require 
                    if (!a_boolSingleGeocode)
                    {
                        App.GEOQueueMgr.QueueOperation(GeoQueueManager.QUEUE_OP_CODE.INSERTITEM, ref a_AccountItem);
                    }
                }
                else
                {
                    a_AccountItem.GeocodeResult.AddressInputParametersDictionary = gr.AddressInputParametersDictionary;
                    a_AccountItem.GeocodeResult.BestScore = gr.BestScore;
                }

                // Indicate this item has been sent to the geocoder
                a_AccountItem.SubmittedToGeocoder = true;

                return gr;
            }

            return new GeocodedResult();
        }

        /// <summary>
        /// Geocodes an address and returns a positive result if the score of the result meets the criteria for the minimum value.
        /// </summary>
        /// <param name="a_strAddress"></param>
        /// <param name="a_strRegionInfo"></param>
        /// <param name="a_spatialReference"></param>
        /// <param name="a_intMinScore"></param>
        /// <returns></returns>
        /// <remarks>
        /// Revision History
        /// Date     Who Defect# Description
        /// -------- --- ------- ----------------------------------------    
        /// 10/07/14 BJC         Initial Version
        /// </remarks>
        public async Task<GeocodedResult> GeocodeAddress(AddressFullInfo aAddressFullInfo, SpatialReference a_spatialReference, int a_intMinScore)
        {
            SynchronizationContext.SetSynchronizationContext(new SynchronizationContext());
            GeocodedResult gr = new GeocodedResult();
            gr.ResultValid = false;


            if (serviceInfo == null)
            {
                throw new Exception("The geocoding locator service is unavailable.");
            }

            // https://developers.arcgis.com/rest/geocode/api-reference/geocoding-find-address-candidates.htm
            System.Collections.Generic.Dictionary<string, string> lAddressInputParametersDictionary = new System.Collections.Generic.Dictionary<string, string>();
            if (! aAddressFullInfo.UseOnlyAddress)
            {
                lAddressInputParametersDictionary.Add("Address", aAddressFullInfo.Address);
                if (!String.IsNullOrEmpty(aAddressFullInfo.City))
                {
                    lAddressInputParametersDictionary.Add("City", aAddressFullInfo.City);
                }
                if (!String.IsNullOrEmpty(aAddressFullInfo.State))
                {
                    lAddressInputParametersDictionary.Add("Region", aAddressFullInfo.State);
                }
                if (!String.IsNullOrEmpty(aAddressFullInfo.ZipCode))
                {
                    lAddressInputParametersDictionary.Add("Postal", aAddressFullInfo.ZipCode);
                }
                if (!String.IsNullOrWhiteSpace(aAddressFullInfo.CountryCode))
                {
                    lAddressInputParametersDictionary.Add("CountryCode", aAddressFullInfo.CountryCode);
                }
            }
            else
            {
                string lSingleLine = aAddressFullInfo.Address;
                if (!String.IsNullOrWhiteSpace(aAddressFullInfo.CountryCode))
                {
                    lSingleLine += "," + aAddressFullInfo.CountryCode;
                }
                lAddressInputParametersDictionary.Add("SingleLine", lSingleLine);
            }

            _candidateResults = await _locatorTask.GeocodeAsync(lAddressInputParametersDictionary, new List<string> { CANDIDATE_FIELD_ADDR_TYPE }, a_spatialReference, CancellationToken.None);

            gr.AddressInputParametersDictionary = lAddressInputParametersDictionary;
            gr.BestScore = 0;
            int nCandidates = 0;
            foreach (LocatorGeocodeResult candidate in _candidateResults)
            {
                // get the address type
                if (!candidate.Attributes.ContainsKey(CANDIDATE_FIELD_ADDR_TYPE))
                {
                    Debug.Assert(false);
                    continue;
                }
                string lAddrType = candidate.Attributes[CANDIDATE_FIELD_ADDR_TYPE];

                // if only address entered then user entered their own search string
                // just let any good match display
                if (! aAddressFullInfo.UseOnlyAddress)
                {
                    // if trying to geocode a point force match at address or building level
                    if (! IsESRIAddressTypeOKForGeocode(lAddrType))
                    {
                        continue;
                    }
                }

                if (candidate.Score > gr.BestScore)
                {
                    gr.BestScore = candidate.Score;
                }

                nCandidates++;
                bool bBreakAfterThisResult = true;
                //if (candidate.Score >= a_intMinScore)
                //MessageBox("candidate.Score = " + candidate.Score.ToString() + " Min Score = " + a_intMinScore.ToString());
                if (candidate.Score >= a_intMinScore)
                {

                    string latlon = String.Format("{0}, {1}", candidate.Location.X, candidate.Location.Y);

                    double x = MapTools.MercatorProjection.xToLon(candidate.Location.X);
                    double y = MapTools.MercatorProjection.yToLat(candidate.Location.Y);

                    // if the spatial reference isn't wsg84, convert the mercator coordinates into lat/long coordinates
                    if (a_spatialReference.Wkid != wgs84.Wkid)
                    {
                        MapTools.MercatorProjection.ReverseMercatorConversion2(candidate.Location.X, candidate.Location.Y, out x, out y);
                    }
                    else
                    {
                        y = candidate.Location.Y;
                        x = candidate.Location.X;
                    }

                    gr.Latitude = y;
                    gr.Longitude = x;

                    gr.GeocodeScore = candidate.Score;
                    gr.ResultValid = true;
                    if (bBreakAfterThisResult)
                    {
                        break;
                    }
                }
            }

            return gr;
        }

        /// <summary>
        /// Determines the match scoring of a candidate versus the original address/region.  The Esri
        /// geocoder returns the address and region information in the candidate results
        /// </summary>
        /// <param name="a_strCandidate"></param>
        /// <param name="a_strSearch"></param>
        /// <returns></returns>
        double MatchScore(string a_strCandidate, string a_strSearch)
        {
            string l_strCandidate = a_strCandidate.ToUpper();
            string l_strSearch = a_strSearch.ToUpper();

            string[] l_strcollCandidates;
            string[] l_strcollSearch;

            int l_nEmptyString = 0;
            int l_nMatches = 0;

            l_strcollCandidates = l_strCandidate.Split(new char[] { ',', ' ' });
            l_strcollSearch = l_strSearch.Split(new char[] { ',', ' ' });

            foreach (string str in l_strcollSearch)
            {
                if (str.Trim().Length > 0)
                {
                    if (l_strcollCandidates.Contains(str))
                    {
                        l_nMatches++;
                    }
                    else if (m_regionAlias.DoesAliasExist(str))
                    {
                        l_nMatches++;
                    }
                }
                else
                {
                    l_nEmptyString++;
                }
            }

            // Count the number of address items that match between the candidate and the supplied address.
            double l_dblCheck;

            int nFactor = l_strcollSearch.Length - l_nEmptyString;

            if (nFactor <= 0)
            {
                nFactor = 1;
            }

            // return the ratio of matches to the number of words (percentage of words
            // that matched)
            l_dblCheck = (double)((double)l_nMatches / (double)nFactor);

            return l_dblCheck;
        }

        private bool IsESRIAddressTypeOKForGeocode(string aESRIAddressType)
        {
            switch (aESRIAddressType)
            {
                case "PointAddress":
                case "BuildingName":
                case "StreetAddress":
                    return true;
                default:
                    return false;
            }
        }

        public async Task BatchGeocode(List<AddressFullInfo> aAccounts)
        {
            JavaScriptSerializer serializer = new JavaScriptSerializer();

            // call single geocode web service if only account passed in
            // this can be used as a workaround in case the batch geocode
            // web service is failing
            if (aAccounts.Count == 1)
            {
                foreach (var account in aAccounts)
                {
                    await GeocodeAddressAsync(
                        account.Account,
                        App.ConfigurationSettingsMgr.MinimumGeocodingHit, 
                        false);
                }
            }
            else
            {
                string records = string.Join(",", aAccounts.Where(s => !string.IsNullOrWhiteSpace(s.Address))
                    .Select((s, idx) => string.Format(
                        "{{ \"attributes\": {{ " +
                        "\"OBJECTID\": {0}, " +
                        "\"Address\": \"{1}\", " +
                        "\"City\": \"{2}\", " +
                        "\"Region\": \"{3}\", " +
                        "\"Postal\": \"{4}\", " +
                        "\"CountryCode\": \"{5}\" " +
                        "}} }}",
                        idx,
                        // ESRI web service will throw an error if double quotes are contained in the fields, escaping with &quot;
                        // Other special XML characters do not seem to matter, so only escaping double quote below
                        s.Address.Replace("\"", "&quot;"),
                        s.City,
                        s.State,
                        s.ZipCode,
                        s.CountryCode))
                    .ToArray());

                string addresses = string.Format("{{ \"records\": [ {0} ] }}", records);

                Dictionary<string, string> parameters = new Dictionary<string, string>();
                parameters["f"] = "json";
                parameters["outSR"] = webMercator.Wkid.ToString();
                parameters["addresses"] = addresses;
                parameters["token"] = MainWindow.Token;

                ArcGISHttpClient httpClient = new ArcGISHttpClient();

                var response = await httpClient.GetOrPostAsync(EsriGeocoder.BATCH_GEOCODE_SERVICE_URL, parameters);

                var jsonResults = await response.EnsureSuccessStatusCode().Content.ReadAsStringAsync();
                var results = serializer.Deserialize<Dictionary<string, object>>(jsonResults);

                var candidates = results["locations"] as ArrayList;
                Dictionary<string, object> attr;
                Dictionary<string, object> location;
                int nIndex;
                int nScore;

                foreach (var candidate in candidates.OfType<Dictionary<string, object>>())
                {
                    // skip this candidate if it does not contain location or attributes
                    if ((!candidate.ContainsKey("location")) ||
                        (!candidate.ContainsKey("attributes")))
                    {
                        continue;
                    }

                    location = candidate["location"] as Dictionary<string, object>;
                    attr = candidate["attributes"] as Dictionary<string, object>;

                    try
                    {
                        nIndex = (int)attr["ResultID"];
                        if ((nIndex >= 0) && (nIndex < aAccounts.Count))
                        {
                            Data.Accounts l_acct = aAccounts[nIndex].Account;

                            string lAddrType = Convert.ToString(attr[CANDIDATE_FIELD_ADDR_TYPE]);
                            nScore = Convert.ToInt32(attr["Score"]);

                            // make sure score is good enough
                            // make sure address type is ok for geocoding
                            if ((nScore >= App.ConfigurationSettingsMgr.MinimumGeocodingHit) &&
                                (IsESRIAddressTypeOKForGeocode(lAddrType)))
                            {
                                // get the account and submit it to the processing queue
                                double y = Convert.ToDouble(attr["Y"]);
                                double x = Convert.ToDouble(attr["X"]);

                                l_acct.GeocodeResult.Latitude = y;
                                l_acct.GeocodeResult.Longitude = x;
                                l_acct.GeocodeResult.HowGeocodeWasCaptured = "G";
                                l_acct.GeocodeResult.GeocodeScore = nScore;
                                l_acct.GeocodeResult.ResultValid = true;

                                l_acct.IsGeocoded = true;
                                l_acct.HowGeocodeWasCaptured = "G";
                                l_acct.CaptureAccuracy = nScore;
                                l_acct.SubmittedToGeocoder = true;
                            }
                            else
                            {
                                l_acct.GeocodeResult.ResultValid = false;
                            }

                            App.GEOQueueMgr.QueueOperation(GeoQueueManager.QUEUE_OP_CODE.INSERTITEM, ref l_acct);
                        }
                    }
                    catch (Exception ex)
                    {
                        string str = ex.Message;

                    }

                }
            }

        }
        
    }

}
