﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace N_POINT.MapTools
{

    class GeocodeResultQueue
    {
        const int MAX_GEOCODE_QUEUE_ITEMS = 256;
        ArrayList m_alItems;
        int m_nHead, m_nTail, m_nSize;

        public GeocodeResultQueue()
        {
            m_alItems = new ArrayList(MAX_GEOCODE_QUEUE_ITEMS+1);
            for (int count = 0; count < m_alItems.Capacity; count++)
            {
                Data.Accounts a = new Data.Accounts();
                m_alItems.Add(a);
            }

        }

        public bool Insert(Data.Accounts acct)
        {
            bool l_boolResult = false;
            if (!IsFull())
            {
                m_nTail += 1;
                if (m_nTail > MAX_GEOCODE_QUEUE_ITEMS)
                {
                    m_nTail = (m_nTail % MAX_GEOCODE_QUEUE_ITEMS);
                }

                m_alItems[m_nTail] = acct;
                m_nSize++;
                l_boolResult = true;
            }

            return l_boolResult;
        }

        public Data.Accounts QueueServe()
        {
            Data.Accounts acct;
            acct = (Data.Accounts)m_alItems[m_nHead];
            m_nHead += 1;
            if (m_nHead > MAX_GEOCODE_QUEUE_ITEMS)
            {
                m_nHead = (m_nHead % MAX_GEOCODE_QUEUE_ITEMS);
            }

            m_nSize--;

            return acct;
        }

        public int QueueLength()
        {
            return m_nSize;
        }

        public bool IsFull()
        {
            bool l_boolRetVal = false;
            if (m_nSize == MAX_GEOCODE_QUEUE_ITEMS)
            {
                l_boolRetVal = true;
            }

            return l_boolRetVal;
        }

        public bool IsEmpty()
        {
            bool l_boolRetVal = false;
            if (m_nSize == 0)
            {
                l_boolRetVal = true;
            }

            return l_boolRetVal;
        }

        public void CreateQueue()
        {
            m_nHead = 1;
            m_nTail = MAX_GEOCODE_QUEUE_ITEMS;
            m_nSize = 0;
        }
    }

    public class GeoQueueManager
    {
        object m_QueueLock = new object();
        public enum QUEUE_OP_CODE
        {
            ISFULL,
            ISEMPTY,
            INSERTITEM,
            REMOVEITEM
        }

        bool m_boolNotificationIssued = false;

        GeocodeResultQueue m_GRQueue;
        public delegate void QueueDataAvailableEvent(object sender);
        public event QueueDataAvailableEvent EventQueueDataAvailable;

        public GeoQueueManager()
        {
            m_GRQueue = new GeocodeResultQueue();
            m_GRQueue.CreateQueue();
        }

        public bool QueueOperation(QUEUE_OP_CODE oc)
        {
            bool l_boolResult = false;
            lock (m_QueueLock)
            {
                switch (oc)
                {
                    case QUEUE_OP_CODE.ISFULL:
                        l_boolResult = m_GRQueue.IsFull();
                        break;
                    case QUEUE_OP_CODE.ISEMPTY:
                        l_boolResult = m_GRQueue.IsEmpty();
                        m_boolNotificationIssued = false;
                        break;
                }
            }

            return l_boolResult;
        }

        public bool QueueOperation(QUEUE_OP_CODE oc, ref Data.Accounts acct)
        {
            bool l_boolResult = false;
            lock (m_QueueLock)
            {
                switch (oc)
                {
                    case QUEUE_OP_CODE.INSERTITEM:
                        l_boolResult = m_GRQueue.Insert(acct);
                        if (!m_boolNotificationIssued)
                        {
                            if (EventQueueDataAvailable != null)
                            {
                                m_boolNotificationIssued = true;
                                EventQueueDataAvailable(this);
                            }
                        }
                        break;
                    case QUEUE_OP_CODE.REMOVEITEM:
                        if (!m_GRQueue.IsEmpty())
                        {
                            acct = m_GRQueue.QueueServe();
                            l_boolResult = true;
                        }
                        break;
                    default:
                        l_boolResult = false;
                        break;
                }
            }

            return l_boolResult;
        }

    }
}
