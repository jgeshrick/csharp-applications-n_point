﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace N_POINT.MapTools
{
    public class GeocodedResult
    {
        double m_dblLatitude;
        double m_dblLongitude;
        int m_intScore;
        string m_strOrigAddress;
        string m_strAddress;
        int m_nIndex;
        bool m_boolResultValid;
        string m_strHowCaptured;
        bool m_boolSubmittedToGeocoder;
        System.Exception m_exception;

        public int BestScore
        {
            get; set;
        } = 0;

        public GeocodedResult()
        {
            m_dblLatitude = 0;
            m_dblLongitude = 0;
            m_intScore = 0;
            m_strAddress = "";
            m_strOrigAddress = "";
            m_nIndex = 0;
            m_boolResultValid = false;
            m_boolSubmittedToGeocoder = false;
            m_strHowCaptured = "";
        }

        public void CopyItems(GeocodedResult a_gr)
        {
            Index = a_gr.Index;
            ResultValid = a_gr.ResultValid;
            Address = a_gr.Address;
            OriginalAddress = a_gr.OriginalAddress;
            GeocodeScore = a_gr.GeocodeScore;
            Latitude = a_gr.Latitude;
            Longitude = a_gr.Longitude;
            HowGeocodeWasCaptured = a_gr.HowGeocodeWasCaptured;
            BestScore = a_gr.BestScore;
            if(a_gr.AddressInputParametersDictionary != null)
            {
                foreach (string lKey in a_gr.AddressInputParametersDictionary.Keys)
                {
                    AddressInputParametersDictionary[lKey] = a_gr.AddressInputParametersDictionary[lKey];
                }
            }            
        }


        public int Index
        {
            get { return m_nIndex; }
            set { m_nIndex = value; }
        }

        /// <summary>
        /// Indicates whether or not a result is valid
        /// </summary>
        public bool ResultValid
        {
            get { return m_boolResultValid; }
            set { m_boolResultValid = value; }
        }

        /// <summary>
        /// Address of the account
        /// </summary>
        public string Address
        {
            get { return m_strAddress; }
            set { m_strAddress = value;  }
        }

        public System.Collections.Generic.Dictionary<string, string> AddressInputParametersDictionary
        {
            get; set;
        } = new Dictionary<string, string>();

        /// <summary>
        /// Original address passed in from the host system
        /// </summary>
        public string OriginalAddress
        {
            get { return m_strOrigAddress; }
            set { m_strOrigAddress = value; }
        }

        /// <summary>
        /// Geocode score reported by the Esri geocoding service
        /// </summary>
        public int GeocodeScore
        {
            get { return m_intScore; }
            set { m_intScore = value; }
        }

        /// <summary>
        /// Latitude value
        /// </summary>
        public double Latitude
        {
            get { return m_dblLatitude; }
            set { m_dblLatitude = value; }
        }

        /// <summary>
        /// Longitude value
        /// </summary>
        public double Longitude
        {
            get { return m_dblLongitude; }
            set { m_dblLongitude = value; }
        }

        /// <summary>
        /// How geocode values were captured
        /// 'C' = Captured
        /// 'S' = Selected / pointed at
        /// 'G' = Geocoded
        /// 'F' = From CIS file
        /// </summary>
        public string HowGeocodeWasCaptured
        {
            get { return m_strHowCaptured; }
            set { m_strHowCaptured = value; }
        }

        public bool SubmittedToGeocoder
        {
            get { return m_boolSubmittedToGeocoder; }
            set { m_boolSubmittedToGeocoder = value; }
        }

        public System.Exception GeocodeException
        {
            get { return m_exception; }
            set { m_exception = value; }
        }
    }
}
