﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
using Esri.ArcGISRuntime.Geometry;
using Esri.ArcGISRuntime.Layers;
using Esri.ArcGISRuntime.Symbology;
using Esri.ArcGISRuntime.Tasks.Geocoding;
using Esri.ArcGISRuntime.Security;
using System.Web.Script.Serialization;
using System.Runtime.CompilerServices;

using System.Diagnostics;


namespace N_POINT.MapTools
{
    public class Geocoder
    {
        private SpatialReference wgs84 = new SpatialReference(4326);
        private EsriGeocoder m_esriGeocoder = null;

        public Geocoder()
        {
            m_esriGeocoder = new EsriGeocoder();
            //var rslt = m_esriGeocoder.InitGeocoder();
            //rslt.Wait();
        }    

        public class GeocodingStatus
        {
            public int NumberSkipped { get; set; }
            public int NumberGeocoded { get; set; }
            public int NumberNotFound { get; set; }

            public int NumberSaveToHostErrors { get; set; }

            public GeocodingStatus()
            {
                NumberSkipped  = 0;
                NumberGeocoded = 0;
                NumberNotFound = 0;
            }
            
        }
        public async Task BatchGeocode_N(List<Data.Accounts> aAccounts, GeocodingStatus aStatus, bool aEmulateGeocode = false)
        {            
            string l_strException = "No Exceptions";

            bool l_boolSaveError = false;
            
            // format the address for the batch geocoding service and link the account object to the address
            List<EsriGeocoder.AddressFullInfo> lFullInfoAccounts = new List<EsriGeocoder.AddressFullInfo>();
            foreach (Data.Accounts acct in aAccounts)
            {
                if (acct.Address.Trim().Length == 0)
                {
                    aStatus.NumberSkipped++;
                }

                else
                {
                    lFullInfoAccounts.Add(new EsriGeocoder.AddressFullInfo(acct));
                }              
            }

            if (lFullInfoAccounts.Count > 0)
            {

                try
                {
                    await m_esriGeocoder.BatchGeocode(lFullInfoAccounts);
                }
                catch (Exception ex)
                {
                    //capture the error message from the exception
                    l_boolSaveError = true;
                    l_strException = ex.Message;
                }
            }           

            if (!l_boolSaveError)
            {
                foreach (EsriGeocoder.AddressFullInfo lFullInfo in lFullInfoAccounts)
                {                                     
                    if (lFullInfo.Account.SubmittedToGeocoder && lFullInfo.Account.GeocodeResult.ResultValid)
                    {
                        aStatus.NumberGeocoded++;
                    }
                    else
                    {
                        aStatus.NumberNotFound++;
                    }
                }
            }

            // if the batch mode threw an exception, throw an exception containing the text of the 
            // generated exception.
            else
            {
                throw new Exception(l_strException);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="a_Account"></param>
        /// <remarks>
        /// Revision History
        /// Date     Who Defect# Description
        /// -------- --- ------- ----------------------------------------    
        /// 10/17/14 BJC         Initial Version
        /// </remarks>
        public async Task<GeocodedResult> GeocodeSingleAddressAsync(Data.Accounts a_Account)
        {
            GeocodedResult l_gr = new GeocodedResult();
            try
            {
                var task = await m_esriGeocoder.GeocodeAddressAsync(a_Account, App.ConfigurationSettingsMgr.MinimumGeocodingHit, true);
                return task;
            }
            catch (Exception ex)
            {
                App.AppLogger.LogMsg(
                    Misc.Logger.enumProcessType.GEOCODER, 
                    Misc.Logger.enumLogType.ERROR, 
                    string.Format("Exception while attempting to geocode an address: {0}", 
                    Misc.Utility.GetAllExceptionMessages(ex)));
                l_gr.GeocodeException = ex;
            }

            return l_gr;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="a_strAddress"></param>
        /// <returns></returns>
        /// <remarks>
        /// Revision History
        /// Date     Who Defect# Description
        /// -------- --- ------- ----------------------------------------    
        /// 10/17/14 BJC         Initial Version
        /// </remarks>
        public async Task<GeocodedResult> GeocodeSingleAddressAsync(string a_strAddress)
        {
            var result = await m_esriGeocoder.GeocodeAddress(new EsriGeocoder.AddressFullInfo(a_strAddress), wgs84, App.ConfigurationSettingsMgr.MinimumGeocodingHit);
            return result;
        }
    }
}
