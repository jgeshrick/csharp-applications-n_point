﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace N_POINT.MapTools
{
    public class MapAreaInfo : ViewModelBase
    {
        double m_dblEastWestDistance;
        double m_dblNorthSouthDistance;
        double m_dblArea;
        double m_XMin;
        double m_XMax;
        double m_YMin;
        double m_YMax;


        public MapAreaInfo()
        {
            m_dblArea = 0;
            m_dblNorthSouthDistance = 0;
            m_dblEastWestDistance = 0;
        }

        public void UpdateMapAreInfo(MapAreaInfo a_item)
        {
            m_dblArea = a_item.AreaSize;
            EastWestDistance = a_item.EastWestDistance;
            NorthSouthDistance = a_item.NorthSouthDistance;
        }

        public double XMin
        {
            get { return m_XMin; }
            set { m_XMin = value; }
        }

        public double XMax
        {
            get { return m_XMax; }
            set { m_XMax = value; }
        }

        public double YMin
        {
            get { return m_YMin; }
            set { m_YMin = value; }
        }

        public double YMax
        {
            get { return m_YMax; }
            set { m_YMax = value; }
        }

        public double EastWestDistance
        {
            get
            {
                return m_dblEastWestDistance;
            }

            set
            {
                m_dblEastWestDistance = value;
                OnPropertyChanged("EastWestDistanceString");
            }
        }

        public double NorthSouthDistance
        {
            get
            {
                return m_dblNorthSouthDistance;
            }

            set
            {
                m_dblNorthSouthDistance = value;
                OnPropertyChanged("NorthSouthDistanceString");
                CalculateArea();
            }
        }

        public double AreaSize
        {
            get
            {
                return m_dblArea;
            }

            set
            {
                m_dblArea = value;
                OnPropertyChanged("AreaSizeString");
            }
        }

        public string EastWestDistanceString
        {
            get
            {
                string str;
                str = string.Format("{0:0.0} miles", m_dblEastWestDistance);

                return str;
            }
        }

        public string NorthSouthDistanceString
        {
            get
            {
                string l_str;
                l_str = string.Format("{0:0.0} miles", m_dblNorthSouthDistance);
                return l_str;
            }
        }

        public string AreaSizeString
        {
            get
            {
                string l_str;
                l_str = string.Format("{0:0.0} square miles", m_dblArea);
                return l_str;
            }
        }

        void CalculateArea()
        {
            if ((m_dblEastWestDistance != 0) && (m_dblNorthSouthDistance != 0))
            {
                AreaSize = m_dblNorthSouthDistance * m_dblEastWestDistance;
            }
        }
    }
}
