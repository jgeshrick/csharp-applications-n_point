﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace N_POINT.MapTools
{
    public class MapPointData
    {
        double m_dLongitude;
        double m_dLatitude;

        IDictionary<string, object> m_Attributes;

        public MapPointData()
        {
            m_dLatitude = 0;
            m_dLongitude = 0;

            m_Attributes = new Dictionary<string, object>();
        }

        public IDictionary<string, object> Attributes
        {
            get
            {
                return m_Attributes;
            }
        }


        public double Longitude
        {
            get
            {
                return m_dLongitude;
            }
            set
            {
                m_dLongitude = value;
            }
        }

        public double Latitude
        {
            get
            {
                return m_dLatitude;
            }

            set
            {
                m_dLatitude = value;
            }
        }
    }
}
