﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Diagnostics;

using Esri.ArcGISRuntime.Geometry;
using Esri.ArcGISRuntime.Layers;
using Esri.ArcGISRuntime.Security;
using Esri.ArcGISRuntime.Tasks.Geoprocessing;
using Esri.ArcGISRuntime.Tasks.Offline;
using Esri.ArcGISRuntime.Symbology;

namespace N_POINT.MapTools
{
    public class MapTPKGenerationTask :ViewModelBase
    {        
        public TilePackageRequest MyTilePackageRequest { get; set; }
        public bool Result { get; set; }
        int _PercentCompleteGeneration = 0;
        public int PercentCompleteGeneration
        {
            get { return _PercentCompleteGeneration; }
            set { _PercentCompleteGeneration = value; OnPropertyChanged("PercentCompleteGeneration"); }
        }
        int _PercentCompleteDownload = 0;
        public int PercentCompleteDownload
        {
            get { return _PercentCompleteDownload; }
            set { _PercentCompleteDownload = value; OnPropertyChanged("PercentCompleteDownload"); }
        }

        public Thread MyThread { get; private set; }

        MapTPKGenerator.MapTPKCancelAbortStatus mCancelAbortStatus = null;

        CancellationTokenSource mCancellationToken;

        ExportTileCacheTask mExportTileCacheTask;

        public MapTPKGenerationTask(
            ExportTileCacheTask aExportTileCacheTask,
            CancellationTokenSource aCancellationToken, 
            MapTPKGenerator.MapTPKCancelAbortStatus aCancelAbortStatus)
        {
            mExportTileCacheTask = aExportTileCacheTask;
            MyThread = null;
            MyTilePackageRequest = null;
            Result = false;
            PercentCompleteGeneration = 0;
            PercentCompleteDownload = 0;

            mCancelAbortStatus = aCancelAbortStatus;
            mCancellationToken = aCancellationToken;
        }

        bool _started = false;
        public bool Started
        {
            get { return _started; }
            set { _started = value; NotStarted = !value;  OnPropertyChanged("Started"); OnPropertyChanged("NotStarted"); }
        }

        public bool NotStarted
        {
            get; set;
        } = true;

        public void StartThread(
            bool aIsMX900Download, 
            string aOutputFileDestination,
            double aMinScale,
            double aMaxScale)
        {
            Started = true;
            MyThread = new Thread(new ThreadStart(() => 
                GenerateSingleTilePackage(
                    aIsMX900Download, 
                    aOutputFileDestination, 
                    aMinScale, 
                    aMaxScale,
                    mExportTileCacheTask)));
            MyThread.Start();
        }

        private void GenerateSingleTilePackage(
            bool aIsMX900Download, 
            string aOutputFileDestination,
            double aMinScale,
            double aMaxScale,
            ExportTileCacheTask aExportTileTask)
        {
            Result = false;

            // The API downloads the file into 'layers.tpk' so we need to rename it to what the user selected.
            string l_strDestFolder = "";

            if (aIsMX900Download)
            {
                // place destination files in the proper folder - Map for tiles and Satellite for satellite tiles.
                if (MyTilePackageRequest.TilePackageType == TilePackageRequest.TILE_PACKAGE_TYPE.TPT_TILES)
                {
                    l_strDestFolder = string.Format("{0}\\Map", aOutputFileDestination);
                }
                else
                {
                    l_strDestFolder = string.Format("{0}\\Satellite", aOutputFileDestination);
                }
            }
            else
            {
                if (MyTilePackageRequest.TilePackageType == TilePackageRequest.TILE_PACKAGE_TYPE.TPT_TILES)
                {
                    l_strDestFolder = string.Format("{0}\\Layers\\Map", aOutputFileDestination);
                }
                else
                {
                    l_strDestFolder = string.Format("{0}\\Layers\\Satellite", aOutputFileDestination);
                }
                System.IO.Directory.CreateDirectory(l_strDestFolder);
            }

            GenerateTileCacheParameters l_genOptions = new GenerateTileCacheParameters()
            {
                CompressTileCache = true,
                CompressionQuality = 80,
                Format = ExportTileCacheFormat.TilePackage,
                MinScale = aMinScale,
                MaxScale = aMaxScale,
                GeometryFilter = MyTilePackageRequest.m_Extent
            };

            // setup the download options
            var downloadOptions = new DownloadTileCacheParameters(l_strDestFolder)
            {
                OverwriteExistingFiles = true,
                CacheName = MyTilePackageRequest.TPKFileName
            };

            App.AppLogger.LogMsg(Misc.Logger.enumProcessType.TILECACHER, Misc.Logger.enumLogType.GENERAL, string.Format("Downloading TPK {0}", MyTilePackageRequest.m_strFileName));

            ExportTileCacheJob res = null;
            GenerateTileCacheResult genResult = null;

            int lAttemptCount = 1;
            while( (!Result) && (!mCancelAbortStatus.AbortOperation) )
            {                
                try
                {
                    // start generating the tile cache                
                    App.AppLogger.LogMsg(
                            Misc.Logger.enumProcessType.TILECACHER,
                            Misc.Logger.enumLogType.GENERAL,
                            string.Format("{0} - GenerateTileCacheAsync", MyTilePackageRequest.TPKFileName));
                    res = aExportTileTask.GenerateTileCacheAsync(l_genOptions).Result;
                    App.AppLogger.LogMsg(
                            Misc.Logger.enumProcessType.TILECACHER,
                            Misc.Logger.enumLogType.GENERAL,
                            string.Format("{0} - GenerateTileCacheAsync waiting for result", MyTilePackageRequest.TPKFileName));
                    // check status of the tile cache generation
                    while (res.Status != GPJobStatus.Cancelled && res.Status != GPJobStatus.Deleted
                        && res.Status != GPJobStatus.Succeeded && res.Status != GPJobStatus.TimedOut
                        && res.Status != GPJobStatus.Failed)
                    {
                        Thread.Sleep(2000);
                        genResult = aExportTileTask.CheckGenerateTileCacheStatusAsync(res).Result;
                        if (mCancelAbortStatus.AbortOperation)
                        {
                            break;
                        }
                        PercentCompleteGeneration = GetPercentCompleteGeneration(res);
                    }
                    
                    // check status of the tile cache generation
                    // tile cache success
                    if ((!mCancelAbortStatus.AbortOperation) && (res.Status == GPJobStatus.Succeeded))
                    {
                        // set generation percent to 100 in case we missed status above
                        PercentCompleteGeneration = 100;

                        App.AppLogger.LogMsg(
                            Misc.Logger.enumProcessType.TILECACHER,
                            Misc.Logger.enumLogType.GENERAL,
                            string.Format("{0} - DownloadTileCacheAsync", MyTilePackageRequest.TPKFileName));
                        var rslt = aExportTileTask.DownloadTileCacheAsync(
                            genResult,
                            downloadOptions,
                            mCancellationToken.Token,
                            new Progress<ExportTileCacheDownloadProgress>((downloadProgress) =>
                            {
                                ProcessDownloadStatusMessages(downloadProgress);
                            })).Result;

                        // set generation percent to 100 in case we missed status above
                        PercentCompleteDownload = 100;

                        // set success result
                        Result = true;
                    }

                    // report failure if user did not abort and not tile cache not successful
                    else if (res.Status != GPJobStatus.Succeeded)
                    {
                        App.AppLogger.LogMsg(
                            Misc.Logger.enumProcessType.TILECACHER,
                            Misc.Logger.enumLogType.ERROR,
                            string.Format("{0} - Unable to generate the tile cache, error: {1}",
                            MyTilePackageRequest.TPKFileName,
                            res.Status.ToString()));
                    }
                }
                catch (Exception ex)
                {
                    // unable to download
                    App.AppLogger.LogMsg(Misc.Logger.enumProcessType.TILECACHER, Misc.Logger.enumLogType.ERROR, string.Format("Unable to save TPK file '{0}'.  Error: {1}", MyTilePackageRequest.TPKFileName, ex.Message));
                    App.AppLogger.LogMsg(Misc.Logger.enumProcessType.TILECACHER, Misc.Logger.enumLogType.DEBUG, string.Format("Stack Trace: {0}", ex.StackTrace));
                }

                if(! Result)
                {
                    if (lAttemptCount == 5)
                    {
                        App.AppLogger.LogMsg(
                            Misc.Logger.enumProcessType.TILECACHER,
                            Misc.Logger.enumLogType.ERROR,
                            string.Format("{0} - DownloadTileCacheAsync failed, max attempt count reached, ABORTING", MyTilePackageRequest.TPKFileName));
                        break;
                    }
                    else
                    {
                        Debug.WriteLine(string.Format("{0} - DownloadTileCacheAsync failed, trying again", MyTilePackageRequest.TPKFileName));
                    }
                }
                lAttemptCount++;
            }

            // cleanup any temp files
            try
            {
                string lPath = System.IO.Path.Combine(l_strDestFolder, MyTilePackageRequest.TPKFileName + ".tmp");
                if (System.IO.File.Exists(lPath))
                {
                    System.IO.File.Delete(lPath);
                }
            }
            catch (Exception) { }

            if (Result)
            {
                App.AppLogger.LogMsg(
                    Misc.Logger.enumProcessType.TILECACHER,
                    Misc.Logger.enumLogType.GENERAL,
                    string.Format("{0} - Tile package created, SUCCESS", MyTilePackageRequest.TPKFileName));
            }
            else
            {
                if (mCancelAbortStatus.AbortOperation)
                {
                    App.AppLogger.LogMsg(
                        Misc.Logger.enumProcessType.TILECACHER,
                        Misc.Logger.enumLogType.GENERAL,
                        string.Format("{0} - Tile package created ABORTED", MyTilePackageRequest.TPKFileName));
                }
                else
                {
                    App.AppLogger.LogMsg(
                        Misc.Logger.enumProcessType.TILECACHER,
                        Misc.Logger.enumLogType.ERROR,
                        string.Format("{0} - Tile package creation FAILED", MyTilePackageRequest.TPKFileName));
                }
            }
        }

        /// <summary>
        /// Process the messages from the tile generating service to determine it's progress
        /// in generating the TPK file.
        /// </summary>
        /// <param name="downloadProgress"></param>
        void ProcessDownloadStatusMessages(ExportTileCacheDownloadProgress downloadProgress)
        {
            // If the operation has been aborted by the user and the cancellation token hasn't been registered
            // as cancelled then go ahead and cancel it now.  This is to work around an issue with Esri API and
            // cancelling an operation during the TPK generation phase.
            if (mCancelAbortStatus.UserCancelled && !mCancellationToken.IsCancellationRequested)
            {
                mCancellationToken.Cancel();
            }
            PercentCompleteDownload = (int)(100 * downloadProgress.ProgressPercentage);
            //App.AppLogger.LogMsg(Misc.Logger.enumProcessType.TILECACHER, Misc.Logger.enumLogType.DEBUG, string.Format("Download Progress Report"));
            //App.AppLogger.LogMsg(Misc.Logger.enumProcessType.TILECACHER, Misc.Logger.enumLogType.DEBUG, string.Format("  --Percent Complete: {0}", downloadProgress.ProgressPercentage));
            //App.AppLogger.LogMsg(Misc.Logger.enumProcessType.TILECACHER, Misc.Logger.enumLogType.DEBUG, string.Format("  --Current Bytes rcvd: {0}", downloadProgress.CurrentFileBytesReceived));
            //App.AppLogger.LogMsg(Misc.Logger.enumProcessType.TILECACHER, Misc.Logger.enumLogType.DEBUG, string.Format("Download Progress Report - END OF REPORT THIS INTERVAL"));
        }

        private int GetPercentCompleteGeneration(ExportTileCacheJob job)
        {
            double lPercent = 0;

            // from https://developers.arcgis.com/net/desktop/guide/create-an-offline-app.htm
            foreach (var m in job.Messages)
            {
                // find messages with percent complete
                // "Finished:: 9 percent", e.g.
                if (m.Description.Contains("Finished::"))
                {
                    // parse out the percentage complete and update the progress bar
                    var numString = m.Description.Substring(m.Description.IndexOf("::") + 2, 3).Trim();
                    double.TryParse(numString, out lPercent);
                }
            }

            return (int)lPercent;
        }
    }
}
