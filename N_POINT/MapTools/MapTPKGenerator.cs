﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
using System.Threading;
using System.Xml;
using System.Diagnostics;

using Esri.ArcGISRuntime.Geometry;
using Esri.ArcGISRuntime.Layers;
using Esri.ArcGISRuntime.Security;
using Esri.ArcGISRuntime.Tasks.Geoprocessing;
using Esri.ArcGISRuntime.Tasks.Offline;
using Esri.ArcGISRuntime.Symbology;

namespace N_POINT.MapTools
{
    public class MapTPKGenerator
    {
        // used to divide total number of tiles to determine how many TPK files we need
        const int MAX_TILES_THRESHOLD = 99000;
        public const int MAX_TILES_ALLOWED = 50000000;

        CancellationTokenSource m_CTS;
        private int m_nCacheSize;

        public class MapTPKCancelAbortStatus
        {
            public bool UserCancelled { get; set; }
            public bool AbortOperation
            {
                get;
                set;
            }
            public MapTPKCancelAbortStatus()
            {
                UserCancelled = false;
                AbortOperation = false;
            }
        }
        MapTPKCancelAbortStatus mCancelAbortStatus = new MapTPKCancelAbortStatus();
        public MapTPKCancelAbortStatus CancelAbortStatus { get { return mCancelAbortStatus; } }

        bool m_boolOperationSuccess = false;
        int m_nMinLevel;
        int m_nMaxLevel;
        string m_strLastErrorMessage;
        TilePackageStatus m_TPS;
        private bool m_boolGenerateSatellite = false;
        private GenerateTileCacheParameters m_genOptions;
        static ObservableCollection<TilePackageRequest> m_TPKFiles = null;
        static string m_strOutputFileDestination = "";
        static bool m_boolIsMX900Download = true;
        private int m_nNumTilesNeeded;
        Thread m_workerThread;
        IReadOnlyList<Esri.ArcGISRuntime.ArcGISServices.Lod> mMapLevelsOfDetail;
        ObservableCollection<MapTPKGenerationTask> mTasks = new ObservableCollection<MapTPKGenerationTask>();
        public ObservableCollection<MapTPKGenerationTask> Tasks { get { return mTasks; } }

        public bool Cancelled { get { return CancelAbortStatus.UserCancelled; } }

        public delegate void TaskCompletedEvent(TilePackageStatus a_TPS);
        public event TaskCompletedEvent EventTaskStatus;

        public enum TASKS
        {
            TASK_STATUS,
            TASK_CACHE_ESTIMATION,
            TASK_PACKAGE_DOWNLOAD,
            TASK_INVALID_REQUEST,
            TASK_DOWNLOAD_COMPLETE
        }

        public TilePackageStatus CurrentStatus
        {
            get
            {
                return m_TPS;
            }
        }

        private ExportTileCacheTask GetNewTilesTask(TilePackageRequest.TILE_PACKAGE_TYPE aPackageType)
        {
			ExportTileCacheTask lReturn = null;

			switch (aPackageType)
            {                
                case TilePackageRequest.TILE_PACKAGE_TYPE.TPT_SATELLITE:
					lReturn = new ExportTileCacheTask(new Uri(EsriArcgisRuntimeInterface.DEFAULT_ONLINE_SATELLITE_BASEMAP_URL_TILES));
					break;
                case TilePackageRequest.TILE_PACKAGE_TYPE.TPT_TILES:
                default:
					lReturn = new ExportTileCacheTask(new Uri(EsriArcgisRuntimeInterface.DEFAULT_ONLINE_BASEMAP_URL_TILES));
					break;
            }

			lReturn.Token = MainWindow.Token;
			return lReturn;
            
        }

        public MapTPKGenerator(
            IReadOnlyList<Esri.ArcGISRuntime.ArcGISServices.Lod> aMapLevelsOfDetail)
        {
            mMapLevelsOfDetail = aMapLevelsOfDetail;
            m_TPS = new TilePackageStatus();            
        }

        public string LastErrorMessage
        {
            get { return m_strLastErrorMessage; }
        }

        public bool OperationSuccess
        {
            get { return m_boolOperationSuccess; }
        }

        public int EstimatedCacheSize
        {
            get
            {
                return m_nCacheSize;
            }
            set
            {
                m_nCacheSize = value;
            }
        }

        public void CancelOperation()
        {            
            CancelAbortStatus.UserCancelled = true;
            CancelAbortStatus.AbortOperation = true;
            if (m_CTS != null)
            {
                m_CTS.Cancel();
                // just register that the operation is being aborted
            }
        }

        /// <summary>
        /// Determine the number of requests required based on the maximum tile request size to download the entire 
        /// set of TPK files.  If the number of tiles requested exceeds the defined MAX_TILES_THRESHOLD value, 
        /// calculate the number of requests that will be needed to generate a TPK set for the entire map area the
        /// user requested.
        /// </summary>
        /// <returns></returns>
        /// <remarks>
        /// Revision History
        /// Date     Who Defect# Description
        /// -------- --- ------- ----------------------------------------    
        /// 10/27/14 BJC         Initial Version
        /// </remarks>
        private int CalculateNumberOfRequestsRequired()
        {
            int l_nNumRequests;

            // Calculate the number of requests to the service to make to fulfill the complete tile set request.
            double l_dblNumRequests;
            l_dblNumRequests = (double)((double)m_nNumTilesNeeded / (double)MAX_TILES_THRESHOLD);
            l_dblNumRequests = Math.Ceiling(l_dblNumRequests);

            l_nNumRequests = (int)l_dblNumRequests;

            if (l_nNumRequests == 0)
            {
                l_nNumRequests = 1;
            }

            return l_nNumRequests;
        }

        /// <summary>
        /// Sends a request to ArcGISOnline to estimate the size of the area displayed on the map.
        /// </summary>
        /// <param name="a_nMinLevel"></param>
        /// <param name="a_nMaxLevel"></param>
        /// <param name="a_boolSatellite"></param>
        /// <remarks>
        /// Revision History
        /// Date     Who Defect# Description
        /// -------- --- ------- ----------------------------------------    
        /// 10/27/14 BJC         Initial Version
        /// </remarks>
        public async void EstimateCacheSize(int a_nMinLevel, int a_nMaxLevel, bool a_boolSatellite, Envelope aEnvelope)
        {
            m_boolOperationSuccess = false;
            CancelAbortStatus.AbortOperation = false;
            m_strLastErrorMessage = "";
            m_TPS.Init();
            App.AppLogger.LogMsg(Misc.Logger.enumProcessType.TILECACHER, Misc.Logger.enumLogType.GENERAL, "Estimating TPK file size");
            UpdateCurrentStatus(TASKS.TASK_CACHE_ESTIMATION, "Estimating TPK file size");

            m_boolGenerateSatellite = a_boolSatellite;

            int l_nCacheSize;
            int l_nTileCount;

            // Covers the selected the selected map area in a shade of red
            //m_AreaOfInterestLayer.Graphics.Clear();
            //m_AreaOfInterestLayer.Graphics.Add(new Graphic(m_MapView.Extent));

            m_nMinLevel = a_nMinLevel;
            m_nMaxLevel = a_nMaxLevel;

            m_genOptions = new GenerateTileCacheParameters()
            {
                Format = ExportTileCacheFormat.TilePackage,
                MinScale = mMapLevelsOfDetail[m_nMinLevel].Scale,
                MaxScale = mMapLevelsOfDetail[m_nMaxLevel].Scale,
                GeometryFilter = aEnvelope
            };

            try
            {

                ExportTileCacheTask lTask = GetNewTilesTask(TilePackageRequest.TILE_PACKAGE_TYPE.TPT_TILES);
				
                var job = await lTask.EstimateTileCacheSizeAsync(m_genOptions);
                App.AppLogger.LogMsg(Misc.Logger.enumProcessType.TILECACHER, Misc.Logger.enumLogType.DEBUG, "Estimating Tile Cache Size");
                // Poll for the results async
                while (job.Status != GPJobStatus.Cancelled && job.Status != GPJobStatus.Deleted
                    && job.Status != GPJobStatus.Succeeded && job.Status != GPJobStatus.TimedOut
                    && job.Status != GPJobStatus.Failed)
                {
                    await Task.Delay(TimeSpan.FromSeconds(5));
                    await lTask.CheckEstimateTileCacheSizeJobStatusAsync(job);

                    if (CancelAbortStatus.AbortOperation)
                    {
                        break;
                    }
                    ProcessStatusMessages(job);

                }

                // If we didn't abort the operation calculate the tile cache size.
                if (!CancelAbortStatus.AbortOperation)
                {
                    var result = await lTask.CheckEstimateTileCacheSizeJobStatusAsync(job);

                    l_nCacheSize = 0;

                    if (job.Status == GPJobStatus.Succeeded)
                    {
                        l_nCacheSize = result.Size;
                        l_nTileCount = result.TileCount;

                        m_TPS.TilePackageFileSize = l_nCacheSize;
                        m_TPS.NumberOfTiles = result.TileCount;

                    }
                    else
                    {
                        //
                        // The request failed. If it failed because the max tile count was exceeded, find out how many 
                        // tiles the request generated.
                        //
                        string l_strTileCountMsg = "";
                        l_nTileCount = 0;
                        foreach (GPMessage msg in job.Messages)
                        {
                            // error 0015654 is the tile count exceeded error message
                            if (msg.Description.Contains("ERROR 001564"))
                            {
                                l_strTileCountMsg = msg.Description;
                                // break out of the loop once we find the message with the number of tiles the request generated
                                break;
                            }
                        }

                        if (l_strTileCountMsg.Length > 0)
                        {
                            l_nTileCount = GetTileCountFromErrorMsg(l_strTileCountMsg);

                            m_TPS.TilePackageFileSize = 0;
                            m_TPS.NumberOfTiles = l_nTileCount;
                        }
                    }

                    if ((l_nTileCount > 0) && (l_nTileCount < MAX_TILES_ALLOWED))
                    {
                        m_nNumTilesNeeded = l_nTileCount;
                        App.AppLogger.LogMsg(Misc.Logger.enumProcessType.TILECACHER, Misc.Logger.enumLogType.DEBUG, string.Format("Tile Cache Size Estimation Complete: Tiles: {0}, Size: {1}", l_nTileCount, m_TPS.TilePackageFileSize));
                        BuildTilePackageRequestList(aEnvelope);

                        UpdateCurrentStatus(TASKS.TASK_PACKAGE_DOWNLOAD, "TPK Estimation complete");
                    }
                    else
                    {
                        UpdateCurrentStatus(TASKS.TASK_INVALID_REQUEST, "Task estimation failed.");
                    }
                }
                else
                {
                    UpdateCurrentStatus(TASKS.TASK_DOWNLOAD_COMPLETE, "");
                }
            }
            catch (Exception ex)
            {
                m_TPS.ProcessingSubStatus = "";
                App.AppLogger.LogMsg(Misc.Logger.enumProcessType.TILECACHER, Misc.Logger.enumLogType.ERROR, string.Format("Error generating tile package file. Exception: {0}", ex.Message));
                App.AppLogger.LogMsg(Misc.Logger.enumProcessType.TILECACHER, Misc.Logger.enumLogType.DEBUG, string.Format("Stack Trace: {0}", ex.StackTrace));
                m_strLastErrorMessage = ex.Message;
                UpdateCurrentStatus(TASKS.TASK_DOWNLOAD_COMPLETE, "Tile Cache Exception.");
            }
        }

        /// <summary>
        /// Processes the string that contains the error message that describes why a tile estimation job failed
        /// if it failed because the user makes a request that generates a number of tiles that exceeds the service
        /// limits.
        /// </summary>
        /// <param name="a_strErrorMsg"></param>
        /// <returns></returns>
        /// <remarks>
        /// Revision History
        /// Date     Who Defect# Description
        /// -------- --- ------- ----------------------------------------    
        /// 10/27/14 BJC         Initial Version
        /// </remarks>
        int GetTileCountFromErrorMsg(string a_strErrorMsg)
        {
            int l_nTileCount = 0;
            //ERROR 001564: Requested tile count(278706) exceeds the maximum allowed number of tiles(100000) to be exported for service World_Street_Map:MapServer.
            int l_nIndex = a_strErrorMsg.IndexOf("count(");
            if (l_nIndex >= 0)
            {
                int l_nIndex2;
                int l_nLength;
                l_nIndex2 = a_strErrorMsg.IndexOf(") ", l_nIndex);
                if (l_nIndex2 > l_nIndex)
                {
                    string strTileCount;
                    l_nIndex += "count(".Length;
                    l_nLength = l_nIndex2 - l_nIndex;
                    strTileCount = a_strErrorMsg.Substring(l_nIndex, l_nLength);
                    if (!int.TryParse(strTileCount, out l_nTileCount))
                    {
                        l_nTileCount = 0;
                    }
                }
            }

            return l_nTileCount;
        }

        /// <summary>
        /// Creates a list of tile package request files.  Multiple files are needed if the user adds a satellite tile package
        /// request or the user generates a request that results in more than 100,000 tiles.
        /// </summary>
        /// <remarks>
        /// Revision History
        /// Date     Who Defect# Description
        /// -------- --- ------- ----------------------------------------    
        /// 10/27/14 BJC         Initial Version
        /// </remarks>
        void BuildTilePackageRequestList(Envelope aEnvelope)
        {
            int l_nNumberOfRequests;

            if (m_TPKFiles == null)
            {
                m_TPKFiles = new ObservableCollection<TilePackageRequest>();
            }
            else
            {
                m_TPKFiles.Clear();
            }

            // Calculate the number of requests needed
            l_nNumberOfRequests = CalculateNumberOfRequestsRequired();

            if (l_nNumberOfRequests == 0)
            {
                l_nNumberOfRequests = 1;
            }

            int l_nTotalRows = 0;
            int l_nTotalColumns = 0;

            // Determine how the sub-sections of the map will be arranged, if it will be necessary
            DetermineTileSetArrangement(ref l_nNumberOfRequests, out l_nTotalRows, out l_nTotalColumns);

            double dbgXMIN, dbgXMAX, dbgYMIN, dbgYMAX;

            dbgXMIN = aEnvelope.XMin;
            dbgXMAX = aEnvelope.XMax;
            dbgYMIN = aEnvelope.YMin;
            dbgYMAX = aEnvelope.YMax;

            double l_dblLeft, l_dblRight, l_dblTop, l_dblBottom;

            // root file name to apply to all maps generated
            string strMapRoot = App.ConfigurationSettingsMgr.MapRootName;

            if (l_nNumberOfRequests > 1)
            {
                int l_nRow = 0, l_nColumn = 0;

                double l_dblUnitsPerCol;
                double l_dblUnitsPerRow;
                double l_dblDistance;

                l_dblDistance = dbgXMAX - dbgXMIN;
                l_dblUnitsPerCol = (l_dblDistance / (double)l_nTotalColumns);

                l_dblDistance = dbgYMAX - dbgYMIN;
                l_dblUnitsPerRow = (l_dblDistance / (double)l_nTotalRows);

                l_nRow = 0;
                l_dblBottom = dbgYMIN;
                l_dblTop = dbgYMIN + l_dblUnitsPerRow;
                int l_nFileCount = 0;
                while (l_nRow < l_nTotalRows)
                {
                    l_nColumn = 0;
                    l_dblLeft = dbgXMIN;
                    l_dblRight = dbgXMIN + l_dblUnitsPerCol;

                    while (l_nColumn < l_nTotalColumns)
                    {
                        // create a request for the tpk file
                        l_nFileCount++;
                        TilePackageRequest tpr = new TilePackageRequest();
                        tpr.m_Extent = new Envelope(l_dblLeft, l_dblBottom, l_dblRight, l_dblTop, aEnvelope.SpatialReference);
                        tpr.TilePackageType = TilePackageRequest.TILE_PACKAGE_TYPE.TPT_TILES;
                        tpr.TPKFileName = string.Format("{0}[{1}]_Map.tpk", strMapRoot, l_nFileCount);
                        m_TPKFiles.Add(tpr);

                        if (m_boolGenerateSatellite)
                        {
                            // create a request for the satellite tpk file using the same extent
                            tpr = new TilePackageRequest();
                            tpr.m_Extent = new Envelope(l_dblLeft, l_dblBottom, l_dblRight, l_dblTop, aEnvelope.SpatialReference);
                            tpr.TilePackageType = TilePackageRequest.TILE_PACKAGE_TYPE.TPT_SATELLITE;
                            tpr.TPKFileName = string.Format("{0}[{1}]_Satellite.tpk", strMapRoot, l_nFileCount);
                            m_TPKFiles.Add(tpr);
                        }

                        l_dblLeft += l_dblUnitsPerCol;
                        l_dblRight += l_dblUnitsPerCol;
                        l_nColumn++;
                    }

                    l_dblBottom += l_dblUnitsPerRow;
                    l_dblTop += l_dblUnitsPerRow;
                    l_nRow++;
                }
            }
            else
            {
                // only one file needed to fulfill the request for map tiles
                l_dblLeft = aEnvelope.XMin;
                l_dblRight = aEnvelope.XMax;
                l_dblBottom = aEnvelope.YMin;
                l_dblTop = aEnvelope.YMax;
                TilePackageRequest tpr = new TilePackageRequest();
                tpr.m_Extent = aEnvelope;
                tpr.TilePackageType = TilePackageRequest.TILE_PACKAGE_TYPE.TPT_TILES;
                tpr.TPKFileName = string.Format("{0}_Map.tpk", strMapRoot);
                //m_TilePackageRequests.Add(tpr);
                m_TPKFiles.Add(tpr);

                if (m_boolGenerateSatellite)
                {
                    // add a request for the satellite view of the same area
                    tpr = new TilePackageRequest();
                    tpr.m_Extent = aEnvelope;
                    tpr.TilePackageType = TilePackageRequest.TILE_PACKAGE_TYPE.TPT_SATELLITE;
                    tpr.TPKFileName = string.Format("{0}_Satellite.tpk", strMapRoot);
                    m_TPKFiles.Add(tpr);
                }
            }
        }

        /// <summary>
        /// Determines how to divide up an area the user has chosen to generate a TPK file for.  If the request
        /// generates more than one TPK file, this function determines how sub areas are organized in the tile
        /// package set generated.
        /// </summary>
        /// <param name="a_nNumRequests"></param>
        /// <param name="a_nTotalRows"></param>
        /// <param name="a_nTotalColumns"></param>
        /// <remarks>
        /// Revision History
        /// Date     Who Defect# Description
        /// -------- --- ------- ----------------------------------------    
        /// 10/27/14 BJC         Initial Version
        /// </remarks>
        void DetermineTileSetArrangement(ref int a_nNumRequests, out int a_nTotalRows, out int a_nTotalColumns)
        {
            switch (a_nNumRequests)
            {
                case 1:
                    a_nTotalRows = 1;
                    a_nTotalColumns = 1;
                    break;
                case 2:
                    a_nTotalRows = 1;
                    a_nTotalColumns = 2;
                    break;
                case 3:
                    a_nNumRequests = 4;
                    a_nTotalRows = 2;
                    a_nTotalColumns = 2;
                    break;
                case 4:
                    a_nTotalRows = 2;
                    a_nTotalColumns = 2;
                    break;
                case 5:
                    a_nNumRequests = 6;
                    a_nTotalRows = 2;
                    a_nTotalColumns = 3;
                    break;
                case 6:
                    a_nTotalRows = 2;
                    a_nTotalColumns = 3;
                    break;
                case 7:
                    a_nNumRequests = 8;
                    a_nTotalRows = 2;
                    a_nTotalColumns = 4;
                    break;
                case 8:
                    a_nTotalRows = 2;
                    a_nTotalColumns = 4;
                    break;
                case 9:
                    a_nTotalRows = 3;
                    a_nTotalColumns = 3;
                    break;
                case 10:
                    a_nNumRequests = 12;
                    a_nTotalRows = 3;
                    a_nTotalColumns = 4;
                    break;
                case 11:
                    a_nNumRequests = 12;
                    a_nTotalRows = 3;
                    a_nTotalColumns = 4;
                    break;
                case 12:
                    a_nTotalRows = 3;
                    a_nTotalColumns = 4;
                    break;
                default:
                    a_nTotalRows = (int) (Math.Sqrt(a_nNumRequests) + 0.5);
                    a_nTotalColumns = 1;
                    while(a_nTotalRows * a_nTotalColumns < a_nNumRequests)
                    {
                        a_nTotalColumns++;
                    }
                    a_nNumRequests = a_nTotalRows * a_nTotalColumns;
                    break;
            }
        }

        void UpdateGenerationStatus(Object state)
        {
            int lPercent = 0;
            foreach (MapTPKGenerationTask lTask in mTasks)
            {
                lPercent += lTask.PercentCompleteGeneration + lTask.PercentCompleteDownload; 
            }

            lPercent = lPercent / (mTasks.Count*2);

            m_TPS.PercentComplete = lPercent;
            UpdateCurrentStatus(TASKS.TASK_STATUS, "Generating TPK files");
        }

        /// <summary>
        /// Updates the status display of a tile caching operation.
        /// </summary>
        /// <param name="a_task"></param>
        /// <param name="a_strStatus"></param>
        /// <remarks>
        /// Revision History
        /// Date     Who Defect# Description
        /// -------- --- ------- ----------------------------------------    
        /// 10/27/14 BJC         Initial Version
        /// </remarks>
        void UpdateCurrentStatus(TASKS a_task, string a_strStatus)
        {
            m_TPS.TaskStatus = a_task;
            m_TPS.ProcessingStatus = a_strStatus;

            // Let anyone that is subscribed to the update event to know about the status update
            if (EventTaskStatus != null)
            {
                EventTaskStatus(m_TPS);
            }
        }

        void ProcessStatusMessages(ExportTileCacheJob job)
        {
            App.AppLogger.LogMsg(Misc.Logger.enumProcessType.TILECACHER, Misc.Logger.enumLogType.DEBUG, string.Format("Export Tile Cache job Status Messages - Status: {0}", job.Status.ToString()));
            foreach (GPMessage msg in job.Messages)
            {
                App.AppLogger.LogMsg(Misc.Logger.enumProcessType.TILECACHER, Misc.Logger.enumLogType.DEBUG, string.Format("Message: {0}", msg.Description));
                string l_str;
                int i1, i2, i3;
                l_str = msg.Description.ToLower();
                // 'finished::' indicates processing is in the tile cache generation phase
                if (l_str.IndexOf("finished::") != -1)
                {
                    if (CancelAbortStatus.AbortOperation)
                    {
                        App.AppLogger.LogMsg(Misc.Logger.enumProcessType.TILECACHER, Misc.Logger.enumLogType.DEBUG, "Abort detected in generation phase...");
                    }

                    i1 = l_str.IndexOf("finished::");
                    i2 = l_str.IndexOf("percent");
                    i1 += "finished::".Length;
                    i3 = i2 - i1;
                    if (i3 > 0)
                    {
                        l_str = l_str.Substring(i1, i3);
                        if (int.TryParse(l_str, out i1))
                        {
                            m_TPS.PercentComplete = i1;
                            string str = m_TPS.ProcessingStatus;
                            m_TPS.ProcessingSubStatus = string.Format(" (Generating {0} %)", i1);
                            App.AppLogger.LogMsg(Misc.Logger.enumProcessType.TILECACHER, Misc.Logger.enumLogType.DEBUG, string.Format("Setting sub status: {0}", m_TPS.ProcessingSubStatus));
                            UpdateCurrentStatus(TASKS.TASK_STATUS, str);
                        }
                    }
                }
                else
                {
                    // We're not in the generation phase, allow cancellation
                    if (CancelAbortStatus.AbortOperation)
                    {
                        App.AppLogger.LogMsg(Misc.Logger.enumProcessType.TILECACHER, Misc.Logger.enumLogType.DEBUG, "Abort detected");
                    }
                }
            }

            App.AppLogger.LogMsg(Misc.Logger.enumProcessType.TILECACHER, Misc.Logger.enumLogType.DEBUG, string.Format("Export Tile Cache job Status Messages - END OF MESSAGES THIS REPORT INTERVAL"));
        }

        public void StartDownload(string strLocation)
        {
            m_workerThread = new Thread(Download);
            m_workerThread.Start(strLocation);
        }

        public void Download(object obj)
        {
            GenerateTileCacheAndDownload((string)obj);
        }

        /// <summary>
        /// Generates a TPK file on the ArcGISOnline server and downloads it when complete
        /// </summary>
        /// <param name="a_strLocation"></param>
        public void GenerateTileCacheAndDownload(string a_strLocation)
        {
            m_boolOperationSuccess = false;         
            m_TPS.PercentComplete = 0;
            SynchronizationContext.SetSynchronizationContext(new SynchronizationContext());

            App.AppLogger.LogMsg(Misc.Logger.enumProcessType.TILECACHER, Misc.Logger.enumLogType.GENERAL, "Generating and downloading tile package file(s)");

            m_CTS = new CancellationTokenSource();

            UpdateCurrentStatus(TASKS.TASK_STATUS, "Downloading TPK file(s)...");

            MainWindow.MyDispatcher.Invoke(() => mTasks.Clear());
            Stopwatch lWatch = new Stopwatch();
            lWatch.Start();

            Debug.WriteLine("*** Starting TPK threads");
            foreach (TilePackageRequest l_tpr in m_TPKFiles)
            {
                UpdateCurrentStatus(TASKS.TASK_STATUS, "Generating TPK files");
                MapTPKGenerationTask lTask = new MapTPKGenerationTask(GetNewTilesTask(l_tpr.TilePackageType), m_CTS, CancelAbortStatus) { MyTilePackageRequest = l_tpr };
                MainWindow.MyDispatcher.Invoke(() => mTasks.Add(lTask));
            }

            // kick off timer to update progress percentage
            Timer lTimer = new Timer(UpdateGenerationStatus, null, 1000, 2000);            

            // set result to success for now
            m_boolOperationSuccess = true;

            // start the first 5 threads
            for(int i=0; i<mTasks.Count; i++)
            {
                MapTPKGenerationTask lTask = mTasks[i];
                Debug.WriteLine(string.Format("Starting task: {0}/{1}", i.ToString() + 1, mTasks.Count));
                lTask.StartThread(
                    m_boolIsMX900Download,
                    m_strOutputFileDestination,
                    mMapLevelsOfDetail[m_nMinLevel].Scale,
                    mMapLevelsOfDetail[m_nMaxLevel].Scale);                
                if (i == 4) { break; }
            }

            // wait for each thread and check results of the threads
            Debug.WriteLine("*** Waiting on TPK threads");
            foreach (MapTPKGenerationTask lTask in mTasks)
            {
                lTask.MyThread.Join();
                if (!lTask.Result)
                {
                    m_boolOperationSuccess = false;

                    // set cancel and aborted to stop any other threads                    
                    CancelAbortStatus.AbortOperation = true;
                    m_CTS.Cancel();
                }

                // start another task
                for (int i = 0; i < mTasks.Count; i++)
                {
                    MapTPKGenerationTask lNextTask = mTasks[i];
                    if (!lNextTask.Started)
                    {
                        Debug.WriteLine(string.Format("Starting task: {0}/{1}", i.ToString() + 1, mTasks.Count));
                        lNextTask.StartThread(
                            m_boolIsMX900Download,
                            m_strOutputFileDestination,
                            mMapLevelsOfDetail[m_nMinLevel].Scale,
                            mMapLevelsOfDetail[m_nMaxLevel].Scale);
                        break;
                    }
                }
            }

            // kill the timer
            lTimer.Dispose();

            // Time: 104273ms
            Debug.WriteLine("Time: " + lWatch.ElapsedMilliseconds.ToString() + "ms");

            // user cancelled
            if (CancelAbortStatus.UserCancelled)
            {
                UpdateCurrentStatus(TASKS.TASK_DOWNLOAD_COMPLETE, "Cancelled");
            }

            // got error
            else if (!m_boolOperationSuccess)
            {
                UpdateCurrentStatus(TASKS.TASK_DOWNLOAD_COMPLETE, "Failed.");
            }

            // success
            else
            {
                m_boolOperationSuccess = true;
                UpdateCurrentStatus(TASKS.TASK_DOWNLOAD_COMPLETE, "TPK Download complete...");
            }

            App.AppLogger.LogMsg(Misc.Logger.enumProcessType.TILECACHER, Misc.Logger.enumLogType.GENERAL, "Download TPK complete");
        }

        public class DownloadTilePackageParameters
        {
            public Envelope m_Extent;
            public string m_strLocation;
            public string m_strFileName;
        }

        static public ObservableCollection<TilePackageRequest> TPKFiles
        {
            get { return m_TPKFiles; }
        }

        static public string TPKFileDestination
        {
            get { return m_strOutputFileDestination; }
            set { m_strOutputFileDestination = value; }
        }

        static public bool IsMX900DownloadLocation
        {
            get { return m_boolIsMX900Download; }
            set { m_boolIsMX900Download = value; }
        }
    }
}
