﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace N_POINT.MapTools
{
    public static class MercatorProjection
    {
        private static readonly double R_MAJOR = 6378137.0;
        private static readonly double R_MINOR = 6356752.3142;
        private static readonly double RATIO = R_MINOR / R_MAJOR;
        private static readonly double ECCENT = Math.Sqrt(1.0 - (RATIO * RATIO));
        private static readonly double COM = 0.5 * ECCENT;

        private static readonly double DEG2RAD = Math.PI / 180.0;
        private static readonly double RAD2Deg = 180.0 / Math.PI;
        private static readonly double PI_2 = Math.PI / 2.0;

        public static double[] toPixel(double lon, double lat)
        {
            return new double[] { lonToX(lon), latToY(lat) };
        }

        public static double[] toGeoCoord(double x, double y)
        {
            return new double[] { xToLon(x), yToLat(y) };
        }

        public static double lonToX(double lon)
        {
            return R_MAJOR * DegToRad(lon);
        }

        public static double latToY(double lat)
        {
            lat = Math.Min(89.5, Math.Max(lat, -89.5));
            double phi = DegToRad(lat);
            double sinphi = Math.Sin(phi);
            double con = ECCENT * sinphi;
            con = Math.Pow(((1.0 - con) / (1.0 + con)), COM);
            double ts = Math.Tan(0.5 * ((Math.PI * 0.5) - phi)) / con;
            return 0 - R_MAJOR * Math.Log(ts);
        }

        public static double xToLon(double x)
        {
            return RadToDeg(x) / R_MAJOR;
        }

        public static double yToLat(double y)
        {
            double ts = Math.Exp(-y / R_MAJOR);
            double phi = PI_2 - 2 * Math.Atan(ts);
            double dphi = 1.0;
            int i = 0;
            while ((Math.Abs(dphi) > 0.000000001) && (i < 15))
            {
                double con = ECCENT * Math.Sin(phi);
                dphi = PI_2 - 2 * Math.Atan(ts * Math.Pow((1.0 - con) / (1.0 + con), COM)) - phi;
                phi += dphi;
                i++;
            }
            return RadToDeg(phi);
        }

        private static double RadToDeg(double rad)
        {
            return rad * RAD2Deg;
        }

        private static double DegToRad(double deg)
        {
            return deg * DEG2RAD;
        }

        public static void ReverseMercatorConversion2(double dMercX, double dMercY, out double dLong, out double dLat)
        {
            dLong = (dMercX / 20037508.34) * 180;
            dLat = (dMercY / 20037508.34) * 180;

            dLat = 180 / Math.PI * (2 * Math.Atan(Math.Exp(dLat * Math.PI / 180)) - Math.PI / 2);
        }

        /// <summary>
        /// Converts a lat/long coordinate into a mercator projection. This function uses the more accurate formula
        /// of those that were tested
        /// </summary>
        /// <param name="dLong"></param>
        /// <param name="dLat"></param>
        /// <param name="dMercX"></param>
        /// <param name="dMercY"></param>
        public static void MercatorConversion2(double dLong, double dLat, out double dMercX, out double dMercY)
        {
            double RadiansPerDegree = Math.PI / 180;
            double Rad = dLat * RadiansPerDegree;
            double FSin = Math.Sin(Rad);

            dMercY = 6378137 / 2.0 * Math.Log((1.0 + FSin) / (1.0 - FSin));
            dMercX = dLong * 0.017453292519943 * 6378137;
        }
    }
}
