﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace N_POINT.MapTools
{
    class RegionAlias
    {
        List<NameToAliasMap> m_ListRegions;
        Dictionary<string, string> m_Aliases;
        public RegionAlias()
        {
            SetupAliases();

            SetupDictionary();
        }

        void SetupDictionary()
        {
            m_Aliases = new Dictionary<string, string>();

            m_Aliases.Add("AL", "Alabama");
            m_Aliases.Add("AK", "Alaska" );
            m_Aliases.Add("AZ", "Arizona");
            m_Aliases.Add("AR", "Arkansas");
            m_Aliases.Add("CA", "California");
            m_Aliases.Add("CO", "Colorado");
            m_Aliases.Add("CT", "Connecticut");
            m_Aliases.Add("DE", "Delaware");
            m_Aliases.Add("DC", "District of Columbia");
            m_Aliases.Add("FL", "Florida");
            m_Aliases.Add("GA", "Georgia");
            m_Aliases.Add("HI", "Hawaii");
            m_Aliases.Add("ID", "Idaho");
            m_Aliases.Add("IL", "Illinois");
            m_Aliases.Add("IN", "Indiana");
            m_Aliases.Add("IA", "Iowa");
            m_Aliases.Add("KS", "Kansas");
            m_Aliases.Add("KY", "Kentucky");
            m_Aliases.Add("LA", "Louisiana");
            m_Aliases.Add("ME", "Maine");
            m_Aliases.Add("MD", "Maryland");
            m_Aliases.Add("MA", "Massachusetts");
            m_Aliases.Add("MI", "Michigan");
            m_Aliases.Add("MN", "Minnesota");
            m_Aliases.Add("MS", "Mississippi");
            m_Aliases.Add("MO", "Missouri");
            m_Aliases.Add("MT", "Montana");
            m_Aliases.Add("NE", "Nebraska");
            m_Aliases.Add("NV", "Nevada");
            m_Aliases.Add("NH", "New Hampshire");
            m_Aliases.Add("NJ", "New Jersey");
            m_Aliases.Add("NM", "New Mexico");
            m_Aliases.Add("NY", "New York");
            m_Aliases.Add("NC", "North Carolina");
            m_Aliases.Add("ND", "North Dakota");
            m_Aliases.Add("OH", "Ohio");
            m_Aliases.Add("OK", "Oklahoma");
            m_Aliases.Add("OR", "Oregon");
            m_Aliases.Add("PA", "Pennsylvania");
            m_Aliases.Add("PR", "Puerto Rico");
            m_Aliases.Add("RI", "Rhode Island");
            m_Aliases.Add("SC", "South Carolina");
            m_Aliases.Add("SD", "South Dakota");
            m_Aliases.Add("TN", "Tennessee");
            m_Aliases.Add("TX", "Texas");
            m_Aliases.Add("UT", "Utah");
            m_Aliases.Add("VT", "Vermont");
            m_Aliases.Add("VA", "Virginia");
            m_Aliases.Add("WA", "Washington");
            m_Aliases.Add("WV", "West Virginia");
            m_Aliases.Add("WI", "Wisconsin");
            m_Aliases.Add("WY", "Wyoming");
        }

        public bool DoesAliasExist(string strItem)
        {
            return m_Aliases.ContainsKey(strItem);
        }

        void SetupAliases()
        {
            m_ListRegions = new List<NameToAliasMap>();
            AddItem("Alabama", "AL");
            AddItem("Alaska", "AK");
            AddItem("Arizona", "AZ");
            AddItem("Arkansas", "AR");
            AddItem("California", "CA");
            AddItem("Colorado", "CO");
            AddItem("Connecticut", "CT");
            AddItem("Delaware", "DE");
            AddItem("District of Columbia", "DC");
            AddItem("Florida", "FL");
            AddItem("Georgia", "GA");
            AddItem("Hawaii", "HI");
            AddItem("Idaho", "ID");
            AddItem("Illinois", "IL");
            AddItem("Indiana", "IN");
            AddItem("Iowa", "IA");
            AddItem("Kansas", "KS");
            AddItem("Kentucky", "KY");
            AddItem("Louisiana", "LA");
            AddItem("Maine", "ME");
            AddItem("Maryland", "MD");
            AddItem("Massachusetts", "MA");
            AddItem("Michigan", "MI");
            AddItem("Minnesota", "MN");
            AddItem("Mississippi", "MS");
            AddItem("Missouri", "MO");
            AddItem("Montana", "MT");
            AddItem("Nebraska", "NE");
            AddItem("Nevada", "NV");
            AddItem("New Hampshire", "NH");
            AddItem("New Jersey", "NJ");
            AddItem("New Mexico", "NM");
            AddItem("New York", "NY");
            AddItem("North Carolina", "NC");
            AddItem("North Dakota", "ND");
            AddItem("Ohio", "OH");
            AddItem("Oklahoma", "OK");
            AddItem("Oregon", "OR");
            AddItem("Pennsylvania", "PA");
            AddItem("Puerto Rico", "PR");
            AddItem("Rhode Island", "RI");
            AddItem("South Carolina", "SC");
            AddItem("South Dakota", "SD");
            AddItem("Tennessee", "TN");
            AddItem("Texas", "TX");
            AddItem("Utah", "UT");
            AddItem("Vermont", "VT");
            AddItem("Virginia", "VA");
            AddItem("Washington", "WA");
            AddItem("West Virginia", "WV");
            AddItem("Wisconsin", "WI");
            AddItem("Wyoming", "WY");
        }

        void AddItem(string a_strRegion, string a_strAlias)
        {
            NameToAliasMap l_item = new NameToAliasMap();
            l_item.RegionName = a_strRegion;
            l_item.RegionAlias = a_strAlias;
            m_ListRegions.Add(l_item);
        }
    }

    class NameToAliasMap
    {
        string m_strName;
        string m_strAlias;
        public NameToAliasMap()
        {
            m_strName = "";
            m_strAlias = "";
        }

        public string RegionName
        {
            get { return m_strName; }
            set { m_strName = value; }
        }

        public string RegionAlias
        {
            get { return m_strAlias; }
            set { m_strAlias = value; }
        }
    }
}
