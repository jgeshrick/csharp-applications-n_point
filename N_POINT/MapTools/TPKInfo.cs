﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace N_POINT.MapTools
{
    class TPKInfo
    {
        double m_XMin;
        double m_XMax;
        double m_YMin;
        double m_YMax;
        string m_strTPKFileName;

        public TPKInfo()
        {
            m_XMin = 0;
            m_XMax = 0;
            m_YMin = 0;
            m_YMax = 0;
        }

        public double XMin
        {
            get { return m_XMin; }
            set { m_XMin = value; }
        }

        public double XMax
        {
            get { return m_XMax; }
            set { m_XMax = value; }
        }

        public double YMin
        {
            get { return m_YMin; }
            set { m_YMin = value; }
        }

        public double YMax
        {
            get { return m_YMax; }
            set { m_YMax = value; }
        }

        public string TPKFileName
        {
            get { return m_strTPKFileName; }
            set { m_strTPKFileName = value; }
        }
    }
}
