﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Esri.ArcGISRuntime.Geometry;

namespace N_POINT.MapTools
{
    public class TilePackageRequest : ViewModelBase
    {
        // enum defining a the type of tiles a tile package contains
        public enum TILE_PACKAGE_TYPE
        {
            TPT_TILES,
            TPT_SATELLITE
        }

        public Geometry m_Extent;
        public string m_strLocation;
        public string m_strFileName;
        TILE_PACKAGE_TYPE m_tptype;

        public TilePackageRequest()
        {
        }

        public string TPKFileLocation
        {
            get { return m_strLocation; }
            set { m_strLocation = value; }
        }

        public string TPKFileName
        {
            get { return m_strFileName; }
            set
            {
                m_strFileName = value;
                OnPropertyChanged("TPKFileName");
            }
        }

        public Geometry TPKExtent
        {
            get { return m_Extent; }
            set { m_Extent = value; }
        }

        public TILE_PACKAGE_TYPE TilePackageType
        {
            get { return m_tptype; }
            set { m_tptype = value; }
        }
    }
}
