﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace N_POINT.MapTools
{
    public class TilePackageStatus
    {
        int m_nNumTiles;
        int m_nCacheSize;
        string m_strStatus;
        string m_strSubStatus;
        int m_nAreaSize;
        int m_nPercentComplete;
        MapTPKGenerator.TASKS m_taskStatus;


        public TilePackageStatus()
        {
            m_nNumTiles = 0;
            m_nCacheSize = 0;
            m_strStatus = "";
            m_nAreaSize = 0;
            m_nPercentComplete = 0;
            m_strSubStatus = "";
        }

        public void Init()
        {
            m_nNumTiles = 0;
            m_nCacheSize = 0;
            m_strStatus = "";
            m_strSubStatus = "";
            m_nPercentComplete = 0;
            m_nAreaSize = 0;
        }

        public void UpdateTilePackageStatus(TilePackageStatus a_tps)
        {
            if (a_tps.ProcessingSubStatus.Trim().Length > 0)
            {
                ProcessingStatus = a_tps.ProcessingStatus + a_tps.ProcessingSubStatus;
                App.AppLogger.LogMsg(Misc.Logger.enumProcessType.TILECACHER, Misc.Logger.enumLogType.DEBUG, string.Format("Displaying Status: {0}", ProcessingStatus));
            }
            else
            {
                ProcessingStatus = a_tps.ProcessingStatus;
            }
            
            /*
            if (a_tps.PercentComplete > 0)
            {
                ProcessingStatus = a_tps.ProcessingStatus + string.Format("( {0} )", a_tps.PercentComplete);
            }
            else
            {
                ProcessingStatus = a_tps.ProcessingStatus;
            }
            */
            NumberOfTiles = a_tps.NumberOfTiles;
            TilePackageFileSize = a_tps.TilePackageFileSize;
            TaskStatus = a_tps.TaskStatus;
            PercentComplete = a_tps.PercentComplete;
            ProcessingSubStatus = a_tps.ProcessingSubStatus;
        }

        public string ProcessingStatus
        {
            get
            {
                return m_strStatus;
            }

            set
            {
                m_strStatus = value;
            }
        }

        public string ProcessingSubStatus
        {
            get { return m_strSubStatus; }
            set { m_strSubStatus = value; }
        }

        public int NumberOfTiles
        {
            get
            {
                return m_nNumTiles;
            }
            set
            {
                m_nNumTiles = value;
            }
        }

        public int PercentComplete
        {
            get { return m_nPercentComplete; }
            set { m_nPercentComplete = value; }
        }

        public int TilePackageFileSize
        {
            get
            {
                return m_nCacheSize;
            }

            set
            {
                m_nCacheSize = value;
            }
        }

        public int TilePackageFileSizeMB
        {
            get
            {
                return (int)(m_nCacheSize / (1024 * 1024));
            }
        }

        public string TPKFileSizeMBText
        {
            get
            {
                string l_str = "";
                if (TilePackageFileSize > 0)
                {
                    l_str = string.Format("{0} MB", TilePackageFileSizeMB);
                }
                else
                {
                    l_str = "";
                }

                return l_str;
            }
        }


        public int AreaSize
        {
            get
            {
                return m_nAreaSize;
            }

            set
            {
                m_nAreaSize = value;
            }
        }

        public string AreaSizeText
        {
            get
            {
                string l_str = "";
                if (AreaSize > 0)
                {
                    l_str = string.Format("{0} Square Miles", AreaSize);
                }

                return l_str;
            }
        }

        public MapTPKGenerator.TASKS TaskStatus
        {
            get { return m_taskStatus; }
            set { m_taskStatus = value; }
        }
    }
}
