﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;


namespace N_POINT.Misc
{
    public class Authentication
    {
        const string m_strPartnerString = "15DCF6852AFC90875135DDFE273F4B59";
        string m_strCustomerNum;
        string m_strSiteID;
        string m_strCurrentToken;
        //string m_strFailureDescription;
        int m_nFailureCode;
        bool m_boolServiceAvailable = false;

        public delegate void AuthorizationValidEvent(bool a_boolValue);
        static public event AuthorizationValidEvent EventAuthorizationValid;

        public Authentication()
        {
            m_nFailureCode = 0;
            m_strCustomerNum = "";
            m_strSiteID = "";
            //m_strCurrentToken = "15DCF6852AFC90875135DDFE273F4B59";
            //BuildAuthenticationRequest("CUSTOMER_NUM", "SITE_ID");
            //<tns:rowset xmlns:tns="http://10.8.75.254:8080/FIXED_NETWORK"><tns:row><tns:string_x0028__x0027__x003C_authorize_x003E__x0027__x002C_lc_x005F_xml_out_x002C__x0027__x003C__x002F_authorize_x003E__x0027__x0029_><authorize><token>bc72b12c0dce205c</token></authorize></tns:string_x0028__x0027__x003C_authorize_x003E__x0027__x002C_lc_x005F_xml_out_x002C__x0027__x003C__x002F_authorize_x003E__x0027__x0029_></tns:row></tns:rowset>
            //<tns:rowset xmlns:tns="http://10.8.75.254:8080/FIXED_NETWORK"><tns:row><tns:string_x0028__x0027__x003C_authorize_x003E__x0027__x002C_lc_x005F_xml_out_x002C__x0027__x003C__x002F_authorize_x003E__x0027__x0029_><authorize><error code=”1000”>Invalid Partner String</error></authorize></tns:string_x0028__x0027__x003C_authorize_x003E__x0027__x002C_lc_x005F_xml_out_x002C__x0027__x003C__x002F_authorize_x003E__x0027__x0029_></tns:row></tns:rowset>
            //Test();
        }

        void Test()
        {
            //GetAuthenticationToken("15DCF6852AFC90875135DDFE273F4B59", "36084");
            //string strXML = "<tns:rowset xmlns:tns=\"http://10.8.75.254:8080/FIXED_NETWORK/"><tns:row><tns:string_x0028__x0027__x003C_authorize_x003E__x0027__x002C_lc_x005F_xml_out_x002C__x0027__x003C__x002F_authorize_x003E__x0027__x0029_><authorize><error code=”1000”>Invalid Partner String</error></authorize></tns:string_x0028__x0027__x003C_authorize_x003E__x0027__x002C_lc_x005F_xml_out_x002C__x0027__x003C__x002F_authorize_x003E__x0027__x0029_></tns:row></tns:rowset>";
            //string strResult;
            //int nResultCode;
            //ParseAuthenticationResponse(strXML, out strResult);
        }

        public string CustomerNumber
        {
            get
            {
                return m_strCustomerNum;
            }

            set
            {
                m_strCustomerNum = value;
            }
        }

        public string SiteID
        {
            get
            {
                return m_strSiteID;
            }

            set
            {
                m_strSiteID = value;
            }
        }

        /// <summary>
        /// The current authentication token
        /// </summary>
        public string AuthenticationToken
        {
            get
            {
                return m_strCurrentToken;
            }
        }

        /// <summary>
        /// Indicates whether or not the service is available
        /// </summary>
        public bool ServiceAvailable
        {
            get
            {
                return m_boolServiceAvailable;
            }
        }

        /// <summary>
        /// Builds an authentication request to be sent to the host system.
        /// </summary>
        /// <param name="a_strCustomerNum"></param>
        /// <param name="a_strSiteID"></param>
        /// <returns></returns>
        /// <remarks>
        /// Revision History
        /// Date     Who Defect# Description
        /// -------- --- ------- ----------------------------------------    
        /// 10/07/14 BJC         Initial Version
        /// </remarks>
        string BuildAuthenticationRequest(string a_strSiteID)
        {
            XmlDocument l_xml = new XmlDocument();
            XmlElement l_element;
            XmlText l_text;
            XmlElement l_group;

            //m_strCustomerNum = a_strCustomerNum;
            m_strSiteID = a_strSiteID;

            l_group = l_xml.CreateElement("authenticate");

            l_element = l_xml.CreateElement("PartnerString");
            l_text = l_xml.CreateTextNode(m_strPartnerString);
            l_element.AppendChild(l_text);
            l_group.AppendChild(l_element);

            l_element = l_xml.CreateElement("siteId");
            l_text = l_xml.CreateTextNode(a_strSiteID);
            l_element.AppendChild(l_text);
            l_group.AppendChild(l_element);

            l_xml.AppendChild(l_group);

            return l_xml.InnerXml;
        }



        /// <summary>
        /// Request a new token from the host using the current customer credentials.  This is called when a current token is in use
        /// but expires before the applicatin is restarted.  
        /// </summary>
        /// <returns>true if an authentication token is acquired</returns>
        /// <remarks>
        /// Revision History
        /// Date     Who Defect# Description
        /// -------- --- ------- ----------------------------------------    
        /// 10/07/14 BJC         Initial Version
        /// </remarks>
        public bool RequestNewToken()
        {
            return GetAuthenticationToken(m_strSiteID);
        }


        /// <summary>
        /// Requests an authentication token from the host system.
        /// </summary>
        /// <param name="a_strCustomerNum">A valid customer number</param>
        /// <param name="a_strSiteID">A valid site ID</param>
        /// <returns></returns>
        /// <remarks>
        /// Revision History
        /// Date     Who Defect# Description
        /// -------- --- ------- ----------------------------------------    
        /// 10/07/14 BJC         Initial Version
        /// </remarks>
        public bool GetAuthenticationToken(string a_strSiteID)
        {
            // if any subscribers to the event that informs of valid authorizations, trigger
            // the event now to force those subscribers to invalidate current data
            if (EventAuthorizationValid != null)
            {
                EventAuthorizationValid(false);
            }
            
            bool l_boolResult = true;
            int l_nCode;
            string l_strResponse;

            if (a_strSiteID.Trim().Length == 0)
            {
                App.AppLogger.LogMsg(Logger.enumProcessType.MISC, Logger.enumLogType.GENERAL, "Authentication: Site ID not set");
                return false;
            }

            // Assume the service is unavailable at start of check
            m_boolServiceAvailable = false;

            // generate an authentication request
            string l_strRequest = BuildAuthenticationRequest(a_strSiteID);

            int l_nRetry = HostCommunication.HostInterface.SERVICE_COMMAND_RETRY;
            string l_strErrorMsg = "";
            do
            {
                try
                {
                    // Send the request and receive the response
                    l_strResponse = App.NeptuneClient.authenticate(l_strRequest, out l_nCode);

                    if (ParseAuthenticationResponse(l_strResponse, out m_strCurrentToken))
                    {                        
                        m_boolServiceAvailable = true;
                        // break out of loop if we get a good response
                        break;
                    }
                    else
                    {
                        
                    }
                }
                catch (Exception ex)
                {
                    m_boolServiceAvailable = false;
                    l_boolResult = false;
                    App.AppLogger.LogMsg(Logger.enumProcessType.GEOCODER, 
                                         Logger.enumLogType.ERROR, 
                                         string.Format("Exception while attempting to get the authentication token: {0}", ex.Message));
                    l_strErrorMsg = ex.Message;
                }

                l_nRetry--;

                System.Threading.Thread.Sleep(1000);

            } while (l_nRetry > 0);

            if (!m_boolServiceAvailable)
            {
                string l_strMsg;
                l_strMsg = "Unable to authenticate with the host system";
                if (l_strErrorMsg.Length > 0)
                {
                    l_strMsg += string.Format(": {0}", l_strErrorMsg);
                }

                System.Windows.MessageBox.Show(l_strMsg);
            }

            if (EventAuthorizationValid != null)
            {
                EventAuthorizationValid(m_boolServiceAvailable);
            }


            return l_boolResult;
        }


        /// <summary>
        /// Requests an authentication token from the host based on the customer number and site ID. 
        /// The token is used to verify that any requests made are made by an authenticated application.
        /// </summary>
        /// <param name="a_strCustomerNum"></param>
        /// <param name="a_strSiteID"></param>
        /// <returns></returns>
        /// <remarks>
        /// Revision History
        /// Date     Who Defect# Description
        /// -------- --- ------- ----------------------------------------    
        /// 10/07/14 BJC         Initial Version
        /// </remarks>
        public async Task<bool> GetAuthenticationTokenAsync(string a_strSiteID)
        {
            // if any subscribers to the event that informs of valid authorizations, trigger
            // the event now to force those subscribers to invalidate current data
            if (EventAuthorizationValid != null)
            {
                EventAuthorizationValid(false);
            }

            bool l_boolResult = true;

            string l_strErrorMsg = "";

            string l_strResponse;
            m_boolServiceAvailable = false;

            // clear the current token before requesting a new one
            m_strCurrentToken = "";

            if (a_strSiteID.Trim().Length == 0)
            {
                App.AppLogger.LogMsg(Logger.enumProcessType.MISC, 
                                     Logger.enumLogType.GENERAL, 
                                     "Authentication: Site ID not set");
                return false;
            }
            else
            {
                // validate whether the Site ID value is a number
                int l_nTest;
                if (!int.TryParse(a_strSiteID, out l_nTest))
                {
                    System.Windows.MessageBox.Show("Site ID is not a numeric value");
                }
                else if (App.ConfigurationSettingsMgr.HostWebServiceURL.Trim().Length > 0)
                {
                    //
                    // Site ID is valid and customer number is set. Now build the request
                    //
                    string l_strRequest = BuildAuthenticationRequest(a_strSiteID);

                    try
                    {
                        // Send the request and receive the response
                        var resp = await App.NeptuneClient.authenticateAsync(l_strRequest);

                        l_strResponse = resp.Body.authenticateResult;

                        // If the authentication parameters were valid, the service will be available, otherwise,
                        // the service will not be available
                        m_boolServiceAvailable = ParseAuthenticationResponse(l_strResponse, out m_strCurrentToken);
                    }
                    catch (Exception ex)
                    {
                        l_boolResult = false;
                        App.AppLogger.LogMsg(Logger.enumProcessType.GEOCODER, 
                                             Logger.enumLogType.ERROR, 
                                             string.Format("Exception while attempting to get the authentication token: {0}", ex.Message));

                        //l_strErrorMsg = ex.Message; - don't display detailed message to user
                        l_strErrorMsg = "";
                    }

                    if (!m_boolServiceAvailable)
                    {
                        string l_strMsg;
                        l_strMsg = "Unable to authenticate with the host system";
                        if (l_strErrorMsg.Trim().Length > 0)
                        {
                            l_strMsg += string.Format(":  {0}", l_strErrorMsg);
                        }
                        else if (m_strCurrentToken.Length > 0)
                        {
                            l_strMsg += string.Format(":  {0}", m_strCurrentToken);
                        }

                        System.Windows.MessageBox.Show(l_strMsg);
                    }

                    if (EventAuthorizationValid != null)
                    {
                        EventAuthorizationValid(m_boolServiceAvailable);
                    }
                }
                else
                {
                    l_boolResult = false;
                }
            }

            return l_boolResult;
        }

        /// <summary>
        /// Parse the response from an authentication request.
        /// </summary>
        /// <param name="a_strXML">Response from host</param>
        /// <param name="a_strResult">The authentication token if successful, the error description if failure</param>
        /// <returns>true if successful, false otherwise</returns>
        /// <remarks>
        /// Revision History
        /// Date     Who Defect# Description
        /// -------- --- ------- ----------------------------------------    
        /// 10/07/14 BJC         Initial Version
        /// </remarks>
        public bool ParseAuthenticationResponse(string a_strXML, out string a_strResult)
        {
            XmlDocument l_xml = new XmlDocument();
            string l_strToken = "";
            a_strResult = ""; 

            bool l_boolResult = true;

            try
            {
                l_xml.LoadXml(a_strXML);
            }
            catch(Exception ex)
            {
                // XML response is not a valid XML document, but the response may still be valid (seen in testing)
                l_boolResult = false;
                App.AppLogger.LogMsg(Logger.enumProcessType.MISC, 
                                     Logger.enumLogType.WARNING, 
                                     string.Format("Error in ParseAuthenticationResponse. Unable to load XML document. Message: {0}", ex.Message));
            }

            if (l_boolResult)
            {
                XmlNodeList xl;

                // The document was valid so now we can try and extract the node that contains the token from the document
                xl = l_xml.GetElementsByTagName("token");

                if ((xl != null) && (xl.Count > 0))
                {
                    l_strToken = xl[0].InnerText;
                    a_strResult = l_strToken;
                }
                else
                {
                    // set to false - no valid token received
                    l_boolResult = false;

                    // look for the error node in the response to get the reason for the failure
                    XmlNodeList xlError;
                    xlError = l_xml.GetElementsByTagName("error");
                    if ( (xlError != null) && (xlError.Count > 0) )
                    {
                        a_strResult = xlError[0].InnerText;
                        App.AppLogger.LogMsg(Logger.enumProcessType.MISC, Logger.enumLogType.ERROR, string.Format("Unable to authenticate with the host service: {0}", a_strResult));
                    }
                    else
                    {                        
                        // could not find error node. Just log a generic failure message
                        a_strResult = "Host service response did not contain a token.";
                        App.AppLogger.LogMsg(Logger.enumProcessType.MISC, Logger.enumLogType.GENERAL, "Authentication response did not contain a token");
                        App.AppLogger.LogMsg(Logger.enumProcessType.MISC, Logger.enumLogType.DEBUG, string.Format("Response: {0}", a_strXML));
                    }
                }
            }
            else
            {
                //
                // The document was invalid so we need to try to manually find the error code and description 
                // from the response string
                //
                int l_nIndex;
                bool l_boolParseFailure = false;
                int l_nLength;
                string l_strCode;
                string l_strDescription;
                l_nIndex = a_strXML.IndexOf("error code");
                if (l_nIndex >= 0)
                {
                    // Add the length of the string 'error code='
                    l_nIndex += ("error code=").Length;

                    // Advance past the 'close quote'
                    l_nIndex++;

                    // subtract 1 to exclude the ending 'close quote'
                    l_nLength = a_strXML.IndexOf(">", l_nIndex) - 1;

                    if (l_nLength > 0)
                    {
                        l_nLength = l_nLength - l_nIndex;
                        // Advance the length of the code + the 2 'close quote' characters
                        l_nIndex += l_nLength + 2;

                        // Extract the numeric code
                        l_strCode = a_strXML.Substring(l_nIndex, l_nLength);

                        int.TryParse(l_strCode, out m_nFailureCode);
                    }
                    else
                    {
                        l_boolParseFailure = true;
                    }

                    l_nLength = a_strXML.IndexOf("</error>", l_nIndex);
                    if (l_nLength > 0)
                    {
                        l_nLength = l_nLength - l_nIndex;

                        // Extract the description
                        l_strDescription = a_strXML.Substring(l_nIndex, l_nLength);
                        a_strResult = l_strDescription;
                    }
                    else
                    {
                        l_boolParseFailure = true;
                    }
                }
                else
                {
                    l_boolParseFailure = true;
                }

                if (l_boolParseFailure)
                {
                    App.AppLogger.LogMsg(Logger.enumProcessType.MISC, 
                                         Logger.enumLogType.ERROR, 
                                         string.Format("Authentication Failure: Error parsing string: {0}", a_strXML));
                }
            }

            return l_boolResult;
        }
        //function to call the web service
        //function to get token age

    }
}
