﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Cryptography;
using System.Configuration;
using Esri.ArcGISRuntime.Security;

namespace N_POINT.Misc
{
    public class ConfigSettingsManager : ViewModelBase
    {
        private const int DEFAULT_MIN_GEOCODING_HIT = 85;
        private const int SECONDS_IN_A_DAY = 86400;

        string m_strCustomerNumber;
        string m_strOptionKey;
        string m_strDetailedMapKey;
        string m_strSiteID;
        string m_strMappingLicense;
        string m_strGeocodingLicense;
        string m_strHostWebServiceURL;
        string m_strEncryptedSAMKH;
        string m_strDMDenialReason;
        string m_strMapRootName;
        string m_strLastMapExtent;
        DateTime m_dtDetailMapTimeStamp;
        int m_nMinGeocodingHit;
        int m_nLifeTimeGeocodeCount;
        int m_nLastBatchGeocodeSessionCount;
        bool m_boolDetailedMobileMapping = false;
        bool m_boolAllowBatchGeocode;
        bool m_boolAuthorizationParamsChanged = false;
        bool m_boolConnectToHost = true;
        
        //string m_strEsriUID;
        //string m_strEsriPWD;

        byte[] m_pseudoRandomBytes;
        string m_strPWD;

        public ConfigSettingsManager()
        {
            m_strCustomerNumber = "";
            m_strMappingLicense = "";
            m_strGeocodingLicense = "";
            m_strSiteID = "";
            m_strHostWebServiceURL = "";
            m_strDetailedMapKey = "";
            m_strEncryptedSAMKH = "";
            m_strDMDenialReason = "";
            m_strMapRootName = "";
            m_boolAllowBatchGeocode = false;
            //m_strEsriUID = DEFAULT_ESRI_UID;
            //m_strEsriPWD = DEFAULT_ESRI_PWD;

            m_dtDetailMapTimeStamp = DateTime.MinValue;

            m_nMinGeocodingHit = DEFAULT_MIN_GEOCODING_HIT;
            LoadSettings();
        }

        ~ConfigSettingsManager()
        {
            SaveSettings();

            //TestOnExit();
        }

        public bool SaveSettings()
        {
            Properties.Settings.Default.CustomerNumber = CustomerNumber;
            Properties.Settings.Default.GeocodingLicense = GeocodingLicense;
            Properties.Settings.Default.MappingLicense = MappingLicense;
            Properties.Settings.Default.SiteID = SiteID;
            Properties.Settings.Default.HostWebService = HostWebServiceURL;
            Properties.Settings.Default.MinimumGeocodeHit = m_nMinGeocodingHit;
            Properties.Settings.Default.OptionKey = m_strOptionKey;
            Properties.Settings.Default.AllowBatchGeocodingOfPrev = AllowBatchGeocodeOfPrevItems;
            Properties.Settings.Default.DetailedMapKey = DetailedMapKey;
            Properties.Settings.Default.MapRootName = MapRootName;
            Properties.Settings.Default.LastMapExtent = LastMapExtent;
            Properties.Settings.Default.LifeTimeBatchGeocodes = LifeTimeGeocodeCount;
            Properties.Settings.Default.LastBatchGeocodeSessionCount = LastBatchGeocodeSessionCount;

            RemoveExpiredDetailedMapOptionKeys();
            string strTest = m_strEncryptedSAMKH;
            Properties.Settings.Default.SAMKH = m_strEncryptedSAMKH;

            Properties.Settings.Default.Save();
            return true;
        }



        /// <summary>
        /// Loads the application settings from the application configuration file
        /// </summary>
        /// <remarks>
        /// Revision History
        /// Date     Who Defect# Description
        /// -------- --- ------- ----------------------------------------    
        /// 12/07/14 BJC         Initial Version
        /// </remarks>
        public void LoadSettings()
        {
            CustomerNumber = Properties.Settings.Default.CustomerNumber;
            GeocodingLicense = Properties.Settings.Default.GeocodingLicense;
            MappingLicense = Properties.Settings.Default.MappingLicense;         
            SiteID = Properties.Settings.Default.SiteID;
            HostWebServiceURL = Properties.Settings.Default.HostWebService;
            MinimumGeocodingHit = Properties.Settings.Default.MinimumGeocodeHit;
            AllowBatchGeocodeOfPrevItems = Properties.Settings.Default.AllowBatchGeocodingOfPrev;
            DetailedMapKey = Properties.Settings.Default.DetailedMapKey;
            MapRootName = Properties.Settings.Default.MapRootName;
            LastMapExtent = Properties.Settings.Default.LastMapExtent;
            m_strOptionKey = Properties.Settings.Default.OptionKey;
            m_strEncryptedSAMKH = Properties.Settings.Default.SAMKH;

            LastBatchGeocodeSessionCount = Properties.Settings.Default.LastBatchGeocodeSessionCount;
            LifeTimeGeocodeCount = Properties.Settings.Default.LifeTimeBatchGeocodes;

            RemoveExpiredDetailedMapOptionKeys();
            try
            {
                NSP_HostHelpers.NSC_HostHelpers.StandaloneMapOptionKey l_standaloneOptions = new NSP_HostHelpers.NSC_HostHelpers.StandaloneMapOptionKey();
                NSP_HostHelpers.NSC_HostHelpers.ParseStandaloneMapOptionKey(DetailedMapKey, CustomerNumber, ref l_standaloneOptions);
                m_dtDetailMapTimeStamp = l_standaloneOptions.m_timestamp;
                m_boolDetailedMobileMapping = l_standaloneOptions.m_allowDetailedMapGeneration;

            }
            catch (Exception)
            {
            }
        }

        public static byte[] ConvertHexStringToByteArray(string hex)
        {
            return Enumerable.Range(0, hex.Length)
                                .Where(x => x % 2 == 0)
                                .Select(x => Convert.ToByte(hex.Substring(x, 2), 16))
                                .ToArray();
        }

        //clearText -> the string to be encrypted
        //passowrd -> encryption key
        public string EncryptString(string clearText, string Password)
        {
            // First we need to turn the input string into a byte array. 
            // NOTE: *MUST* use unicode encoding instead of default 
            byte[] clearBytes = Encoding.Unicode.GetBytes(clearText);

            // Then, we need to turn the password into Key and IV 
            // We are using salt to make it harder to guess our key
            // using a dictionary attack - 
            var pdb = new PasswordDeriveBytes(Password, m_pseudoRandomBytes);

            // Now get the key/IV and do the encryption using the
            // function that accepts byte arrays. 
            byte[] encryptedData = EncryptBytes(clearBytes, pdb.GetBytes(16), pdb.GetBytes(16));

            // Now we need to turn the resulting byte array into a string. 
            // NOTE: *MUST* use the base64 string function
            return Convert.ToBase64String(encryptedData);
        }

        //cipherText -> the string to be decrypted
        //passowrd -> decryption key
        public string DecryptString(string cipherText, string Password)
        {
            // First we need to turn the input string into a byte array. 
            // We presume that Base64 encoding was used 
            byte[] cipherBytes = Convert.FromBase64String(cipherText);

            // Then, we need to turn the password into Key and IV 
            // We are using salt to make it harder to guess our key
            // using a dictionary attack
            var pdb = new PasswordDeriveBytes(Password, m_pseudoRandomBytes);

            byte[] decryptedData = DecryptBytes(cipherBytes, pdb.GetBytes(16), pdb.GetBytes(16));

            // Now we need to turn the resulting byte array into a string. 
            // NOTE: *MUST* be a unicode string
            return Encoding.Unicode.GetString(decryptedData);
        }

        // Encrypt a byte array into a byte array using a key and an IV 
        byte[] EncryptBytes(byte[] a_encryptedData, byte[] a_key, byte[] a_IV)
        {
            // Create a MemoryStream to accept the encrypted bytes 
            var ms = new MemoryStream();

            Rijndael alg = Rijndael.Create();

            alg.Key = a_key;
            alg.IV = a_IV;
            alg.Padding = PaddingMode.PKCS7;

            var cs = new CryptoStream(ms, alg.CreateEncryptor(), CryptoStreamMode.Write);

            // Write the data and make it do the encryption 
            cs.Write(a_encryptedData, 0, a_encryptedData.Length);

            cs.FlushFinalBlock();

            byte[] encryptedData = ms.ToArray();

            return encryptedData;
        }


        private byte[] DecryptBytes(byte[] a_encryptedData, byte[] a_key, byte[] a_IV)
        {
            // receives the decrypted bytes
            var ms = new MemoryStream();

            Rijndael alg = Rijndael.Create();

            alg.Key = a_key;
            alg.IV = a_IV;
            alg.Padding = PaddingMode.PKCS7;

            var cs = new CryptoStream(ms,  alg.CreateDecryptor(), CryptoStreamMode.Write);

            cs.Write(a_encryptedData, 0, a_encryptedData.Length);

            cs.FlushFinalBlock();

            byte[] decryptedData = ms.ToArray();

            return decryptedData;


        }

        /************************************************************************************/

        string EncryptString(string a_strData)
        {            
            string l_strEncryptedValue = "";// = BitConverter.ToString(l_encryptedData).Replace("-", "");

            try
            {
                if (a_strData.Length > 0)
                {
                    l_strEncryptedValue = EncryptString(a_strData, m_strPWD);
                }
            }
            catch (Exception)
            {
            }
            return l_strEncryptedValue;
        }

        string DecryptString(string a_strEncryptedValue)
        {
            string l_strDecrypted;

            l_strDecrypted = "";
            try
            {
                if (a_strEncryptedValue.Length > 0)
                {
                    l_strDecrypted = DecryptString(a_strEncryptedValue, m_strPWD);
                }
            }
            catch (Exception)
            {
            }
            return l_strDecrypted;       
        
        }


        /// <summary>
        /// Removes an expired key from the list of used keys.  
        /// </summary>
        /// <remarks>
        /// Revision History
        /// Date     Who Defect# Description
        /// -------- --- ------- ----------------------------------------    
        /// 12/07/14 BJC         Initial Version
        /// </remarks>
        private void RemoveExpiredDetailedMapOptionKeys()
        {
            if (SAMKH.Trim().Length > 0)
            {
                string l_strTest = "";

                // break the string up into individual optionkey values
                string[] collUsedKeys = SAMKH.Split(new char[] { ';' });
                foreach (string str in collUsedKeys)
                {
                    if (str.Length > 0)
                    {
                        if (!HasDetailMapOptionKeyExpired(str))
                        {
                            l_strTest += str + ";";
                        }
                    }
                }
                
                // save the keys that are still valid, if any.
                SAMKH = l_strTest;
            }
        }

        /// <summary>
        /// Determines whether or not a particular detailed map option key has been previously used
        /// </summary>
        /// <param name="a_strKey"></param>
        /// <returns></returns>
        /// <remarks>
        /// Revision History
        /// Date     Who Defect# Description
        /// -------- --- ------- ----------------------------------------    
        /// 12/07/14 BJC         Initial Version
        /// </remarks>
        private bool HasDetailedMapOptionKeyBeenUsed(string a_strKey)
        {
            bool l_boolResult = false;
            
            if (a_strKey.Trim().Length > 0)
            {
                string[] collUsedKeys = SAMKH.Split(new char[] { ';' });
                if (collUsedKeys.Contains(a_strKey))
                {
                    l_boolResult = true;
                }
            }

            return l_boolResult;
        }

        /// <summary>
        /// Determines if a given key has expired.  An expired key is a key that is older than 24 hours
        /// or 86,400 seconds.
        /// </summary>
        /// <param name="a_strKey"></param>
        /// <returns></returns>
        /// <remarks>
        /// Revision History
        /// Date     Who Defect# Description
        /// -------- --- ------- ----------------------------------------    
        /// 12/07/14 BJC         Initial Version
        /// </remarks>
        bool HasDetailMapOptionKeyExpired(string a_strKey)
        {
            bool l_boolResult = true;
            try
            {
                NSP_HostHelpers.NSC_HostHelpers.StandaloneMapOptionKey l_standaloneOptions = new NSP_HostHelpers.NSC_HostHelpers.StandaloneMapOptionKey();
                NSP_HostHelpers.NSC_HostHelpers.ParseStandaloneMapOptionKey(a_strKey, CustomerNumber, ref l_standaloneOptions);

                ///////////////////////////////////////////////////////////
                // GMT/UTC time test
                DateTime l_UTC = DateTime.Now.ToUniversalTime();
                DateTime l_LocalEpoch = new DateTime(1970, 1, 1, 0, 0, 0);
                TimeSpan tsTest;
                long ltotsec;
                tsTest = l_UTC - l_LocalEpoch;
                ltotsec = (long)tsTest.TotalSeconds;
                ///////////////////////////////////////////////////////////

                DateTime l_dtNow = DateTime.Now;
                TimeSpan l_tspan = l_dtNow - l_standaloneOptions.m_timestamp;

                if (l_tspan.TotalSeconds < SECONDS_IN_A_DAY)
                {
                    l_boolResult = false;
                }
            }
            catch (Exception ex)
            {
                App.AppLogger.LogMsg(Logger.enumProcessType.MISC, 
                                     Logger.enumLogType.ERROR, 
                                     string.Format("Failure to determine if the detailed map option key has expired. Exception: {0}", ex.Message));
            }

            //l_boolResult = false; -- DEBUG
            return l_boolResult;
        }

        /// <summary>
        /// Determine if the key is a valid key
        /// </summary>
        /// <returns></returns>
        /// <remarks>
        /// Revision History
        /// Date     Who Defect# Description
        /// -------- --- ------- ----------------------------------------    
        /// 12/07/14 BJC         Initial Version
        /// </remarks>
        public bool IsDetailedMapOptionKeyValid()
        {
            bool l_boolResult = false;
            // initially indicate the option is not enabled
            m_boolDetailedMobileMapping = false;
            try
            {
                NSP_HostHelpers.NSC_HostHelpers.StandaloneMapOptionKey l_standaloneOptions = new NSP_HostHelpers.NSC_HostHelpers.StandaloneMapOptionKey();

                if (NSP_HostHelpers.NSC_HostHelpers.ParseStandaloneMapOptionKey(DetailedMapKey, CustomerNumber, ref l_standaloneOptions))
                {
                    l_boolResult = true;

                    // capture the setting in a member variable
                    m_boolDetailedMobileMapping = l_standaloneOptions.m_allowDetailedMapGeneration;
                }
            }
            catch (Exception ex)
            {
                App.AppLogger.LogMsg(Logger.enumProcessType.MISC,
                                     Logger.enumLogType.ERROR,
                                     string.Format("Unable to process detailed map option key. Exception: {0}", ex.Message));
            }

            return l_boolResult;
        }


        /// <summary>
        /// Determines if the detailed map key is valid and if so, whether or not the option is enabled.
        /// Check to see if the key:
        /// 1) Is valid for use
        /// 2) Has been previously used
        /// 3) Is expired
        /// </summary>
        /// <returns>true if the key is valid and can be used, false otherwise</returns>
        /// <remarks>
        /// Revision History
        /// Date     Who Defect# Description
        /// -------- --- ------- ----------------------------------------    
        /// 12/07/14 BJC         Initial Version
        /// </remarks>
        public bool IsUseDetailedMapOptionAllowed()
        {
            // assume the key can be used
            bool l_boolResult = true;

            // initialize the denial reason to an empty string
            m_strDMDenialReason = "";

            if (DetailedMapKey.Trim().Length > 0)
            {
                // is the key valid?
                if (!IsDetailedMapOptionKeyValid())
                {
                    m_strDMDenialReason = "The key is invalid";
                    l_boolResult = false;
                }

                // has it been used before?
                if (l_boolResult && HasDetailedMapOptionKeyBeenUsed(DetailedMapKey))
                {
                    m_strDMDenialReason = "The key has been previously used.";
                    l_boolResult = false;
                }

                // has it expired?
                if (l_boolResult && HasDetailMapOptionKeyExpired(DetailedMapKey))
                {
                    m_strDMDenialReason = "The key has expired";
                    l_boolResult = false;
                }

                // if everything checks out up to here, check the actual flag value in the option
                if (l_boolResult)
                {
                    l_boolResult = m_boolDetailedMobileMapping;

                    if (!m_boolDetailedMobileMapping)
                    {
                        m_strDMDenialReason = "The option is disabled.";
                    }
                }
            }
            else
            {
                // key isn't set
                l_boolResult = false;
                m_strDMDenialReason = "The detailed map key is not set";
            }

            return l_boolResult;
        }

        public string SAMKH
        {
            get
            {
                string strValue = m_strEncryptedSAMKH;
                if (m_strEncryptedSAMKH.Length >= 2)
                {
                    if (m_strEncryptedSAMKH.Substring(0, 2) == "E:")
                    {
                        strValue = m_strEncryptedSAMKH.Remove(0, 2);
                        strValue = DecryptString(strValue);
                    }
                }
                return strValue;
            }
            set
            {
                string l_strValue = value;
                m_strEncryptedSAMKH = "";

                if (l_strValue.Length > 0)
                {
                    // keep the used keys encrypted.
                    m_strEncryptedSAMKH = string.Format("E:{0}", EncryptString(l_strValue));
                }
            }
        }

        /// <summary>
        /// Returns the reason the detailed map key was rejected
        /// </summary>
        /// <remarks>
        /// Revision History
        /// Date     Who Defect# Description
        /// -------- --- ------- ----------------------------------------    
        /// 12/07/14 BJC         Initial Version
        /// </remarks>
        public string StandAloneDetailedMapDenialReason
        {
            get
            {
                return m_strDMDenialReason;
            }
        }

        /// <summary>
        /// Adds the detailed map key to a list of used keys.  
        /// </summary>
        /// <remarks>
        /// Revision History
        /// Date     Who Defect# Description
        /// -------- --- ------- ----------------------------------------    
        /// 12/07/14 BJC         Initial Version
        /// </remarks>
        public void ConsumeCurrentDetailedMapKey()
        {
            string strValue;
            strValue = SAMKH;
            if (!strValue.Contains(DetailedMapKey))
            {
                strValue += DetailedMapKey + ";";
                SAMKH = strValue;
            }

            //m_strSAMKH += 

            //Properties.Settings.Default.SAMKH = m_strSAMKH;
            //Properties.Settings.Default.Save();
            
        }

        /// <summary>
        /// Resets the authorization changed and host connection flags
        /// </summary>
        /// <remarks>
        /// Revision History
        /// Date     Who Defect# Description
        /// -------- --- ------- ----------------------------------------    
        /// 12/07/14 BJC         Initial Version
        /// </remarks>
        public void ResetFlags()
        {
            m_boolAuthorizationParamsChanged = false;
            m_boolConnectToHost = false;
        }

        public bool AuthorizationParametersChanged
        {
            get
            {
                return m_boolAuthorizationParamsChanged;
            }
        }

        public bool ConnectToHostRequired
        {
            get
            {
                return m_boolConnectToHost;
            }
        }

        public string CountryCode
        {
            get { return Properties.Settings.Default.CountryCode; }
            set 
            { 
                Properties.Settings.Default.CountryCode = value;
                Properties.Settings.Default.Save();
            }
        }

        public string DefaultCity
        {
            get { return Properties.Settings.Default.DefaultCity; }
            set 
            {
                Properties.Settings.Default.DefaultCity = value;
                Properties.Settings.Default.Save();
            }
        }

        public string DefaultState
        {
            get { return Properties.Settings.Default.DefaultState; }
            set 
            { 
                Properties.Settings.Default.DefaultState = value;
                Properties.Settings.Default.Save();
            }
        }

        public string CustomerNumber
        {
            get
            {
                return m_strCustomerNumber;
            }

            set
            {
                m_strCustomerNumber = value;
                m_boolAuthorizationParamsChanged = true;
                OnPropertyChanged("CustomerNumber");
            }
        }


        public string OptionKey
        {
            get
            {
                return m_strOptionKey;
            }

            set
            {
                m_strOptionKey = value;
                OnPropertyChanged("OptionKey");
            }
        }

        public string DetailedMapKey
        {
            get 
            { 
                return m_strDetailedMapKey; 
            }
            set 
            { 
                m_strDetailedMapKey = value;
                OnPropertyChanged("DetailedMapKey");
            }
        }

        public string MappingLicense
        {
            get
            {
                return m_strMappingLicense;
            }

            set
            {
                m_strMappingLicense = value;
                OnPropertyChanged("MappingLicense");
            }
        }

        public string GeocodingLicense
        {
            get
            {
                return m_strGeocodingLicense;
            }

            set
            {
                m_strGeocodingLicense = value;
                OnPropertyChanged("GeocodingLicense");
            }
        }


        public bool DetailedMobileMapping
        {
            get 
            {
                return IsUseDetailedMapOptionAllowed();
            }

        }
        
        public bool AllowBatchGeocodeOfPrevItems
        {
            get { return m_boolAllowBatchGeocode; }
            set 
            { 
                m_boolAllowBatchGeocode = value;
                OnPropertyChanged("AllowBatchGeocodeOfPrevItems");
            }
        }

        public string SiteID
        {
            get
            {
                return m_strSiteID;
            }

            set
            {
                m_strSiteID = value;
                m_boolAuthorizationParamsChanged = true;
                OnPropertyChanged("SiteID");
            }
        }

        public string HostWebServiceURL
        {
            get { return m_strHostWebServiceURL; }
            set 
            { 
                m_strHostWebServiceURL = value;

                // Host service URL changed. Require re-connect and re-authorization
                m_boolAuthorizationParamsChanged = true;
                m_boolConnectToHost = true;
                OnPropertyChanged("HostWebServiceURL");
            }
        }

        public bool ForceDefaultCityState
        {
            get { return Properties.Settings.Default.ForceDefaultCityState; }
            set { Properties.Settings.Default.ForceDefaultCityState = value; }
        }

        public int MinimumGeocodingHit
        {
            get
            {
                return m_nMinGeocodingHit;
            }

            set
            {
                m_nMinGeocodingHit = value;
            }
        }

        public int LifeTimeGeocodeCount
        {
            get { return m_nLifeTimeGeocodeCount; }
            set { m_nLifeTimeGeocodeCount = value; }
        }

        public int LastBatchGeocodeSessionCount
        {
            get { return m_nLastBatchGeocodeSessionCount; }
            set { m_nLastBatchGeocodeSessionCount = value; }
        }
        
        public string MapRootName
        {
            get { return m_strMapRootName; }
            set { m_strMapRootName = value; }
        }

        public string LastMapExtent
        {
            get { return m_strLastMapExtent; }
            set { m_strLastMapExtent = value; }
        }
    }
}
