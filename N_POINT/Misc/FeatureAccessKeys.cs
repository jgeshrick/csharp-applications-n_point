﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace N_POINT.Misc
{
    class FeatureAccessKeys
    {
        Dictionary<int, string> m_FeatureKeys;
        Dictionary<int, string> m_UsedKeys;
        Random m_rndGenerator;
        const int MAX_KEYS_SEQUENCE = 100;
        const int TOTAL_SECONDS_ONE_DAY = 86400;

        int m_nCurrentKey = 0;

        public FeatureAccessKeys()
        {
            m_FeatureKeys = new Dictionary<int, string>();
            m_UsedKeys = new Dictionary<int, string>();

            //GenerateKeys();
            m_nCurrentKey = 0;
        }


        //function to call the web service
        //function to get token age

        void ByteArrayToHexString(byte[] a_bytes, out string strHex)
        {
            strHex = BitConverter.ToString(a_bytes).Replace("-", string.Empty);
        }

        public void GetKeyFromValue(string strValue, out int a_nValue, out string strKey)
        {
            int nValue;

            int.TryParse(strValue, out nValue);
            Random l_rnd = new Random(nValue);
            byte[] byteData = new byte[4];
            l_rnd.NextBytes(byteData);
            ByteArrayToHexString(byteData, out strKey);
            a_nValue = nValue;
        }

        int GetCurrentUTCSeconds()
        {
            DateTime l_dt;
            DateTime l_dtCurrent;
            l_dt = new DateTime(1970, 1, 1, 0, 0, 0).ToUniversalTime();

            // l_dt is created to indicate what UTC time is from the current time zone. Subtract the
            // hours and minutes components to get the true UTC time value.
            l_dt = l_dt.AddHours(-l_dt.Hour);
            l_dt = l_dt.AddMinutes(-l_dt.Minute);

            // get the current UTC time
            l_dtCurrent = DateTime.Now.ToUniversalTime();

            // Calculate the number seconds since UTC began
            TimeSpan l_ts;
            l_ts = l_dtCurrent - l_dt;
            int nSeconds = (int)l_ts.TotalSeconds;

            return nSeconds;
        }

        /// <summary>
        /// Generates a list of keys to issue to a customer to grant access to a feature in the application.
        /// These keys are intended to be single use and valid for a 24 hour period of time.  The application
        /// restricting access needs to guarantee that the key will only be used once.  
        /// </summary>
        public void GenerateKeys()
        {
            int count;
            string l_strKey;

            // first determine how many seconds since UTC began
            int nSeconds = GetCurrentUTCSeconds();

            // seed the random number generator with the UTC seconds value
            m_rndGenerator = new Random(nSeconds);

            m_FeatureKeys.Clear();

            // declare a 4 byte array to hold the 4 bytes for a key
            byte[] l_byteData = new byte[4];

            // generate a sequence of 100 keys for the the current UTC second
            for (count = 0; count < MAX_KEYS_SEQUENCE; count++)
            {
                m_rndGenerator.NextBytes(l_byteData);

                // convert bytes to a hex string
                ByteArrayToHexString(l_byteData, out l_strKey);

                m_FeatureKeys.Add(count, l_strKey);
            }
        }

        public string GetNextKey()
        {
            var item = m_FeatureKeys.ElementAt(m_nCurrentKey);
            m_nCurrentKey++;
            if (m_nCurrentKey >= m_FeatureKeys.Count)
            {
                // if for any reason we exceed the number of generated keys, generate 
                // a new set of keys based off of the current time
                GenerateKeys();

                m_nCurrentKey = 0;
                item = m_FeatureKeys.ElementAt(m_nCurrentKey);
            }
            else if (m_nCurrentKey <= 0)
            {
                m_nCurrentKey = m_FeatureKeys.Count - 1;
            }

            return item.Value;
        }


        public bool ValidateKeyValue(string strKeyValue)
        {
            int nSeconds;
            int nLowerBound;
            nSeconds = GetCurrentUTCSeconds();
            // set the lower bound for the test to be one day from the current time
            nLowerBound = nSeconds - TOTAL_SECONDS_ONE_DAY;
            bool l_boolFound = false;

            int count;
            byte[] l_byteData = new byte[4];
            string l_strKey;
            do
            {
                m_rndGenerator = new Random(nSeconds);
                for (count = 0; count < MAX_KEYS_SEQUENCE; count++)
                {
                    m_rndGenerator.NextBytes(l_byteData);
                    ByteArrayToHexString(l_byteData, out l_strKey);
                    if (l_strKey == strKeyValue)
                    {
                        l_boolFound = true;
                        break;
                    }
                }
                nSeconds--;
            } while ((nSeconds >= nLowerBound) && !l_boolFound);

            return l_boolFound;
            /*
            bool l_boolResult = false;
            if (m_FeatureKeys.ContainsValue(strKeyValue))
            {
                l_boolResult = true;
                foreach (KeyValuePair<int, string> entry in m_FeatureKeys)
                {
                    if (entry.Value == strKeyValue)
                    {
                        // find the entry we just validated and add it to the used keys list
                        m_UsedKeys.Add(entry.Key, entry.Value);
                        break;
                    }
                }
            }

            return l_boolResult;
            */
        }

        void ManageKeys()
        {

        }

        bool SaveUsedKeys()
        {
            bool l_boolResult = true;
            int nItems;
            nItems = m_UsedKeys.Count;
            // 12 bytes per key pair
            nItems = nItems * 12;

            return l_boolResult;
        }

        //load the bytes for the keys from the key file if it exists
        bool LoadUsedKeys()
        {
            bool l_boolResult = true;
            string strFileName = "";

            //strFileName = string.Format("{0}\\MMKF.dat", Utility.GetApplicationDataFolder());
            if (System.IO.File.Exists(strFileName))
            {
                try
                {
                    //read the file into an array of bytes

                    //decrypt the array of bytes

                    //extract the keys mappings from the decrypted array
                }
                catch (Exception)
                {
                }
            }

            return l_boolResult;
        }
        // decrypt the file into individual keys
    }
}
