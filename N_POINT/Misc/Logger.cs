﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Diagnostics;

namespace N_POINT.Misc
{
    public class Logger
    {
        object m_logLock;
        string m_strFileName;
        string m_strLoggerError;
        string m_strLogFolder;
        const string LOG_FILE_NAME = "N_POINT.log";
        const string LOG_NAME_WITHOUT_EXTENSION = "N_POINT";
        DateTime m_dtLastLogCheck = DateTime.MinValue;
        long MAX_LOG_FILE_SIZE = (long)Math.Pow(2, 20) * 25; // 25 megabytes

        public enum enumLogType
        {
            ERROR,
            WARNING,
            GENERAL,
            DEBUG
        }

        public enum enumProcessType
        {
            GEOCODER,
            TILECACHER,
            MISC
        }

        public Logger()
        {
            Misc.Utility.VerifyApplicationLogFolder();
            m_logLock = new object();
            m_strLogFolder = Utility.GetApplicationLogFolder();

            m_strFileName = string.Format("{0}\\{1}", m_strLogFolder, LOG_FILE_NAME);
            
            //SelfTest();


        }

        public string TranslateLogType(enumLogType a_type)
        {
            string l_strRet = "";
            switch (a_type)
            {
                case enumLogType.ERROR:
                    l_strRet = "ERROR";
                    break;
                case enumLogType.WARNING:
                    l_strRet = "WARNING";
                    break;
                case enumLogType.GENERAL:
                    l_strRet = "GENERAL";
                    break;
                case enumLogType.DEBUG:
                    l_strRet = "DEBUG";
                    break;
                default:
                    l_strRet = "";
                    break;
            }

            return l_strRet;
        }

        public string TranslateProcessType(enumProcessType a_ptype)
        {
            string l_strRet = "";
            switch (a_ptype)
            {
                case enumProcessType.GEOCODER:
                    l_strRet = "GEOCODER";
                    break;
                case enumProcessType.TILECACHER:
                    l_strRet = "TILECACHER";
                    break;
                case enumProcessType.MISC:
                    l_strRet = "MISC";
                    break;
                default:
                    l_strRet = "";
                    break;
            }

            return l_strRet;

        }

        /// <summary>
        /// Logs messages into the log file
        /// </summary>
        /// <param name="a_process"></param>
        /// <param name="a_type"></param>
        /// <param name="a_logMSg"></param>
        public void LogMsg(enumProcessType a_process, enumLogType a_type, string a_logMSg)
        {
            if (!App.IsDebugMode && (a_type == enumLogType.DEBUG))
            {
                return;
            }

            lock (m_logLock)
            {
                ManageLogFile();
                string l_strMsg;
                string strTime = string.Format("{0:0000}{1:00}{2:00},{3:00}:{4:00}:{5:00}", DateTime.Now.Year, DateTime.Now.Month,
                                 DateTime.Now.Day, DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second);
                l_strMsg = string.Format("{0}, {1}, {2}, {3}", strTime, TranslateLogType(a_type), TranslateProcessType(a_process), a_logMSg);

                try
                {
                    using (StreamWriter logfile = new StreamWriter(m_strFileName, true))
                    {
                        logfile.WriteLine(l_strMsg);
                        logfile.Close();
                    }
                }
                catch (Exception ex)
                {
                    m_strLoggerError = ex.Message;
                }

                Debug.WriteLine(a_logMSg);
            }
        }

        /// <summary>
        /// Determine if the current active log file needs to be archived and whether the oldest archived
        /// log file needs to be deleted.
        /// </summary>
        private void ManageLogFile()
        {
            //if last check was more than 5 minutes ago, check now
            TimeSpan l_ts;
            l_ts = DateTime.Now - m_dtLastLogCheck;
            if (l_ts.TotalMinutes > 5)
            {

                // get the size of the file
                try
                {
                    long l_FileSize = new FileInfo(m_strFileName).Length;

                    // if the size of the file is greater than MAX_LOG_FILE_SIZE, archive it
                    if (l_FileSize > MAX_LOG_FILE_SIZE)
                    {
                        ArchiveCurrentLogFile();
                    }

                    if (Directory.GetFiles(Utility.GetApplicationLogFolder()).Length > 3)
                    {
                        DeleteOldestArchivedFile();
                    }

                    // set the last check time to the current time after the check is done
                    m_dtLastLogCheck = DateTime.Now;
                }
                catch (Exception)
                {
                }
            }
        }

        void SelfTest()
        {
            //MAX_LOG_FILE_SIZE = 16384;
            ManageLogFile();
            DeleteOldestArchivedFile();
        }

        /// <summary>
        /// Deletes the oldest archived file in the log folder
        /// </summary>
        /// <returns></returns>
        private bool DeleteOldestArchivedFile()
        {
            bool l_boolResult = true;
            try
            {
                // get a list of the log files in the log folder
                List<string> l_listDates = new List<string>();
                string[] l_fileList = Directory.GetFiles(Utility.GetApplicationLogFolder(), "*.log");

                // extract the date value from the file name
                foreach (string str in l_fileList)
                {
                    // the dates inserted into the file names of archived files are in ISO format
                    int l_nIndex;
                    l_nIndex = str.LastIndexOf(LOG_NAME_WITHOUT_EXTENSION);
                    if (l_nIndex != -1)
                    {
                        l_nIndex += 3; //advance past 'N_P'
                        l_nIndex = str.IndexOf("_", l_nIndex);
                        if (l_nIndex != -1)
                        {
                            l_nIndex++;
                            int l_nLength, l_nIndex2;
                            l_nIndex2 = str.IndexOf(".", l_nIndex);
                            l_nLength = l_nIndex2 - l_nIndex;
                            string strdate;
                            strdate = str.Substring(l_nIndex, l_nLength);
                            l_listDates.Add(strdate);
                        }
                    }
                }

                //if there are any files in the list, look for the oldest
                string strOldest = "99999999999999";
                foreach (string strDate in l_listDates)
                {
                    int order = string.Compare(strDate, strOldest);
                    if (order < 0)
                    {
                        strOldest = strDate;
                    }
                }

                // did we find a valid file?
                if (strOldest != "99999999999999")
                {
                    // we've found the oldest file date, now find its asociated file in 
                    // the list of files in the directory
                    string strOldestFile = "";
                    foreach (string strFile in l_fileList)
                    {
                        if (strFile.Contains(strOldest))
                        {
                            strOldestFile = strFile;
                            break;
                        }
                    }

                    // did we find the file in the list.
                    if (strOldestFile != "")
                    {
                        File.Delete(strOldestFile);
                    }
                }


                
            }
            catch (Exception)
            {
                l_boolResult = false;
            }

            return l_boolResult;
        }

        private bool ArchiveCurrentLogFile()
        {
            bool l_boolResult = true;
            //create new file with current date and time of the archival
            string l_strFileName, l_strDateTimeInfo;
            int l_nIndex;
            l_nIndex = m_strFileName.IndexOf(LOG_FILE_NAME);
            if (l_nIndex != -1)
            {
                l_nIndex = m_strFileName.IndexOf(".", l_nIndex);
            }

            if (l_nIndex != -1)
            {
                try
                {
                    // insert the formatted date and time between the file name and extension
                    l_strDateTimeInfo = string.Format("_{0:0000}{1:00}{2:00}{3:00}{4:00}{5:00}", DateTime.Now.Year, DateTime.Now.Month,
                        DateTime.Now.Day, DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second);

                    l_strFileName = m_strFileName.Insert(l_nIndex, l_strDateTimeInfo);

                    File.Move(m_strFileName, l_strFileName);
                }
                catch (Exception)
                {
                    l_boolResult = false;
                }
            }

            return l_boolResult;
        }
    }
}
