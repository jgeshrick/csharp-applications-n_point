﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace N_POINT.Misc
{
    class Utility
    {
        static public bool PathExists(string a_strPath)
        {
            bool l_boolRetVal = true;
            System.IO.DirectoryInfo l_di = new System.IO.DirectoryInfo(a_strPath);
            try
            {
                if (!l_di.Exists)
                {
                    l_boolRetVal = false;
                }
            }
            catch (Exception ex)
            {
                App.AppLogger.LogMsg(Logger.enumProcessType.MISC, Logger.enumLogType.WARNING, string.Format("Unable to verify path '{0}' exists. Message: {1}", a_strPath, ex.Message));
            }
            return l_boolRetVal;
        }


        /// <summary>
        /// Extracts the file name from a fully qualified path
        /// </summary>
        /// <param name="a_strPath"></param>
        /// <returns></returns>
        /// <remarks>
        /// Revision History
        /// Date     Who Defect# Description
        /// -------- --- ------- ----------------------------------------    
        /// 01/24/14 BJC         Initial Version
        /// </remarks>
        static public string GetFileNameFromPath(string a_strPath)
        {
            string l_str = "";
            int l_index;
            l_index = a_strPath.LastIndexOf("\\");
            if (l_index == -1)
            {
                l_str = a_strPath;
            }
            else
            {
                int l_nLength;

                l_index++;
                l_nLength = a_strPath.Length - l_index;
                l_str = a_strPath.Substring(l_index, l_nLength);
            }

            return l_str;
        }

        /// <summary>
        /// Retrieves the path location from a variable that contains a path and file name
        /// </summary>
        /// <param name="a_strFileAndPath"></param>
        /// <returns></returns>
        /// <remarks>
        /// Revision History
        /// Date     Who Defect# Description
        /// -------- --- ------- ----------------------------------------    
        /// 01/24/14 BJC         Initial Version
        /// </remarks>
        static public string GetPathFromFileAndPath(string a_strFileAndPath)
        {
            string l_str = "";
            int l_index;
            l_index = a_strFileAndPath.LastIndexOf("\\");
            if (l_index == -1)
            {
                l_str = a_strFileAndPath;
            }
            else
            {
                l_str = a_strFileAndPath.Substring(0, l_index);
            }

            return l_str;
        }

        static public string GetDefaultMapTPKPath()
        {
            string l_str;
            string l_strMapPath;
            l_str = System.Environment.GetEnvironmentVariable("public");
            l_strMapPath = System.IO.Path.Combine(l_str, "Neptune Technology Group, Inc", "MX900", "Mapping", "Layers");
            return l_strMapPath;
        }


        static public string GetMX900MapPath()
        {
            string l_str;
            string l_strMapPath;
            l_str = System.Environment.GetEnvironmentVariable("public");
            l_strMapPath = System.IO.Path.Combine(l_str, "Neptune Technology Group, Inc", "MX900", "Mapping", "Layers", "Map");
            return l_strMapPath;
        }

        static public string GetMX900SatellitePath()
        {
            string l_str;
            string l_strSatellitePath;
            l_str = System.Environment.GetEnvironmentVariable("public");
            l_strSatellitePath = System.IO.Path.Combine(l_str, "Neptune Technology Group, Inc", "MX900", "Mapping", "Layers", "Satellite");

            return l_strSatellitePath;
        }

        static public string GetApplicationWorkingFolder()
        {
            string l_str;
            string l_strWorkingPath;
            l_str = System.Environment.GetEnvironmentVariable("public");
            l_strWorkingPath = System.IO.Path.Combine(l_str, "Neptune Technology Group, Inc", "N_POINT");

            return l_strWorkingPath;
        }

        static public string GetApplicationDataFolder()
        {
            string l_str;
            string l_strAppDataPath;
            l_str = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
            l_strAppDataPath = System.IO.Path.Combine(l_str, "Neptune Technology Group, Inc", "N_POINT");

            return l_strAppDataPath;
        }


        static public bool VerifyApplicationDataFolder()
        {
            string l_str;
            bool l_boolResult = true;
            l_str = GetApplicationDataFolder();
            try
            {
                if (!PathExists(l_str))
                {
                    System.IO.Directory.CreateDirectory(l_str);
                }
            }
            catch (Exception ex)
            {
                App.AppLogger.LogMsg(Logger.enumProcessType.MISC, 
                                     Logger.enumLogType.ERROR, 
                                     string.Format("Unable to verify the application data folder. Message: {0}", ex.Message));

                l_boolResult = false;
            }

            return l_boolResult;
        }

        static public string GetApplicationMyDocumentsFolder()
        {
            string l_str;
            l_str = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            l_str = System.IO.Path.Combine(l_str, "Neptune Technology Group, Inc", "N_POINT");
            return l_str;
        }

        public static string GetApplicationLogFolder()
        {
            string l_str;
            string l_strAppLogPath = GetApplicationWorkingFolder();
            l_str = GetApplicationMyDocumentsFolder();
            l_strAppLogPath = System.IO.Path.Combine(l_str, "Log");

            return l_strAppLogPath;
        }

        static public bool VerifyApplicationLogFolder()
        {
            string l_str;
            bool l_boolResult = true;
            l_str = GetApplicationLogFolder();
            try
            {
                if (!PathExists(l_str))
                {
                    System.IO.Directory.CreateDirectory(l_str);
                }
            }
            catch (Exception)
            {
                l_boolResult = false;
            }
            return l_boolResult;
        }


        static public bool VerifyMX900FolderPaths()
        {
            string l_str;
            bool l_boolResult = true;

            l_str = GetMX900MapPath();

            try
            {
                if (!PathExists(l_str))
                {
                    System.IO.Directory.CreateDirectory(l_str);
                }                
            }
            catch(Exception ex)
            {
                App.AppLogger.LogMsg(Logger.enumProcessType.MISC, Logger.enumLogType.ERROR, string.Format("Unable to verify MX900 Map folder path. Message: {0}", ex.Message));
                l_boolResult = false;
            }

            l_str = GetMX900SatellitePath();

            try
            {
                if (!PathExists(l_str))
                {
                    System.IO.Directory.CreateDirectory(l_str);
                }
            }
            catch (Exception ex)
            {
                App.AppLogger.LogMsg(Logger.enumProcessType.MISC, Logger.enumLogType.ERROR, string.Format("Unable to verify MX900 Satellite folder path. Message: {0}", ex.Message));
                l_boolResult = false;
            }

            return l_boolResult;
        }

        static public string GetAllExceptionMessages(System.Exception a_ex)
        {
            string l_strValue = "";

            do
            {
                if (l_strValue.Length > 0)
                {
                    l_strValue += ", ";
                }
                l_strValue += "Exception: ";
                l_strValue += a_ex.Message;
                
                a_ex = a_ex.InnerException;
            } while (a_ex != null);

            return l_strValue;
        }
    }
}
