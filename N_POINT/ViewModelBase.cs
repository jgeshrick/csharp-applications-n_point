﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;

namespace N_POINT
{
    public class ViewModelBase : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// handles bound commands from the UI
        /// </summary>
        /// <remarks>
        /// Date     Who Defect Description
        /// -------- --- ------ -------------------------------------------------------
        /// 04/15/14 EJN        Initial Version
        /// </remarks>

        protected virtual void OnPropertyChanged(string a_propertyName)
        {
            if (this.PropertyChanged != null)
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs(a_propertyName));
            }
        }

        protected void CloseWindow()
        {
            foreach (System.Windows.Window window in System.Windows.Application.Current.Windows)
            {
                if (window.DataContext == this)
                {
                    window.Close();
                }
            }
        }
    }
}
